<?

// -- CLOSE SITE ---------------------------------
/*
if (($_SERVER['REMOTE_ADDR'] != '127.0.0.1') && ($_SERVER['REMOTE_ADDR'] != '188.234.213.56')) {
    header("HTTP/1.0 503 Service Unavailable");
    header("Retry-After: 3600");
    header("Location: /tech.html");
    exit();
}
*/
// -----------------------------------------------


// -- WWW. ---------------------------------------
if (isset($_SERVER['HTTP_HOST']) and (isset($_SERVER['REMOTE_ADDR']))) {
    $_ip = $_SERVER['REMOTE_ADDR'];
    if (($_ip != '127.0.0.1') and (strpos($_ip, '192.168.0') === false)) {
        $host = $_SERVER['HTTP_HOST'];
        if (count(explode('.', $host)) == 2) {
            $request_uri = $_SERVER['REQUEST_URI'];
            if ($request_uri == '/') $request_uri = '';
            header("HTTP/1.1 301 Moved Permanently");
            header("Location: http://www.{$host}{$request_uri}");
            exit();
        }
    }
}
// -----------------------------------------------

header('Pragma: public');
header('Cache-Control: private');
header('Cache-Control: no-cache, must-revalidate');

require "lib/Core/Vars.php";

// -- AUTOLOAD -----------------------------------
require R_Lib . 'autoload.php';
// -----------------------------------------------

$Modules = Load_Modules::GetInstance();
$db = MyDB2::GetInstance();
$Location = Location::getInstance();
$url = $Location->reset();
$url_arr = $Location->get_array();
$smarty = SmartyHolder::GetInstance(R_tpl_main);
$Page = Page::GetInstance();

$vars = $db->getAll("SELECT name, value FROM vars");
foreach ( $vars as $var_value ) {
    extract($var_value);
    $Page->vars[$name] = $value;
}

$TreeMenu = TreeMenu::GetInstance();

$Page->doc['menu_lv1'] = $TreeMenu->get_menu(1);
foreach ($Page->doc['menu_lv1'] as $k=>$p) {
    if ($p['module'] == 'Airports'){
        $airports = $db->getAll("
        SELECT a.id, a.title as head, concat_ws('/', '/airports', a.id, (
            SELECT ais.id FROM airports_section ais WHERE a.id = ais.id_airport AND ais.visible ORDER BY ais.sort ASC LIMIT 1
        )) as fullpath
        FROM airports a
        WHERE a.visible
        GROUP BY a.id
        ORDER BY a.sort ASC");
        $Page->doc['menu_lv1'][$k]['childs'] = $airports;
    } else {
        $Page->doc['menu_lv1'][$k]['childs'] = $TreeMenu->get_childs($p['id']);
    }
}

$loc = array();
$loc['string'] = $Location->get_string();
$loc['array'] = $Location->get_array();
$smarty->assign('location', $loc);
$smarty->assign('modules_dir', R_Modules);

$browser = $Page->get_browser();
$smarty->assign('browser', $browser);
$tpl = false;

if ($url) {
    $doc_id = $Page->init(1);

    if ($url != '-virtual-handler') {

        if (!$doc_id) {return $Page->show_404();}

        $root = $Location->get(0);
        $root_id = $db->getOne("SELECT id FROM tree WHERE fullpath = '/{$root}/'");
        if ($root_id) {
            $menu_lv2 = $TreeMenu->get_childs($root_id);
            $Page->doc['menu_lv2'] = $menu_lv2;
            foreach ($Page->doc['menu_lv2'] as &$child) {
                if ($child['module'] == 'Presskit') {
                    $Page->doc['menu_lv3'] = $db->getAll("SELECT p.id, p.title as head, concat('/pressroom/presskit/?category=', p.id) as fullpath FROM presskit_categories p WHERE p.category_id IS NULL");
                }
            }
        }

        $module = $Page->doc['module'];
        if (! empty($Page->doc['redirect'])) {
            header("HTTP/1.1 301 Moved Permanently");
            header("location: " . $Page->doc['redirect']);
            exit();
        }

        $Page->add_title($Page->vars['short_name_site']);
        $Page->add_title($Page->doc['title']);

        /* Image Page */
        $img_head_name = $Page->doc['parent'].'_'.$Page->doc['id'];
        $Page->doc['img_head'] = str_replace(ROOT, URL, Utils::findImage($img_head_name, ROOT.'u/tree/'));

    } else {
        $module = ucfirst($loc['array'][1].'_virtual');
        $module_file = $Modules->get_ext($module);
    }

    if ($module) {
        $module_file = $Modules->get_ext($module);
        if ($module_file and file_exists($module_file)) {
            require_once ($module_file);
            $handler = new $module();
            if (is_callable(array($handler,'getContent'))) {
                if (! $handler->getContent()) exit();
            }

            if ($module == 'Airports') {
                $airports_page = (int)$Location->get(2);
                $smarty->assign('airports_page', $airports_page);
            }


            $tpl = 'inner.tpl';
        }
    } else {
        $parent_id = $Page->doc['parent'];
        if ($parent_id) {
            $Page->doc['menu_brothers'] = $TreeMenu->get_brothers($parent_id); // $doc_id
        }
        $Page->doc['menu_childs'] = $TreeMenu->get_childs($doc_id);

        $tpl = 'inner.tpl';
    }
} else {
    $Page->add_title($Page->vars['name_site']);
    $Page->doc['meta_description'] = $Page->vars['index_description'];
    $Page->doc['meta_keywords'] = $Page->vars['index_keywords'];

    $News = new News();
    $main_news = $News->get_main_news();
    $smarty->assign('main_news', $main_news);

    $Airports = new Airports();
    $main_airport_info = $Airports->get_main_info();
    $smarty->assign('air_info', $main_airport_info);

    $MainPage = new Mainpage();
    $banners = $MainPage->getBanners();

//  if ($_SERVER['REMOTE_ADDR'] == '37.15.48.243') var_dump($banners);
    $smarty->assign('banners', $banners);

    $headers = $MainPage->getHeaders();
    $smarty->assign('headers', $headers);

    $tpl = 'index.tpl';
}

if (! $tpl) return $Page->show_404();

$smarty->assign('doc', $Page->doc);
$smarty->assign('vars', $Page->vars);
$smarty->display($tpl);
