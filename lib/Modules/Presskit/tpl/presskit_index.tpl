<div class="presskit">
    {if !empty($categories)}
        {foreach $categories as $category}
            <div class="category"{if $category@iteration is div by 4} style="margin-right: 0"{/if}>
                <a href="{$presskit_link}/?category={$category.id}">
                    {if ($category.thumb)}
                        <img src="{$category.thumb}">
                    {else}
                        <img src="/i/nophoto_news.jpg">
                    {/if}
                </a>
                <p>{$category.title}</p>
            </div>
        {/foreach}
    {else}
        <p><b>Файлы отсутствуют</b></p>
    {/if}

    <br class="clear" />
</div>