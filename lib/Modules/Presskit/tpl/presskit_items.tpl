<div class="presskit files list">
    {if !empty($files)}
        {if ($isGallery)}
            <script src="/js/bootstrap.min.js" type="text/javascript"></script>
            <script>
                $(document)
                    .on('mouseover', '.photo', function(){
                        $(this).children('.zoom').css('display','block');
                    })
                    .on('mouseout', '.photo', function(){
                        $(this).children('.zoom').css('display','none');
                    })

                    .on('hide.bs.collapse', function (e) {
                        $('[href$="'+e.target.id+'"]').text('Раскрыть');
                    })
                    .on('show.bs.collapse', function (e) {
                        $('[href$="'+e.target.id+'"]').text('Свернуть');
                    });
            </script>

            <div class="photo-materials" id="accordion">
                {foreach $categories as $category}
                    <div class="photo-category">

                        <div class="category-head">
                            <div class="title">{$category.title}</div>
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse{$category.id}">Раскрыть</a>
                        </div>

                        <div id="collapse{$category.id}" class="category-body collapse">
                            {foreach $catFiles[$category.id] as $item}
                                <div class="photo"{if $item@iteration is div by 6} style="margin-right: 0;"{/if}>
                                    {if ($item.thumb)}<div class="mask"><img src="{$item.thumb}" /></div>{/if}
                                    <div class="zoom">
                                        <div class="info">{$item.title}</div>
                                        <div class="download">
                                            {$item.width}x{$item.height}; {$item.size}<br /><a href="{$presskit_link}/?get={$item.hash}">Скачать</a>
                                        </div>
                                        <img src="{$item.thumb}" />
                                    </div>
                                </div>
                            {/foreach}
                        </div>

                    </div>
                    <br class="clear" />
                {/foreach}
            </div>

            {*
            <div class="photo-materials" id="accordion">
                {foreach $categories as $category}
                    <div class="panel photo-category">
                        <div class="category-head">
                            <div class="title">{$category.title}</div>
                            <a data-toggle="collapse" href="#collapse{$category.id}" data-parent="#accordion">раскрыть</a>
                        </div>
                        <div id="collapse{$category.id}" class="category-body collapse">
                            {foreach $catFiles[$category.id] as $item}
                                <div class="photo"{if $item@iteration is div by 6} style="margin-right: 0;"{/if}>
                                    {if ($item.thumb)}<div class="mask"><img src="{$item.thumb}" /></div>{/if}
                                    <div class="zoom">
                                        <div class="info">{$item.title}</div>
                                        <div class="download">
                                            {$item.width}x{$item.height}; {$item.size}<br /><a href="{$presskit_link}/?get={$item.hash}">Скачать</a>
                                        </div>
                                        <img src="{$item.thumb}" />
                                    </div>
                                </div>
                            {/foreach}
                        </div>
                    </div>
                    <br class="clear" />
                {/foreach}
            </div>
            *}
        {else}
            {foreach $files as $item}
                <a href="{$presskit_link}/?get={$item.hash}" class="item"{if $item@iteration is div by 4} style="margin-right: 0"{/if}>
                    <div class="icon"><img src="/i/file_{$item.icon}.png" /></div>
                    <span>Скачать {$item.size}</span>
                    <p>{$item.title}</p>
                </a>
                {if $item@iteration is div by 4}
                    <br class="clear" />
                {/if}
            {/foreach}
        {/if}
    {else}
        <p><b>Файлы отсутствуют</b></p>
    {/if}
</div>