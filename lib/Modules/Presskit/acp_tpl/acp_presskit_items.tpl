<div class="padding-block">
    <h3>Пресс-кит. {$category.title}</h3>
    <div class="row-fluid">

        <a class="btn btn-success addFile" style="margin-bottom: 20px;" href="?doc_id={$doc_id}&module&action=edit&category_id={$category.id}" data-original-title="">
            <i class="icon-plus icon-white"></i> Добавить файл
        </a>

        {if ($category.category_id)}
            <a class="btn btn-info" style="margin-bottom: 20px;" href="?doc_id={$doc_id}&module&action=category&id={$category.category_id}" data-original-title="">
                <i class="icon-circle-arrow-left icon-white"></i> Список категорий
            </a>
        {else}
            <a class="btn btn-info" style="margin-bottom: 20px;" href="?doc_id={$doc_id}&module" data-original-title="">
                <i class="icon-circle-arrow-left icon-white"></i> Список категорий
            </a>
        {/if}

        <ul class="thumbnails">
            {foreach $files as $file}
                <li class="span3" data-id="{$file.id}"{if $file@iteration is div by 5} style="margin-left: 0"{/if}>
                    <div class="thumbnail">
                        <div class="thumb">
                            {if ($file.thumb)}
                                <div style="height: 110px; overflow: hidden;"><img src="{$file.thumb}" class="span12"></div>
                            {else}
                                <img src="/i/nophoto_news.jpg" class="span12">
                            {/if}
                        </div>
                        <div class="name span12 overflow-text-white">{$file.title}<span>&nbsp;</span></div>
                        <a data-original-title="{$file.title}" href="?doc_id={$doc_id}&module&action=edit&category_id={$category.id}&id={$file.id}" class="btn btn-warning btn-mini editFile"><i class="icon-pencil icon-white"></i></a>
                        <a href="?doc_id={$doc_id}&module&action=delete&category_id={$category.id}&id={$file.id}" class="btn btn-danger btn-mini deleteFile"><i class="icon-remove icon-white"></i></a>
                    </div>
                </li>
            {/foreach}
        </ul>

        <div id="editModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel">Новый файл</h3>
            </div>
            <div class="modal-body">
                <form class="form-inline" id="createFileForm" action="?doc_id={$doc_id}&module&action=edit&category_id={$category.id}" method="post">
                    Название:
                    <input type="text" name="title" id="title" class="input-block-level" placeholder="Текущее название файла">
                    Файл:
                    <input type="file" id="fileupload" name="file" />
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Отмена</button>
                <button class="btn btn-primary uploadFile">Сохранить изменения</button>
            </div>
        </div>

    </div>
</div>


<script id="fileTemplate" type="text/html">
    <li class="span3">
        <div class="thumbnail">
            <div class="thumb"></div>
            <div class="name span12 overflow-text-white"><span>&nbsp;</span></div>
            <a data-original-title="" href="" class="btn btn-warning btn-mini editFile"><i class="icon-pencil icon-white"></i></a>
            <a href="" class="btn btn-danger btn-mini deleteFile"><i class="icon-remove icon-white"></i></a>
        </div>
    </li>
</script>


<script type="application/javascript">
    $(function() {
        var success = function (item) {
            var template = $('[data-id="'+item.id+'"]');
            if (!template.length) {
                template = $($('#fileTemplate').html())
                $('.thumbnails').append(template);
            }
            template.data('id', item.id);
            $('.thumb', template)
                    .html(
                            item.thumb ? '<div style="height: 110px; overflow: hidden;"><img src="'+item.thumb+'?'+Math.random()+'" class="span12"></div>'
                                    : '<img src="/i/nophoto_news.jpg" class="span12">'
                    );
            $('div.name', template).text(item.title);
            $('.editFile', template).data('original-title', item.title).attr('href', '?doc_id={$doc_id}&module&action=edit&category_id={$category.id}&id='+item.id);
            $('.deleteFile', template).attr('href', '?doc_id={$doc_id}&module&action=delete&category_id={$category.id}&id='+item.id);
            $('#editModal').modal('hide');
        }

        $('.uploadFile').click(function() {
            var form = $('#createFileForm');
            $.post(form.attr('action'), form.serialize(), success, 'json');
        });

        $('#fileupload').fileupload({
            autoUpload: false,
            dataType: 'json',
            maxChunkSize: 10485760, // 10mb
            add: function (e, data) {
                var uploadErrors = [];
                var acceptFileTypes = /(\.|\/)({$category.exts})$/i;
                if(data.originalFiles[0]['type'].length && !acceptFileTypes.test(data.originalFiles[0]['name'])) {
                    uploadErrors.push('Неправильный тип файла');
                }
                if(uploadErrors.length > 0) {
                    alert(uploadErrors.join("\n"));
                }

                $('.uploadFile').off().click(function () {
                    data.submit();
                });
            },
            done: function (e, data) {
                if (data.result) {
                    success(data.result);
                }
            }
        });

        $(document)
            .on('click', '.editFile, .addFile', function(e) {
                e.preventDefault();
                $('#title', '#editModal').val($(this).data('original-title'));
                $('#createFileForm', '#editModal').attr('action', $(this).attr('href'));
                $('#editModal').modal('show');
            })
            .on('click', '.deleteFile', function(e) {
                e.preventDefault();
                var self = $(this);
                $.get(self.attr('href'), function() {
                    self.closest('li').remove();
                });
            });
    });
</script>