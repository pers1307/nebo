<?
class Presskit extends Module
{

    protected $name = __CLASS__;

    private $folder = 'presskit/';
    private $path;
    private $link;

    function __construct()
    {
        parent::__construct();
        $this->path = USER_FILES_DIRECTORY . $this->folder;
        $this->folder = USER_FILES_PATH . $this->folder;

        $Location = Location::getInstance();
        $this->link = '/'.$Location->first().'/'.$Location->last();
    }

    function getContent()
    {
        $this->smarty->assign('presskit_link', $this->link);

        if (isset($_GET['category'])) {
            $this->category();
        } elseif (isset($_GET['get'])) {
            $this->get();
            return false;
        } else
            $this->index();

        return true;
    }

    private function index()
    {
        $categories = $this->db->getAll("SELECT * FROM presskit_categories WHERE category_id IS NULL");

        $this->_index($categories);
    }

    private function category()
    {
        $id = Utils::parseGet('category');
        $category = $this->db->getRow("SELECT * FROM presskit_categories WHERE id = ?", array($id));
        if ($category['parent']) {
            $categories = $this->db->getAll("SELECT * FROM presskit_categories WHERE category_id = ?", array($id));
            $category['categories'] = $categories;
        }

        $this->smarty->assign('link_back', $this->link);
        $this->page->doc['head'] = $category['title'];
        $this->_items($category);
    }

    private function _index($categories)
    {
        $path = $this->path;
        $folder = $this->folder;
        array_walk($categories, function (&$item) use ($path, $folder) {
            $file = "{$path}category_{$item['id']}.jpg";
            if (file_exists($file))
                $item['thumb'] = str_replace($path, $folder, $file);
        });
        $this->smarty->assign('categories', $categories);
        $this->page->doc['body'] = $this->smarty->fetch($this->tpl_dir.'presskit_index.tpl');
    }

    private function _items($category)
    {
        if (isset($category['categories'])) {
            $ids = array_map(function($item) {
                return $item['id'];
            }, $category['categories']);
            $files = $this->db->getAll("SELECT * FROM presskit_files WHERE category_id IN ("
                . implode(',', array_fill(0, count($ids), '?')) . ")", $ids);
        } else {
            $files = $this->db->getAll("SELECT * FROM presskit_files WHERE category_id = ?", array($category['id']));
        }

        $folder = $this->folder;
        $path = $this->path;
        $isGallery = true;
        array_walk($files, function (&$item) use ($folder, $path, &$isGallery) {
            if (isset($item['ext']) && in_array($item['ext'], array('jpg', 'png', 'gif'))) {
                $item['thumb'] = "{$folder}{$item['hash']}_thumb.{$item['ext']}";
                $item['original'] = "{$folder}{$item['hash']}.{$item['ext']}";
            }
            $item['size'] = Tools::convert_file_size($item['size']);
            $item['icon'] = preg_replace(
                array('/docx/', '/pptx|ppsx/', '/xlsx|xlsm/', '/^(?!(ai|cdr|doc|otf|pdf|ppt|psd)).+$/'),
                array('doc',    'ppt',         'xls',         'all'),
                $item['ext']
            );
            $isGallery = $isGallery && isset($item['thumb']);
            if (isset($item['thumb']))
                list($item['width'], $item['height']) = getimagesize("{$path}{$item['hash']}.{$item['ext']}");
        });

        if (isset($category['categories'])) {
            $catFiles = array();
            array_walk($files, function($item) use (&$catFiles) {
                $catFiles[$item['category_id']][] = $item;
            });
            $this->smarty->assign('categories', $category['categories']);
            $this->smarty->assign('catFiles', $catFiles);
        }

        $this->smarty->assign('isGallery', $isGallery);
        $this->smarty->assign('files', $files);
        $this->page->doc['body'] = $this->smarty->fetch($this->tpl_dir.'presskit_items.tpl');
    }

    private function get()
    {
        $hash = $_GET['get'];
        $file = $this->db->getRow("SELECT * FROM presskit_files WHERE hash = ?", array($hash));
        if ($file) {
            header('Pragma: public');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
//            header("Content-Type: $mimeType");
//            header('Content-Length: '.$length);
            header("Content-Disposition: attachment; filename=\"{$file['title']}.{$file['ext']}\"");
            header('Content-Transfer-Encoding: binary');
            readfile("{$this->path}{$file['hash']}.{$file['ext']}");
        } else {
            header("Location: /pressroom/presskit/");
        }
    }
}