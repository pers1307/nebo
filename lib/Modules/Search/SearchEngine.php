<?php

include_once R_core . 'SphinxClient.php';

class SearchEngine
{
    const CAT_TREE = 1;

    const CAT_LEADERS = 2;

    const CAT_ASECTION = 3;

    const CAT_AALBUMS = 4;

    const CAT_NEWS = 5;

    const CAT_MEDIA = 6;

    const CAT_PRESSKIT = 7;

    const CAT_VACANCIES = 8;

    const CAT_VIDEOS = 9;

    private $sphinx;

    private $db;

    public function __construct()
    {
        $this->sphinx = new \Sphinx\SphinxClient();
        $this->db = MyDB2::GetInstance();
    }

    public function search($query)
    {
        $this->sphinx->_limit = 1000;
        $this->sphinx->SetSortMode(SPH_SORT_ATTR_ASC, 'type');
        $results = $this->sphinx->Query($query, 'tree-ru leaders-ru airports_section-ru airport_albums-ru news-ru news_media-ru presskit_files-ru vacancies-ru videos-ru');

        $return = array();
        if ($results === false) {
            $return = 'Запрос не выполнен: ' . $this->sphinx->GetLastError();
        } else {
            foreach ($results['matches'] as $id => $item) {
                if (!isset($return[$item['attrs']['type']]))
                    $return[$item['attrs']['type']] = array();
                $return[$item['attrs']['type']] = array_replace_recursive($return[$item['attrs']['type']], $this->getLink($id, $item));
            }
        }

        return $return;
    }

    private function getLink($id, $item)
    {
        $category = $item['attrs']['type'];
        switch ($category) {
            case self::CAT_TREE:
                $row = $this->findModuleById($id);
                $title = '-';
                $url = $row['fullpath'];
                $label = $row['title'];
                break;
            case self::CAT_LEADERS:
                $row = $this->findModuleByName('leaders');
                $title = $row['title'];
                $url = $row['fullpath'];
                $label = $row['title'];
                break;
            case self::CAT_ASECTION:
                $row = $this->findModuleByName('airports');
                $title = $row['title'];
                $airport = $this->db->getRow("SELECT a.title, ais.name, a.id as airport FROM airports_section ais JOIN airports a ON ais.id_airport = a.id where ais.id = ?", array($id));
                $url = "{$row['fullpath']}{$airport['airport']}/{$id}/";
                $label = "{$airport['title']} - {$airport['name']}";
                break;
            case self::CAT_AALBUMS:
                $row = $this->db->getRow("SELECT a.title, aa.title as name, a.id as airport FROM airport_albums aa JOIN airports a ON aa.id_airport = a.id where aa.id = ?", array($id));
                $title = 'Фотогалерея';
                $url = "/airports/{$row['airport']}/gallery/?album={$id}";
                $label = "{$row['name']}";
                break;
            case self::CAT_NEWS:
                $row = $this->findModuleByName('news');
                $title = $row['title'];
                $news = $this->db->getRow("SELECT n.title FROM news n where n.id = ?", array($id));
                $url = $row['fullpath'] . "?n={$id}";
                $label = "{$news['title']}";
                break;
            case self::CAT_MEDIA:
                $row = $this->findModuleByName('media');
                $title = $row['title'];
                $media = $this->db->getRow("SELECT m.title FROM news_media m where m.id = ?", array($id));
                $url = $row['fullpath'] . "?n={$id}";
                $label = "{$media['title']}";
                break;
            case self::CAT_PRESSKIT:
                $row = $this->findModuleByName('presskit');
                $title = $row['title'];
                $presskit = $this->db->getRow("SELECT pc.title, p.category_id as category FROM presskit_files p join presskit_categories pc where p.id = ?", array($id));
                $url = $row['fullpath'] . "?category={$presskit['category']}";
                $label = "{$presskit['title']}";
                break;
            case self::CAT_VACANCIES:
                $row = $this->findModuleByName('vacancy');
                $title = $row['title'];
                $vacancy = $this->db->getRow("SELECT a.id as airport, a.title, a.city FROM vacancies v join airports a on v.airport_id = a.id where v.id = ?", array($id));
                $url = $row['fullpath'] . "?airport={$vacancy['airport']}";
                $label = "{$vacancy['city']} - {$vacancy['title']}";
                break;
            case self::CAT_VIDEOS:
                $row = $this->findModuleByName('video');
                $title = $row['title'];
                $video = $this->db->getRow("SELECT v.type FROM videos v where v.id = ?", array($id));
                $url = "{$row['fullpath']}?t={$video['type']}";
                $label = $row['title'];
                break;
            default:
                $title = $url = $label = '';
        }
        return array('title' => $title, 'items' => array($id => array('url' => $url, 'label' => $label)));
    }

    private function findModuleById($id)
    {
        return $this->db->getRow("SELECT IF(title_h1, title_h1, title) as title, fullpath FROM tree WHERE id = ?", array($id));
    }

    private function findModuleByName($name)
    {
        return $this->db->getRow("SELECT IF(title_h1, title_h1, title) as title, fullpath FROM tree WHERE fname = ?", array($name));
    }
}
