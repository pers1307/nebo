<div class="search-result">
    <form method="get" enctype="application/x-www-form-urlencoded">
        <input type="text" name="q" id="q" value="{$smarty.get.q}" placeholder="Введите запрос для поиска">
        <button type="submit" class="btn-send">Искать</button>
        <br class="clear" />
    </form>
    {if $results}
        {if is_array($results)}
            <ul>
                {foreach $results as $category}
                    {if $category.title == '-'}
                        {foreach $category.items as $item}
                            <li class="page"><a href="{$item.url}">{$item.label}</a></li>
                        {/foreach}
                    {else}
                        <li class="page">{$category.title}</li>
                        <ul>
                            {foreach $category.items as $item}
                                <li class="link"><a href="{$item.url}">{$item.label}</a></li>
                            {/foreach}
                        </ul>
                    {/if}
                {/foreach}
            </ul>
        {else}
            <p>{$results}</p>
        {/if}
    {/if}
</div>