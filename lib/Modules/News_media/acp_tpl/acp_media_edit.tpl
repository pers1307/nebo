<div class="padding-block">
    <h3>Редактирование новости {if $media_item.visible == 0}<span class="label label-info">скрытая<span>{/if}</h3>
    <a class="btn btn-info" href="?doc_id={$doc_id}&module{if $smarty.get.p}&p={$smarty.get.p}{/if}">
        <i class="icon-circle-arrow-left icon-white"></i>
        к списку новостей
    </a>
</div>

<form method="post" enctype="multipart/form-data" action="?doc_id={$doc_id}&module&action=save" onsubmit="return media_form_confirm()"  class="form-horizontal">
    <input type="hidden" name="id" value="{$media_item.id}" />

    <div class="control-group">
        <label class="control-label" for="title">Заголовок новости</label>
        <div class="controls">
            <input type="text" class="span6" id="title" name="title" value="{$media_item.title}" autocomplete="off" />
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="short_text">Анонс</label>
        <div class="controls">
            <textarea name="short_text" id="short_text" class="span6">{$media_item.short_text}</textarea>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="full_text">Текст новости</label>
        <div class="controls">
            <textarea name="full_text" id="full_text">{$media_item.full_text}</textarea>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="publish_date">Дата публикации</label>
        <div class="controls">
            <input class="datepicker" type="text" name="publish_date" id="publish_date" value="{$media_item.publish_date|date_format:'%d.%m.%Y'}" autocomplete="off" />
        </div>
    </div>

    {*
    <div class="control-group">
        <label class="control-label">Изображение</label>
        <div class="controls">
            {if ($media_item.img)}
                <ul class="thumbnails" id="img">
                    <li style="width:180px; margin-left:20px;">
                        <a class="thumbnail"><img onclick="img_preview($(this), '{$media_item.img_src}')" src="{$media_item.img_src}"></a>
                        <span class="btn btn-mini btn-danger" style="margin-top: 5px;" onclick="media_img_delete({$media_item.id}, 'img')"><i class="icon-white icon-remove"></i> Удалить изображение</span>
                    </li>
                </ul>
            {/if}
            <input type="file" name="img" id="input_file" {if ($media_item.img)}style="display:none"{/if}/>
        </div>
    </div>
    *}



    <div class="control-group">
        <label class="control-label"></label>
        <div class="controls">
            <div class="row-fluid">
                <ul class="thumbnails" style="margin-right: 20px;"></ul>
            </div>
            <!--div id="files" class="files"></div-->
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="publish_date"></label>
        <div class="controls">
            <button class="btn btn-primary b-save" onclick="submit">Сохранить</button>
        </div>
    </div>
</form>

<link rel="stylesheet" href="/_bo/s/ui.datepicker.css">
<link rel="stylesheet" href="/_bo/s/ui.theme.css">
<script type="text/javascript">
    $(function() {
        $('.datepicker').datepicker( { firstDay: 1, dateFormat: 'dd.mm.yy', dayNamesMin: ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'], dayNames: ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье'] } )
        CKEDITOR.replace('full_text', { "filebrowserBrowseUrl": "\/_bo\/ckfinder\/ckfinder.html", "filebrowserImageBrowseUrl": "\/_bo\/ckfinder\/ckfinder.html?type=Images", "filebrowserFlashBrowseUrl": "\/_bo\/ckfinder\/ckfinder.html?type=Flash", "filebrowserUploadUrl": "\/_bo\/ckfinder\/core\/connector\/php\/connector.php?command=QuickUpload&type=Files", "filebrowserImageUploadUrl": "\/_bo\/ckfinder\/core\/connector\/php\/connector.php?command=QuickUpload&type=Images", "filebrowserFlashUploadUrl": "\/_bo\/ckfinder\/core\/connector\/php\/connector.php?command=QuickUpload&type=Flash", "toolbar": "Text", "width": "100%", "height": "350" });
        var url = '?doc_id={$doc_id}&module&action=upload_images&id={$media_item.id}';
    });

    function media_img_delete(id, imgkey) {
        if (confirm('Удалить изображение?')) {
            $.post('?doc_id='+doc_id+'&module&action=img_delete', { id:id, imgkey:imgkey }, function(data) {
                if (data == 1) {
                    $('#'+imgkey).fadeOut();
                    $('#input_file').css({ 'display':'block' })
                }
            } )
        }
    }

</script>