<?
class ACP_News_media extends ACP_Module {

	protected $name = __CLASS__;

	private $folder = 'news_media/';
	private $path;

	private $limit = 10;

	private $img_size = array('img'=>array(450=>'', 230=>'_s'));


	function __construct() {
		parent::__construct();
		$this->path = USER_FILES_DIRECTORY.$this->folder;
		$this->folder = USER_FILES_PATH.$this->folder;
	}

	function rewrite() {
		switch ($this->action) {
			case 'edit':
				$this->edit_media();
				break;

			case 'save':
				$this->save_media();
				break;

			case 'delete':
				$this->delete_media();
				return false;
				break;

            case 'upload_images':
                $this->upload_images();
                return false;
                break;

            case 'img_delete':
				$this->delete_img();
				return false;
				break;

            case 'item_visibility':
				$this->item_visibility();
				return false;
				break;

 			default:
 				$this->show_list();
 				break;
		}
		return true;
	}

	function fix_slashes() {
		$list = $this->db->getAll("SELECT id, full_text FROM news_media");
		foreach ($list as $row) {
			extract($row);
			$c = mb_strlen($full_text);
			$full_text = stripslashes($full_text);
			$c2 = mb_strlen($full_text);
			$this->db->update('news_media', compact('full_text'), "id = {$id}");
//			echo "[{$id} {$c} &rarr; {$c2}]<br>";
		}
	}

	private function show_list() {
		$count = $this->db->getOne("SELECT COUNT(*) FROM news_media");
		$limit = $this->limit;
		$p = (isset($_GET['p'])) ? intval($_GET['p']) : 0;
		$limit_from =  $limit * $p;
		$count = ceil($count / $limit);
		$this->smarty->assign('pages', array('current'=>$p, 'count'=>$count));

		$media = $this->db->getAll("SELECT * FROM news_media ORDER BY publish_date DESC LIMIT $limit_from, $limit");
		$this->smarty->assign('media', $media);
		$this->controller->body = $this->smarty->fetch($this->tpl_dir.'acp_media_list.tpl');
	}

	private function edit_media() {
		if ((isset($_GET['id'])) and ($id = intval($_GET['id']))) {
			$news = $this->db->getRow("SELECT * FROM news_media WHERE id = {$id}");
			if ($news['img']) {
				$news['img_src'] = $this->folder.$id.'_s.'.$news['img'];
			}
		} else {
			$id = false;
			$publish_date = date("d.m.Y");
			$news = compact('publish_date');
		}

		$this->smarty->assign('media_item', $news);
		$this->controller->body = $this->smarty->fetch($this->tpl_dir.'acp_media_edit.tpl');
	}

	private function save_media() {
		if (empty($_POST)) {
			header("Location: /_bo/?doc_id={$this->doc_id}&module");
			exit;
		}
		$id = intval($_POST['id']);

		$arr = array();
		$arr['title'] = htmlspecialchars(trim($_POST['title']));
		$arr['short_text'] = htmlspecialchars(trim($_POST['short_text']));
		$arr['full_text'] = stripslashes(trim($_POST['full_text']));
		$arr['create_date'] = time();
		$arr['publish_date'] = Tools::date_to_timestamp($_POST['publish_date'], '.');

		if (!$arr['title']) {
			header("Location: /_bo/?doc_id={$this->doc_id}&module&action=edit&id={$id}");
			exit;
		}

		if ($id) {
			$this->db->update('news_media', $arr, "id = $id");
		} else {
			$id = $this->db->insert('news_media', $arr);
		}


        $img_ext = $this->upload_images($id, $this->img_size);
		if (count($img_ext)) {
			$this->db->update('news_media', $img_ext, "id = $id");
		}


		header("Location: /_bo/?doc_id={$this->doc_id}&module&action=edit&id={$id}");
		exit;
	}

    /*
    function upload_images() {
        $id = intval($_GET['id']);
        $options = array(
            'upload_dir' => $this->path,
            'upload_url' => $this->folder,
        );
        $fileupload = new FileUploader($options, false);
        $fileupload->post();
    }
    */

	private function upload_images($id, $key_arr = array()) {
		$output = array();
		foreach ($key_arr as $key=>$sizes) {
			if (!isset($_FILES[$key]) or empty($_FILES[$key]['tmp_name']) or $_FILES[$key]['error']) continue;

			$tmp_name = $_FILES[$key]['tmp_name'];
			$file_name = $_FILES[$key]['name'];

			$dest_path = $this->path;

			if (!file_exists($dest_path)) {
				Tools::create_directory($dest_path, 0777);
			}
			if (!is_writeable($dest_path)) continue;

			$e = strrpos($file_name, '.') + 1;
			if ($e == 1) continue;

			$ext = substr($file_name, $e);
			if ($ext == 'jpeg') $ext = 'jpg';

			if (!in_array($ext, array('jpg', 'png', 'gif'))) continue;

			$file = $dest_path.$id.'.'.$ext;

			if (file_exists($file)) {
				unlink($file);
			}

			rename($tmp_name, $file);
			@chmod($file, 0755);

			if (is_array($sizes) and count($sizes)) {
				$Img = Image::CreateFromFile($file);
				foreach ($sizes as $w=>$prfx) {
					$new_file = $dest_path.$id.$prfx.'.'.$ext;
					$Img->resize($w);
					$Img->save($new_file);
				}
			}

			$output[$key] = $ext;
		}
		return $output;
	}


	private function delete_media(){
		if (!isset($_POST['id'])) exit;
		$id = intval($_POST['id']);
		if (!$id) exit;
		$this->db->delete('news_media', "id = $id");

		echo 1;
	}


	function delete_img() {
		if (!isset($_POST['id']) or !isset($_POST['imgkey'])) exit;
		$id = intval($_POST['id']);
		$imgkey = trim($_POST['imgkey']);

		if (!$id or !isset($this->img_size[$imgkey])) exit;

		$ext = $this->db->getOne("SELECT $imgkey FROM news_media WHERE id = $id");
		if ($ext) {
			$this->db->update('news_media', array($imgkey=>null), "id = $id");

			$prfxs = $this->img_size[$imgkey];
			if (is_array($prfxs)) {
				foreach ($prfxs as $s=>$prfx) {
					$file = $this->path."{$id}{$prfx}.{$ext}";
					if (file_exists($file)) {
						@unlink($file);
					}
				}
			} else {
				$file = $this->path."{$id}.{$ext}";
				if (file_exists($file)) {
					@unlink($file);
				}
			}
		}
		echo 1;
	}

    public function item_visibility() {
		$id = Utils::parseGet('id');
		$item = $this->db->getRow("SELECT * FROM news_media WHERE id=".(int)$id);
		if (!empty($item)) {
			$this->db->update('news_media', array('visible'=>($item['visible'] ? 0:1)), 'id='.$id);
            echo (int)!$item['visible'];
            exit();
		}
	}

}