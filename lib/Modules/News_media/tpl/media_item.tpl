<div class="news-page one-news">
    {if ($media_item)}

        <h3>{$media_item.title}</h3>
        {if (!$media_item.full_text)}{$media_item.short_text}{else}{$media_item.full_text}{/if}

        <br />
        <a href="/{$media_link}/">Вернуться к списку репортажей</a>
    {else}
        <p>Репортаж не найден</p>
    {/if}
</div>
<script type="text/javascript">
    createPrettyPhoto();
</script>