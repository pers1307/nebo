<?
class Job extends Module {

    protected $name = __CLASS__;

    private $schedule = array(
        1 => 'Полный рабочий день',
        2 => 'Сменный график',
        3 => 'Гибкий график',
    );

    private $link;

    function __construct() {
        parent::__construct();
        $Location = Location::getInstance();
        $this->link = '/'.$Location->first().'/'.$Location->last();
        $this->smarty->assign('vacancy_link', $this->link);
    }

    function getContent() {

        $this->smarty->assign('job_link', $this->link);
        $this->page->doc['head'] = '';

        if (isset($_GET['vacancy'])) {
            $this->show_item();
        } elseif (isset($_GET['print'])) {
            $this->print_item();
            return false;
        } elseif (isset($_GET['resume'])) {
            $this->resume_download();
            return false;
        } else {
            $this->show_list();
        }

        return true;
    }

    function show_list() {
        $airport = Utils::parseGet('airport');
        $airports = $this->db->getAll('SELECT a.id, a.title, a.full_title, a.city FROM airports a JOIN vacancies v ON a.id = v.airport_id AND v.enabled GROUP BY a.id');

        if (!$airport) {
            $vacancies = $this->db->getAll('SELECT id, title, preview, salary FROM vacancies WHERE enabled');
        } else {
            $vacancies = $this->db->getAll('SELECT id, title, preview, salary FROM vacancies WHERE enabled AND airport_id = ?', array($airport));
        }


        $this->smarty->assign('current_airport', $airport);
        $this->smarty->assign('airports', $airports);
        $this->smarty->assign('vacancies', $vacancies);
        $this->page->doc['job'] = $this->smarty->fetch($this->tpl_dir.'job_list.tpl');
    }

    function show_item() {
        $vacancy = $this->db->getRow('SELECT v.*, a.city, a.full_title as airport FROM vacancies v JOIN airports a ON a.id = v.airport_id WHERE v.enabled AND v.id = ?', array($_GET['vacancy']));
        $vacancy['schedule'] = $this->schedule[$vacancy['schedule']];

        if (!empty($_POST) && $_GET['vacancy']) {
            $this->send_resume();
        }

        $this->smarty->assign('vacancy', $vacancy);
        $this->page->doc['job'] = $this->smarty->fetch($this->tpl_dir.'job_item.tpl');
    }

    function print_item() {
        $vacancy = $this->db->getRow('SELECT v.*, a.city, a.full_title as airport FROM vacancies v JOIN airports a ON a.id = v.airport_id WHERE v.enabled AND v.id = ?', array($_GET['print']));
        $vacancy['schedule'] = $this->schedule[$vacancy['schedule']];

        $this->smarty->assign('vacancy', $vacancy);
        echo $this->smarty->fetch($this->tpl_dir.'job_print.tpl');
    }

    function send_resume() {
        $id = Utils::parseGet('vacancy');
        $data = array();

        // Validate form
        if (empty($_POST['name']))
            $errors['name'] = 'Пожалуйста, представьтесь';
        if (empty($_POST['contact']))
            $errors['contact'] = 'Укажите, как с Вами можно связаться';
        if (isset($_POST['comment']))
            $data['comment'] = $_POST['comment'];
        if ($upload = (isset($_FILES['file']) && $_FILES['file']['error'] == 0)) {
            if (!preg_match('/\.(doc|docx|pdf)$/', $_FILES['file']['name'], $match))
                $errors['file'][] = 'Разрешенные форматы: doc, docx, pdf, не более 2 Мб';
            else
                $data['ext'] = $match[1];
            if ($_FILES['file']['size'] > 2097152)
                $errors['file'][] = 'Размер файла больше 2 Мб.';
            $data['hash'] = md5(time() . $_FILES['file']['name']);
        }

        if (!empty($errors)) {
            $this->smarty->assign('errors', $errors);
            return;
        }

        if ($id) {
            $sql = $this->db->getRow('SELECT id, title FROM vacancies WHERE id ='.$id);
            $vac_title = $sql['title'];
            $this->smarty->assign('title_vacancy', $vac_title);
        }

        $data = array_merge(array(
            'name' => $_POST['name'],
            'contact' => $_POST['contact'],
            'vacancy_id' => $id,
        ), $data);

        $data['id'] = $this->db->insert('resumes', $data);
        $this->smarty->assign('item', $data);


        if (!file_exists(USER_FILES_DIRECTORY . 'resumes'))
            Tools::create_directory(USER_FILES_DIRECTORY . 'resumes/', 0777);

        if ($upload && move_uploaded_file($_FILES['file']['tmp_name'], USER_FILES_DIRECTORY . "resumes/{$data['hash']}.{$data['ext']}")) {
            $link = $_SERVER['HTTP_HOST'] . $this->link . "?resume={$data['hash']}";
            $this->smarty->assign('link', $link);
        }

        $body = $this->smarty->fetch($this->tpl_dir.'job_email.tpl');
        $airport = $this->db->getRow("SELECT a.id, a.emails FROM airports a JOIN vacancies v ON a.id = v.airport_id AND v.id = ?", array($id));
        if ($airport['emails']) {
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
            mail($airport['emails'], 'На сайте добавлено резюме', $body, $headers);
        }
    }

    private function resume_download()
    {
        $hash = !empty($_GET['resume']) ? $_GET['resume'] : null;
        if ($hash) {
            $item = $this->db->getRow("SELECT name, ext, hash FROM resumes r where r.hash = ?", array($hash));
            $path = USER_FILES_DIRECTORY . "resumes/{$item['hash']}.{$item['ext']}";
            if (file_exists($path)) {
                $name = str_replace(' ', '_', $item['name']);
                header("Content-Disposition: attachment; filename=\"{$name}.{$item['ext']}\"");
                header('Content-Transfer-Encoding: binary');
                readfile($path);
            }
        }
    }
}