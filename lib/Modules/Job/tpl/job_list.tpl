<div class="title_h1">Актуальные вакансии</div>

<div class="job category">
    {foreach $airports as $airport}
        <a href="{$vacancy_link}?airport={$airport.id}" class="{if $airport.id == $current_airport}active{/if}">{$airport.title}</a>
    {/foreach}
</div>

<div class="job list">
    {foreach $vacancies as $vacancy}
        <a href="{$vacancy_link}/?vacancy={$vacancy.id}" class="item">
            <div class="city">{$airport.city}</div>
            <div class="title">{$vacancy.title}</div>
            <p>{*if !empty($airport.full_title)}{$airport.full_title}<br />{/if*}{$vacancy.preview}</p>
            {if $vacancy.salary != 0}<div class="money">{$vacancy.salary|number_format:0:"":" "} рублей</div>{/if}
        </a>
    {/foreach}
</div>