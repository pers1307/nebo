<!DOCTYPE html>
<html>
<head>

</head>
<body>
<div>
    <div>
        <h1>{$vacancy.title}</h1>

        <p>{$vacancy.city}</p>

        <p>Дата добавления: {$vacancy.createdAt}</p>

        <div>{$vacancy.salary|number_format:0:"":" "} рублей</div>
        <div>{$vacancy.schedule}</div>
        <div>Опыт работы: {$vacancy.experience}</div>
        <p>Контактное лицо:<br/>{$vacancy.contact}</p>
    </div>
    <div>
        {$vacancy.text}
    </div>
    <script type="application/javascript">
        window.print();
    </script>
</div>
</body>
</html>