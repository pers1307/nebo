<div class="job info">
    <a href="{$vacancy_link}?airport={$vacancy.airport_id}" class="back">Назад</a>
    <div class="title">{$vacancy.title}</div>
    <div class="left">
        <p>{$vacancy.city}</p>
        <p>Добавлена: {$vacancy.createdAt}</p>
        {if $vacancy.salary != 0}<div class="money">{$vacancy.salary|number_format:0:"":" "} рублей</div>{/if}
        <div class="day">{$vacancy.schedule}</div>
        <div class="other">Опыт работы: {$vacancy.experience}</div>
        <a href="" class="resume">{if $errors}Посмотреть описание{else}Отправить резюме{/if}</a><br /><br />
        <a href="/airports/{$vacancy.airport_id}">{$vacancy.airport}</a><br /><br />
        <p>Контактное лицо:<br />{$vacancy.contact|nl2br}</p>
    </div>
    <div class="right">
        <div class="vacancyText {if $errors}hide{/if}">
            {$vacancy.text}
            <p class="print">
                <a href="{$vacancy_link}?print={$vacancy.id}">Распечатать</a>
            </p>
            <br class="clear" />
        </div>
        <div class="resumeForm {if !$errors}hide{/if}">
            <form id="resume_form" method="post" enctype="multipart/form-data">
                <div class="resume-form">
                    <div class="row">
                        <label class="valid" for="name">Фамилия, имя, отчество (полностью)</label>
                        <input type="text" name="name" id="name" class="name" value="{$smarty.post.name}">
                        {if $errors.name}
                            <div class="error">{$errors.name}</div>
                        {/if}
                    </div>

                    <div class="row">
                        <label class="valid" for="contact">Контактная информация (телефон, эл. почта)</label>
                        <input type="text" name="contact" id="contact" class="contacts" value="{$smarty.post.contact}">
                        {if $errors.contact}
                            <div class="error">{$errors.contact}</div>
                        {/if}
                    </div>

                    <div class="row">
                        <label for="comment">Комментарий</label>
                        <textarea name="comment" id="comment" class="comment">{$smarty.post.comment}</textarea>
                    </div>

                    <div class="row">
                        <label for="file">Резюме (.doc, .docx, .pdf не более 2 Мб)</label>
                        <input type="file" id="file" name="file" class="file-resume" />
                        {if $errors.file}
                            {foreach $errors.file as $error}
                                <div class="error">{$error}</div>
                            {/foreach}
                        {/if}
                    </div>

                    <div class="row">
                        <button type="submit" class="btn-send">Отправить</button>
                    </div>
                    <br class="clear" />
                </div>
            </form>
        </div>
    </div>
    <script type="application/javascript">
        $(function() {
            $('.resume').click(function(e) {
                e.preventDefault();
                if ($(this).text() == 'Отправить резюме') {
                    $(this).text('К открытой вакансии');
                } else {
                    $(this).text('Отправить резюме');
                }
                $('.vacancyText, .resumeForm').toggleClass('hide');
            })
        })
    </script>

</div>