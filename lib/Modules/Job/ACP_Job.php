<?
class ACP_Job extends ACP_Module {

    protected $name = __CLASS__;

    private $folder = 'resumes/';
    private $path;
    private $schedule = array(
        1 => 'Полный день',
        2 => 'Сменный график',
        3 => 'Гибкий график',
    );

    function __construct() {
        parent::__construct();
        $this->path = USER_FILES_DIRECTORY . $this->folder;
        $this->folder = USER_FILES_PATH . $this->folder;
    }

    function rewrite() {

        switch ($this->action) {
            case 'vacancy_edit':
                $this->vacancy_edit();
                break;

            case 'vacancy_delete':
                $this->vacancy_delete();
                return false;
                break;

            case 'resume_delete':
                $this->resume_delete();
                return false;
                break;

            case 'resume_list':
                $this->resume_list();
                break;

            default:
                $this->vacancy_list();
                break;
        }
        return true;
    }

    private function vacancy_list() {
        $airport = Utils::parseGet('airport');
        $sql = "SELECT v.*, count(r.id) as resumes FROM vacancies v LEFT JOIN resumes r ON v.id = r.vacancy_id";
        $params = array();
        if ($airport) {
            $sql .= " WHERE airport_id = ?";
            $params = array($airport);
        }
        $sql .= " GROUP BY v.id";
        $vacancies = $this->db->getAll($sql, $params);
        $airports = $this->db->getAll("SELECT id, title FROM airports");

        $this->smarty->assign('airports', $airports);
        $this->smarty->assign('vacancies', $vacancies);
        $this->controller->body = $this->smarty->fetch($this->tpl_dir.'acp_job_list.tpl');
    }

    private function resume_list()
    {
        $id = Utils::parseGet('vacancy');
        $resumes = array();
        if ($id) {
            $item = $this->db->getRow("SELECT * FROM vacancies WHERE id = ?", array($id));
            $this->smarty->assign('item', $item);
            $resumes = $this->db->getAll("SELECT * FROM resumes WHERE vacancy_id = ?", array($id));
        }

        $this->smarty->assign('resumes', $resumes);
        $this->controller->body = $this->smarty->fetch($this->tpl_dir.'acp_job_resume.tpl');
    }

    private function vacancy_edit() {
        $id = Utils::parseGet('id');
        $data = array();
        if ($id) {
            $data = $item = $this->db->getRow("SELECT * FROM vacancies WHERE id = ?", array($id));
            $this->smarty->assign('item', $item);
        }
        $airports = $this->db->getAll("SELECT id, title FROM airports");

        if (!empty($_POST)) {
            $data['title'] = stripslashes(trim($_POST['title']));
            $data['text'] = stripslashes(trim($_POST['text']));
            $data['preview'] = stripslashes(trim($_POST['preview']));
            $data['salary'] = stripslashes(trim($_POST['salary']));
            $data['schedule'] = stripslashes(trim($_POST['schedule']));
            $data['experience'] = stripslashes(trim($_POST['experience']));
            $data['contact'] = stripslashes(trim($_POST['contact']));
            $data['airport_id'] = stripslashes(trim($_POST['airport_id']));
            $data['enabled'] = stripslashes(trim($_POST['enabled']));
            if (isset($item))
                $this->db->update('vacancies', $data, "id = {$item['id']}");
            else
                $data['id'] = $this->db->insert('vacancies', $data);
            header("Location: /_bo/?doc_id={$_GET['doc_id']}&module");
            exit();
        }

        $this->smarty->assign('airports', $airports);
        $this->smarty->assign('schedule', $this->schedule);
        $this->controller->body = $this->smarty->fetch($this->tpl_dir.'acp_job_edit.tpl');
    }

    private function vacancy_delete() {
        $id = Utils::parseGet('id');
        if ($id) {
            $resumes = $this->db->getAll("SELECT id FROM resumes WHERE vacancy_id = ?", array($id));
            foreach($resumes as $resume)
                $this->delete_resume($resume['id']);
            $this->db->delete('resumes', "vacancy_id = $id");
            $ret = $this->db->delete('vacancies', "id = $id");
        }

        echo json_encode(array('success' => isset($ret) && !PEAR::isError($ret)));
    }

    private function resume_delete() {
        $success = $this->delete_resume(Utils::parseGet('id'));

        echo json_encode(array('success' => $success));
    }

    private function delete_resume($id)
    {
        if ($id) {
            $ret = $this->db->delete('resumes', "id = $id");
            foreach (glob("{$this->path}{$id}.*") as $file) {
                unlink($file);
            }
        }
        return isset($ret) && !PEAR::isError($ret);
    }
}