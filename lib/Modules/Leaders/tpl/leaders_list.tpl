<div class="leaders">
    {if !empty($leaders_list)}
        {foreach $leaders_list as $g}
            <div class="title">{$g.title}</div>
            {foreach $g.items as $item}
            <a href="{$leaders_link}/?n={$item.id}" class="person{if $item@iteration is div by 4} last{/if}">
                <div class="photo">
                    {if !empty($item.img)}
                        <img src="{$item.img}" />
                    {else}
                        <img src="/i/nophoto_person.jpg" />
                    {/if}
                </div>
                <p class="fade-text gray">
                    {$item.lastname}<br />
                    {$item.firstname}<br />
                    {$item.patronymic}
                    <span>&nbsp;</span>
                </p>
                {if !empty($item.position)}<p class="position">{$item.position}</p>{/if}
            </a>
            {if $item@iteration is div by 4}<br class="clear" />{/if}
            {/foreach}
            <br class="clear" /><br /><br />
        {/foreach}

    {else}
        <div><b>Руководителей нет</b></div>
    {/if}
    <br class="clear" />
</div>