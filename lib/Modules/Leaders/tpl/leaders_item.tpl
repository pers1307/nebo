<div class="leaders">
    {if ($leaders_item)}
        <div class="photo">
            {if !empty($leaders_item.img)}
                <img src="{$leaders_item.img}" />
            {else}
                <img src="/i/nophoto_person_big.jpg" />
            {/if}
        </div>
    <div class="info">
        <div class="title">{$leaders_item.lastname} {$leaders_item.firstname} {$leaders_item.patronymic}</div>
        {if !empty($leaders_item.position)}<div class="position">{$leaders_item.position}</div>{/if}
        {$leaders_item.text}
    </div>
    {else}
        <p>Информация о руководителе не найдена</p>
    {/if}
</div>