<div class="padding-block">
    <h3>{if !$smarty.get.id}Создание нового руководителя{else}Редактирование руководителя{/if} {if $item.visible == 0}<span class="label label-info">скрытый<span>{/if}</h3>
    {if !empty($item.parent)}
    <a class="btn btn-info" href="?doc_id={$doc_id}&module&action=list&id={$item.parent}">
        <i class="icon-circle-arrow-left icon-white"></i>
        К списку руководителей
    </a>
    {/if}
    <a class="btn btn-success" href="?doc_id={$doc_id}&module">
        <i class="icon-list icon-white"></i>
        Списк групп
    </a>
</div>

<form method="post" enctype="multipart/form-data" action="?doc_id={$doc_id}&module&action=save" onsubmit="return leaders_form_confirm()"  class="form-horizontal">
    <input type="hidden" name="id" value="{$item.id}" />

    <div class="control-group">
        <label class="control-label" for="lastname">Фамилия</label>
        <div class="controls">
            <input type="text" class="span3" id="lastname" name="lastname" value="{$item.lastname}" autocomplete="off" />
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="firstname">Имя</label>
        <div class="controls">
            <input type="text" class="span3" id="firstname" name="firstname" value="{$item.firstname}" autocomplete="off" />
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="patronymic">Отчество</label>
        <div class="controls">
            <input type="text" class="span3" id="patronymic" name="patronymic" value="{$item.patronymic}" autocomplete="off" />
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="position">Должность</label>
        <div class="controls">
            <input type="text" class="span6" id="position" name="position" value="{$item.position}" autocomplete="off" />
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="position">Отдел</label>
        <div class="controls">
            <select name="group" id="group" value="{$item.parent}">
                <option value="">Не выбрано</option>
                {foreach $group as $g}
                    <option value="{$g.id}" {if $g.id == $item.parent}selected{/if}>{$g.title}</option>
                {/foreach}
            </select>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="full_text">Описание</label>
        <div class="controls">
            <textarea name="full_text" id="full_text">{$item.text}</textarea>
        </div>
    </div>

    {foreach from=$img_sizes item=q key=img_size}
        <div class="control-group">
            <label class="control-label">{$img_titles.$img_size}</label>
            <div class="controls">
                {if ($item.$img_size)}
                    <ul class="thumbnails" id="{$img_size}">
                        <li style="width:180px; margin-left:20px;">
                            <a class="thumbnail">
                                <img onclick="img_preview($(this), '{$item.$img_size}')" src="{$item.$img_size}">
                            </a>
                            <span class="btn btn-mini btn-danger" style="margin-top: 5px;" onclick="leaders_img_delete({$item.id}, '{$img_size}')"><i class="icon-white icon-remove"></i> Удалить изображение</span>
                        </li>
                    </ul>
                {/if}
                <input type="file" name="{$img_size}" id="input_file_{$img_size}" {if ($item.$img_size)}style="display:none"{/if}/>
            </div>
        </div>
    {/foreach}

    <div class="control-group">
        <label class="control-label"></label>
        <div class="controls">
            <div class="row-fluid">
                <ul class="thumbnails" style="margin-right: 20px;"></ul>
            </div>
            <!--div id="files" class="files"></div-->
        </div>
    </div>

    <div class="control-group">
        <div class="controls">
            <button class="btn btn-primary b-save" onclick="submit"><i class="icon-ok icon-white"></i> Сохранить</button>
        </div>
    </div>
</form>

<link rel="stylesheet" href="/_bo/s/ui.datepicker.css">
<link rel="stylesheet" href="/_bo/s/ui.theme.css">
<script type="text/javascript">
    $(function() {
        CKEDITOR.replace('full_text', { "filebrowserBrowseUrl": "\/_bo\/ckfinder\/ckfinder.html", "filebrowserImageBrowseUrl": "\/_bo\/ckfinder\/ckfinder.html?type=Images", "filebrowserFlashBrowseUrl": "\/_bo\/ckfinder\/ckfinder.html?type=Flash", "filebrowserUploadUrl": "\/_bo\/ckfinder\/core\/connector\/php\/connector.php?command=QuickUpload&type=Files", "filebrowserImageUploadUrl": "\/_bo\/ckfinder\/core\/connector\/php\/connector.php?command=QuickUpload&type=Images", "filebrowserFlashUploadUrl": "\/_bo\/ckfinder\/core\/connector\/php\/connector.php?command=QuickUpload&type=Flash", "toolbar": "Text", "width": "100%", "height": "350" });
        var url = '?doc_id={$doc_id}&module&action=upload_images&id={$item.id}';
    });

    function leaders_img_delete(id, imgkey) {
        if (confirm('Удалить изображение?')) {
            $.post('?doc_id='+doc_id+'&module&action=img_delete', { id:id, imgkey:imgkey }, function(data) {
                if (data == 1) {
                    $('#'+imgkey).fadeOut();
                    $('#input_file_'+imgkey).css({ 'display':'block' })
                }
            } )
        }
    }
</script>