<div class="padding-block">
<h3>Список руководителей</h3>
    <a class="btn btn-info" href="?doc_id={$doc_id}&module">
        <i class="icon-circle-arrow-left icon-white"></i>
        К списку групп
    </a>
    <a class="btn btn-success" href="?doc_id={$doc_id}&module&action=edit">
        <i class="icon-plus icon-white"></i>
        Добавить нового
    </a>
</div>
{if !empty($leaders)}
	<table class="table table-striped">
        <thead>
        <tr>
            <th style="width: 30px;">#</th>
            <th>ФИО</th>
            <th style="width: 40px;">&nbsp;</th>
            <th style="width: 80px;">&nbsp;</th>
        </tr>
        </thead>
	 <tbody id="sortable">
     {foreach $leaders as $item}
         <tr id="tr_{$item.id}"{if $item.visible == 0} class="info muted"{/if} data-id="{$item.id}">
             <td><a href="?doc_id={$doc_id}&module&action=edit&id={$item.id}"{if $item.visible == 0} class="info muted"{/if}>{$item.id}</a></td>
             <td><a href="?doc_id={$doc_id}&module&action=edit&id={$item.id}"{if $item.visible == 0} class="info muted"{/if}>{$item.lastname} {$item.firstname} {$item.patronymic}</a></td>
             <td class="center"><span rel="visible" href="#" class="btn btn-mini{if !$item.visible} btn-info{/if}" onclick="return visibleItem({$item.id})"><i class="{if $item.visible}icon-eye-open{else}icon-eye-close icon-white{/if}"></i></span></td>
             <td class="center"><span onclick="leaders_delete('{$item.id}')" class="btn btn-mini btn-danger"><i class="icon-remove icon-white"></i> Удалить</span></td>
         </tr>
     {/foreach}
	 </tbody>
	</table>
{else}
	<div class="padding-block"><p>Руководители не созданы</p></div>
{/if}

<script type="text/javascript">
function leaders_delete(id) {
    var title = '{$item.lastname} {$item.firstname} {$item.patronymic}';
	if (confirm('Удалить "'+title+'"?')) {
		$.post('?doc_id='+doc_id+'&module&action=delete', { id:id }, function(data) {
        	if (data == 1) {
				$('#tr_'+id).fadeOut('fast');
    		}
		} )
	}
}

visibleItem = function(i) {
    $.get('?doc_id={$doc_id}&module&id='+i+'&action=item_visibility', function(r){
        if (r==1) {
	        $('table #tr_'+i+' span[rel=visible]').removeClass('btn-info');
	        $('table #tr_'+i+' span[rel=visible] i').removeClass('icon-eye-close icon-white').addClass('icon-eye-open');
	        $('table #tr_'+i+' ').removeClass('info muted');
            $('table #tr_'+i+' td a').removeClass('muted');
        }
        else {
            $('table #tr_'+i+' span[rel=visible]').addClass('btn-info');
            $('table #tr_'+i+' span[rel=visible] i').addClass('icon-eye-close icon-white').removeClass('icon-eye-open');
            $('table #tr_'+i+' ').addClass('info muted');
            $('table #tr_'+i+' td a').addClass('muted');
        }
    });
}

$("#sortable").sortable({
    helper: function(e, ui) {
        ui.children().each(function() {
            $(this).width($(this).width());
        });
        return ui;
    },
    axis: 'y',
    delay: 100,
    stop: function() {
        var data = { ids:[] };
        $('#sortable tr').each(function(i, el){
            data.ids[i] = $(el).data('id');
        });
        $.post('?doc_id={$doc_id}&module&id={$smarty.get.id}&action=sort', data);
    }
}).disableSelection();
</script>