<div class="videos item">
    <video width="640" height="360" poster="{$item.thumb}" controls="controls" preload="none">
        <source type="video/mp4" src="{$item.video.mp4}" />
        <object width="640" height="360" type="application/x-shockwave-flash" data="flashmediaelement.swf">
            <param name="movie" value="/js/me/flashmediaelement.swf" />
            <param name="flashvars" value="controls=true&file={$item.video.mp4}" />
            <!-- Image as a last resort -->
            <img src="{$item.thumb}" width="640" height="360" title="No video playback capabilities" />
        </object>
    </video>
    <p>{$item.title}</p>
</div>

<script src="/js/me/mediaelement-and-player.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="/js/me/mediaelementplayer.css" />
<script>
    $('video,audio').mediaelementplayer();
</script>