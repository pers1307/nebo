<div class="videos list">
    {if !empty($videos)}
        {if $type != 'news'}
            {foreach $videos as $item}
                <div class="item"{if $item@iteration is div by 2} style="margin-right: 0"{/if}>
                    <a href="{$videos_link}/?n={$item.id}" class="thumb">
                        {if ($item.thumb)}
                            <img src="{$item.thumb}" />
                        {else}
                            <img src="/i/nophoto_news.jpg" />
                        {/if}
                    </a>
                    <p>{$item.title}</p>
                </div>
                {if $item@iteration is div by 2}
                    <br class="clear" />
                {/if}
            {/foreach}
        {else}
            <div class="news-page">
            {foreach $videos as $item}
                <div class="news-list">
                    <div class="image">
                        <a href="{$videos_link}/?n={$item.id}">
                            {if ($item.thumb)}
                                <img src="{$item.thumb}" width="230" />
                            {else}
                                <img src="/i/nophoto_news.jpg" />
                            {/if}
                        </a>
                    </div>
                    <div class="date"></div>
                    <a href="{$videos_link}/?n={$item.id}"><h3>{$item.title}</h3></a>
                </div>
            {/foreach}
            </div>
        {/if}

    {else}
        <div><b>Видео нет</b></div>
    {/if}
</div>