<?
class Video extends Module {

	protected $name = __CLASS__;

	private $folder = 'videos/';
	private $path;
    private $link;
    private $pageSize = 10;

	function __construct() {
		parent::__construct();
		$this->path = USER_FILES_DIRECTORY.$this->folder;
		$this->folder = USER_FILES_PATH.$this->folder;

        $Location = Location::getInstance();
        $this->link = '/'.$Location->first().'/'.$Location->last();
	}

	function getContent() {
        $this->smarty->assign('videos_link', $this->link);

		if (isset($_GET['n'])) {
			$this->show_item();
		} elseif (isset($_GET['t'])) {
			$this->show_list($_GET['t']);
		} else {
            $this->index();
        }

		return true;
	}

    function index() {
        $this->page->doc['head'] = '';
        $this->page->doc['body'] = $this->smarty->fetch($this->tpl_dir.'videos_index.tpl');
    }

	function show_list($type) {
        if (isset($_GET['page']) && $_GET['page'])
            $page = (int)$_GET['page'];
        else
            $page = 1;

        $videos = $this->db->getAll("SELECT * FROM videos WHERE enabled = 1 AND type = ? ORDER BY create_date DESC LIMIT ? OFFSET ?",
            array($type, $this->pageSize, ($page - 1) * $this->pageSize));
        $videosCount = $this->db->getOne("SELECT COUNT(*) FROM videos WHERE enabled = 1 AND type = ?", array($type));
        $paginator = new Paginator($this->pageSize, 'page');
        $paginator->set_total($videosCount);

		if (!PEAR::isError($videos) && !empty($videos)) {
            $that = $this;
            array_walk($videos, function(&$item) use ($that) {
                $item = $that->getFiles($item);
            });
            $this->smarty->assign('videos', $videos);
		}

        if ($type == 'news')  { $this->page->doc['head'] = 'Новостные материалы'; }
        if ($type == 'presentations')  { $this->page->doc['head'] = 'Презентационное видео'; }

        $this->smarty->assign('type', $type);
        $this->smarty->assign('link_back', $this->link);
        $this->smarty->assign('paginator', $paginator);
        $this->page->doc['body'] = $this->smarty->fetch($this->tpl_dir.'videos_list.tpl');
	}

	function show_item() {
		$id = intval($_GET['n']);
		if ($id) {
            $item = $this->db->getRow("SELECT * FROM videos WHERE id = ? AND enabled = 1", array($id));

            if ($item['type'] == 'news')  {
                $this->page->doc['head'] = 'Новостные материалы';
                $this->smarty->assign('link_back', $this->link.'/?t=news');
            }
            if ($item['type'] == 'presentations')  {
                $this->page->doc['head'] = 'Презентационное видео';
                $this->smarty->assign('link_back', $this->link.'/?t=presentations');
            }

            if (!PEAR::isError($item) && is_array($item)) {
                $item = $this->getFiles($item);
                $this->smarty->assign('item', $item);
                $this->page->doc['body'] = $this->smarty->fetch($this->tpl_dir.'videos_item.tpl');
                return;
            }

        }

        header("Location: $this->link");
	}

    public function getFiles($item)
    {
        $thumb = $this->path . $item['id'] . "/video_{$item['id']}.jpg";
        if (file_exists($thumb))
            $item['thumb'] = str_replace(USER_FILES_DIRECTORY, USER_FILES_PATH, $thumb) . '?' . filesize($thumb);

        $mp4 = $this->path . $item['id'] . "/video_{$item['id']}.mp4";
        if (file_exists($mp4))
            $item['video']['mp4'] = str_replace(USER_FILES_DIRECTORY, USER_FILES_PATH, $mp4);

        $flv = $this->path . $item['id'] . "/video_{$item['id']}.flv";
        if (file_exists($flv))
            $item['video']['flv'] = str_replace(USER_FILES_DIRECTORY, USER_FILES_PATH, $flv);

        return $item;
    }
}