<div class="padding-block">
    <h3>Альбом &laquo;{$item_info.title}&raquo;</h3>
    <a class="btn btn-info" href="?doc_id={$doc_id}&module&action=gallery&id={$item_info.id_airport}">
        <i class="icon-circle-arrow-left icon-white"></i>
        к списку альбомов
    </a>
</div>
<div class="padding-block">
    <div class="control-group">
        <div class="ajax-fileuploader-form">
                    <span class="btn btn-success btn-mini fileinput-button">
                        <i class="icon-picture icon-white"></i>
                        <span>Загрузить фото</span>
                        <input id="fileupload" type="file" name="photo" multiple />
                    </span>
            <div id="progress" class="progress">
                <div class="bar"></div>
            </div>
        </div>
    </div>
    <div class="padding-block" style="background: #fff; border: 1px solid #ddd;">
        <div class="row-fluid">

            <ul class="thumbnails" id="sortable_thumb">
                {foreach $photos as $photo}
                    <li class="span3 _img_{$photo.id}"  data-id="{$photo.id}">
                        <div class="thumbnail">
                            <div style="height: 110px; overflow: hidden;"><img src="/u/album/{$photo.id_album}/{$photo.hash}_thumb.{$photo.ext}" data-href="/u/album/{$photo.id_album}/" data-target="{$photo.hash}" data-ext="{$photo.ext}" class="span12"></div>
                            <div class="name span12 overflow-text-white" data-original-title="{$photo.title}"><p style="height: 15px;">{if !empty($photo.title)}{$photo.title}{else}{/if}</p><span style="margin-top: -25px;">&nbsp;</span></div>
                            {*<button type="button" class="btn btn-info btn-mini rotateImage" data-href="?doc_id={$doc_id}&module&action=rotate_photo&photo={$photo}"><i class="icon-repeat icon-white"></i></button>*}
                            <button type="button" class="btn {if ($item_info.hash == $photo.hash)}btn-info{/if} btn-mini mainPhoto" data-href="?doc_id={$doc_id}&module&action=main_photo&album_id={$item_info.id}&photo={$photo.id}"><i class="icon-picture icon-white"></i></button>
                            <button type="button" class="btn btn-warning btn-mini cropImage" data-toggle="modal" data-target="#cropModal" data-href="?doc_id={$doc_id}&module&action=crop_photo&photo={$photo}"><i class="icon-th-large icon-white"></i></button>
                            <button type="button" class="btn btn-warning btn-mini" data-toggle="modal" data-target="#renameModal" onclick="itemId='{$photo.id}'; $('#renameModal #title').val($('._img_{$photo.id} .name').text());"><i class="icon-pencil icon-white"></i></button>
                            <button type="button" class="btn btn-danger btn-mini" onclick="deletePhoto('{$photo.id}');"><i class="icon-remove icon-white"></i></button>
                            <span class="btn btn-mini sort_move"><i class="icon-move"></i></span>
                        </div>
                    </li>
                {/foreach}
            </ul>

        </div>
    </div>

    {* MODAL RENAME *}

    <div id="renameModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Название изображения</h3>
        </div>
        <div class="modal-body">
            <form class="form-inline" id="editAlbumForm">
                <input type="text" name="title" id="title" class="input-block-level" placeholder="Введите название изображения">
                <span class="fl_callback"></span>
            </form>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Отмена</button>
            <button class="btn btn-primary" onclick="renameFromModal();">Сохранить изменения</button>
        </div>
    </div>

    {* MODAL CROP *}

    <div id="cropModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabelCrop" aria-hidden="true" style="width: 800px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabelCrop">Создание миниатюры</h3>
        </div>
        <div class="modal-body text-center"></div>
        <div class="modal-footer text-center">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Отмена</button>
            <button class="btn btn-primary cropSubmit">Сохранить изменения</button>
        </div>
    </div>

    {* END MODAL *}

    <script type="application/javascript">
        $(function() {
            $('#fileupload').fileupload({
                url: '?doc_id={$doc_id}&module&action=upload_photo&id={$item_info.id}',
                dataType: 'json',
                done: function (e, data) {
                    if (data.result.img) {
                        $('<li class="span3 _img_'+data.result.id+'" data-id="'+data.result.id+'"><div class="thumbnail"><div style="height: 110px; overflow: hidden;"><img src="'+data.result.img+'" class="span12"></div>' +
                                '<div class="name span12 overflow-text-white"><p style="height: 15px;"></p><span style="margin-top: -25px;">&nbsp;</span></div>' +
                                {*'<button type="button" class="btn btn-info btn-mini rotateImage" data-href="?doc_id={$doc_id}&module&action=rotate_photo&photo='+data.result.img+'"><i class="icon-repeat icon-white"></i></button>' +*}
                                {*'<button type="button" class="btn btn-info btn-mini cropImage" data-toggle="modal" data-target="#cropModal" data-href="?doc_id={$doc_id}&module&action=crop_photo&photo='+data.result.img+'"><i class="icon-th icon-white"></i></button>' +*}
                                '<button type="button" style="margin-right: 4px;" class="btn btn-mini mainPhoto" data-href="?doc_id={$doc_id}&module&action=main_photo&album_id={$item_info.id}&photo='+data.result.img+'"><i class="icon-picture icon-white"></i></button>' +
                                '<button type="button" style="margin-right: 4px;" class="btn btn-warning btn-mini" data-toggle="modal" data-target="#renameModal" onclick="itemId=\''+data.result.id+'\'; $(\'#renameModal #title\').val($(\'._img_'+data.result.id+' .name\').text());"><i class="icon-pencil icon-white"></i></button>' +
                                '<button type="button" style="margin-right: 4px;" class="btn btn-danger btn-mini" onclick="deletePhoto(\''+data.result.id+'\');"><i class="icon-remove icon-white"></i></button></div></li>').appendTo('.thumbnails');
                    }
                },
                progressall: function (e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('#progress .bar').css('width',progress + '%');
                }
            }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');

            var randomizeUrl = function(url) {
                var parser = document.createElement('a');
                parser.href = url;
                var dt = new Date();
                parser.search = '?'+dt.getTime();
                return parser.href;
            };

            var normalizeUrl = function(url) {
                var parser = document.createElement('a');
                parser.href = url;
                return parser.pathname;
            };

            $(document)
                    .on('click', '.mainPhoto', function() {
                        var self = $(this);
                        $.post(self.data('href'), function(data) {
                            if (data.success) {
                                self.addClass('btn-info');
                                $('.mainPhoto').not(self).removeClass('btn-info');
                            }
                        }, 'json');
                    })
                    .on('click', '.rotateImage', function() {
                        var self = $(this);
                        $.post(self.data('href'), function(data) {
                            if (data.success) {
                                var img = self.closest('li').find('img');
                                img.attr('src', randomizeUrl(img.attr('src')));
                            } else {
                                $('.fl_callback').html(data);
                            }
                        }, 'json');
                    })
                    .on('click', '.cropImage', function() {
                        var self = $(this);
                        var cropModalHeight = 400;
                        var link = self.closest('li').find('img').attr('data-href');
                        var src = self.closest('li').find('img').attr('data-target');
                        var ext = self.closest('li').find('img').attr('data-ext');
                        var newImg = new Image();
                        newImg.src = link+src+'.'+ext;

                        newImg.onload = function() {
                            var $img = $(newImg);
                            var origW = newImg.width;
                            var origH = newImg.height;
                            $img.attr({
                                'width': Math.round(newImg.width*cropModalHeight/newImg.height),
                                'height': cropModalHeight,
                                'data-href': link,
                                'data-target': src,
                                'data-ext': ext
                            });

                            $('#cropModal div.modal-body').empty().append($img);
                            $('#cropModal div.modal-body img').Jcrop({
                                'trueSize': [origW, origH],
                                'aspectRatio': 3/2
                            }, function() {
                                jc = this;
                            });
                        }
                    })
                    .on('click', '.cropSubmit', function() {
                        var select = jc.tellSelect();
                        var link = $('#cropModal div.modal-body img').attr('data-href');
                        var photo = $('#cropModal div.modal-body img').attr('data-target');
                        var ext = $('#cropModal div.modal-body img').attr('data-ext');
                        var self = $(this);
                        $.post('?doc_id={$doc_id}&module&action=crop_photo', { select: select, link: link, photo: photo, ext: ext }, function(data) {
                            if (data.success) {
                                var img = $('img[src="'+photo+'"]');
                                img.attr('src', randomizeUrl(img.attr('src')));
                                $('#cropModal').modal('hide');
                            } else {
                                $('.fl_callback').html(data);
                            }
                        }, 'json');
                    });
        });

        var itemId = '';

        function renameFromModal() {

            $.post('?doc_id={$doc_id}&module&action=photo_rename', { id:itemId, title:$('#renameModal #title').val() }, function(data) {
                if (data == 1) {
                    $('._img_'+itemId+' .name').text($('#renameModal #title').val());
                    $('#renameModal').modal('hide');
                } else {
                    $('.fl_callback').html(data);
                }
            });
        }

        $("#sortable_thumb").sortable({
            helper: function(e, ui) {
                ui.children().each(function() {
                    $(this).width($(this).width());
                });
                return ui;
            },
            items: "> [data-id]",
            handle: ".sort_move",
            stop: function() {
                var data = { ids:[] };
                $('#sortable_thumb li[data-id]').each(function(i, el){
                    data.ids[i] = $(el).data('id');
                });
                $.post('?doc_id={$doc_id}&module&id={$smarty.get.id}&action=sort_thumb_photos', data);
            }
        }).disableSelection();

        function deletePhoto(id) {
            $.post('?doc_id={$doc_id}&module&action=delete_photo', { id:id }, function(data) {
                if (data.success)
                    $('li._img_'+id).fadeOut();
            }, 'json');
        }
    </script>
    <script type="text/javascript" src="/_bo/js/jquery.jcrop.js"></script>
    <link type="text/css" rel="stylesheet" href="/_bo/s/jquery.jcrop.min.css"/>
</div>