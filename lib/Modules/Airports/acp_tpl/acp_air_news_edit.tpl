<div class="padding-block">
    <h3>Редактирование новости {if $news_item.visible == 0}<span class="label label-info">скрытая<span>{/if}</h3>
    <a class="btn btn-info" href="?doc_id={$doc_id}&module&action=news&id={$smarty.get.airport}{if $smarty.get.p}&p={$smarty.get.p}{/if}">
        <i class="icon-circle-arrow-left icon-white"></i>
        к списку новостей
    </a>
</div>


<form id="news-form" method="post" enctype="multipart/form-data" action="?doc_id={$doc_id}&module&action=save_news" class="form-horizontal">
    <input type="hidden" name="id" value="{$news_item.id}" />
    <input type="hidden" name="airport" value="{$smarty.get.airport}" />

    <div class="control-group">
        <label class="control-label" for="title">Заголовок новости</label>
        <div class="controls">
            <input type="text" class="span6" id="title" name="title" value="{$news_item.title}" autocomplete="off" />
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="short_text">Анонс</label>
        <div class="controls">
            <textarea name="short_text" id="short_text" class="span6">{$news_item.short_text}</textarea>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="full_text">Текст новости</label>
        <div class="controls">
            <textarea name="full_text" id="full_text">{$news_item.full_text}</textarea>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="publish_date">Дата публикации</label>
        <div class="controls">
            <input class="datepicker" type="text" name="publish_date" id="publish_date" value="{$news_item.publish_date|date_format:'%d.%m.%Y'}" autocomplete="off" />
        </div>
    </div>

    {foreach from=$img_sizes item=q key=img_size}
        <div class="control-group">
            <label class="control-label">{$img_titles.$img_size}</label>
            <div class="controls">
                {if ($news_item.$img_size)}
                    <ul class="thumbnails" id="{$img_size}">
                        <li style="width:180px; margin-left:20px;">
                            <a class="thumbnail">
                                <img onclick="img_preview($(this), '{$news_item.$img_size}')" src="{$news_item.$img_size}">
                            </a>
                            <span class="btn btn-mini btn-danger" style="margin-top: 5px;" onclick="news_img_delete({$news_item.id}, '{$img_size}')"><i class="icon-white icon-remove"></i> Удалить изображение</span>
                        </li>
                    </ul>
                {/if}
                <input type="file" name="{$img_size}" id="input_file_{$img_size}" {if ($news_item.$img_size)}style="display:none"{/if}/>
            </div>
        </div>
    {/foreach}

    <div class="control-group">
        <label class="control-label"></label>
        <div class="controls">
            <div class="row-fluid">
                <ul class="thumbnails" style="margin-right: 20px;"></ul>
            </div>
            <!--div id="files" class="files"></div-->
        </div>
    </div>

    {if $news_item.id}
        {if $news_item.video.mp4}
            <div class="control-group">
                <label class="control-label">Видео</label>
                <div class="controls">
                    <video width="640" height="360" poster="{$news_item.thumb}" controls="controls" preload="none">
                        <source type="video/mp4" src="{$news_item.video.mp4}" />
                        <object width="640" height="360" type="application/x-shockwave-flash" data="flashmediaelement.swf">
                            <param name="movie" value="/js/me/flashmediaelement.swf" />
                            <param name="flashvars" value="controls=true&file={$news_item.video.mp4}" />
                            <!-- Image as a last resort -->
                            <img src="{$news_item.thumb}" width="640" height="360" title="No video playback capabilities" />
                        </object>
                    </video>
                </div>
            </div>
        {/if}
        <div class="control-group">
            <label class="control-label">Видео</label>
            <div class="controls">
                <span class="help">mp4, flv. Рекомендуемый размер файла не более 50 Мб</span>
                <div class="ajax-fileuploader-form">
                    <span class="btn btn-success btn-mini fileinput-button">
                        <i class="icon-plus icon-white"></i>
                        <span>Выбрать видео</span>
                        <input id="fileupload" type="file" name="video" />
                    </span>
                    <div id="progress" class="progress">
                        <div class="bar"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="title">Время превью</label>
            <div class="controls">
                <input id="maskedinput" type="text" name="time" value="00:00:00" class="span1" />
                <span class="help-inline">Укажите время кадра для превью изображения (час/минута/секунда)</span>
            </div>
        </div>
    {else}
    <div class="control-group">
        <label class="control-label"></label>
        <div class="controls">
            <span class="help">Сохраните новость, чтобы можно было загрузить видео</span>
        </div>
    </div>
    {/if}

    <div class="control-group">
        <label class="control-label"></label>
        <div class="controls">
            <label class="checkbox">
                <input type="checkbox" name="onlist" id="onlist" {if !empty($news_item.onlist)}checked{/if} />
                Вывести в список основных новостей
            </label>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="publish_date"></label>
        <div class="controls">
            <button class="btn btn-primary b-save" type="submit">Сохранить</button>
        </div>
    </div>
</form>

<link rel="stylesheet" href="/_bo/s/ui.datepicker.css">
<link rel="stylesheet" href="/_bo/s/ui.theme.css">
<link rel="stylesheet" href="/js/me/mediaelementplayer.css" />
<script type="application/javascript" src="/_bo/js/jquery.maskedinput.min.js"></script>
<script src="/js/me/mediaelement-and-player.min.js" type="text/javascript"></script>
<script>
</script>
<script type="text/javascript">
    $(function() {
        $('.datepicker').datepicker( { firstDay: 1, dateFormat: 'dd.mm.yy', dayNamesMin: ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'], dayNames: ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье'] } )
        CKEDITOR.replace('full_text', { "filebrowserBrowseUrl": "\/_bo\/ckfinder\/ckfinder.html", "filebrowserImageBrowseUrl": "\/_bo\/ckfinder\/ckfinder.html?type=Images", "filebrowserFlashBrowseUrl": "\/_bo\/ckfinder\/ckfinder.html?type=Flash", "filebrowserUploadUrl": "\/_bo\/ckfinder\/core\/connector\/php\/connector.php?command=QuickUpload&type=Files", "filebrowserImageUploadUrl": "\/_bo\/ckfinder\/core\/connector\/php\/connector.php?command=QuickUpload&type=Images", "filebrowserFlashUploadUrl": "\/_bo\/ckfinder\/core\/connector\/php\/connector.php?command=QuickUpload&type=Flash", "toolbar": "Text", "width": "100%", "height": "350" });

        $.mask.definitions['h'] = '[0-2]';
        $.mask.definitions['m'] = '[0-5]';
        $('#maskedinput').mask('h9:m9:m9');

        $('video,audio').mediaelementplayer();

        $(':file').fileupload({
            url: '?doc_id={$doc_id}&module&action=save_news',
            autoUpload: false,
            dataType: 'json',
            maxChunkSize: 10485760, // 10mb
            add: function (e, data) {
                $('#news-form').submit(function (e) {
                    e.preventDefault();
                    data.submit();
                });
            },
            done: function (e, data) {
                if (data.result) {
                    if(data.result.success)
                        location.reload();
                    else
                        alert(data.result.errors.join('\n'));
                }
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('.bar', '#progress').css('width',progress + '%');
            }
        });
    });

    function news_img_delete(id, imgkey) {
        if (confirm('Удалить изображение?')) {
            $.post('?doc_id='+doc_id+'&module&action=img_delete_news', { id:id, imgkey:imgkey }, function(data) {
                if (data == 1) {
                    $('#'+imgkey).fadeOut();
                    $('#input_file_'+imgkey).css({ 'display':'block' })
                }
            } )
        }
    }
</script>