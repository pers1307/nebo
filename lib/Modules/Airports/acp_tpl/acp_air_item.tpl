<div class="padding-block">
    <h3>Аэропорт &laquo;{$item_info.title}&raquo;</h3>
    <a class="btn btn-info" href="?doc_id={$doc_id}&module">
        <i class="icon-circle-arrow-left icon-white"></i>
        к списку аэропортов
    </a>
</div>
<div class="padding-block">
    <ul class="nav nav-tabs" style="margin-bottom: 0;" id="sortable">
        <li class="active"><a href="#">Об аэропорте</a></li>
        {foreach $item_sections as $link}
            <li data-id="{$link.id}"><a href="?doc_id={$doc_id}&module&action=edit&airport={$link.id_airport}&page={$link.id}"{if $link.visible == 0} class="muted"{/if}>{$link.name}</a></li>
        {/foreach}
        <li><a href="?doc_id={$doc_id}&module&action=gallery&id={$item_info.id}">Фотогалерея</a></li>
        <li><a href="?doc_id={$doc_id}&module&action=news&id={$item_info.id}">Новости</a></li>
    </ul>
    <div class="padding-block" style="background: #fff; border: 1px solid #ddd; border-top: none;">
        <form method="post" enctype="multipart/form-data" action="?doc_id={$doc_id}&module&action=airport_edit" class="form-horizontal">
            <input type="hidden" name="id" value="{$item_info.id}" />
            <div class="control-group">
                <label class="control-label" for="title">Название</label>
                <div class="controls">
                    <input type="text" id="title" name="title" placeholder="Название" value="{$item_info.title}" class="span3">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="full_title">Полное название</label>
                <div class="controls">
                    <input type="text" id="full_title" name="full_title" placeholder="Полное название" value="{$item_info.full_title|htmlspecialchars}" class="span3">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="description">Описание на карту</label>
                <div class="controls">
                    <input type="text" id="description" name="description" placeholder="Краткое описание" value="{$item_info.description}" class="span5">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="description_main">Описание на главную</label>
                <div class="controls">
                    <input type="text" id="description_main" name="description_main" placeholder="Краткое описание в слайдер на главную" value="{$item_info.description_main}" class="span5">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="contacts">Контакты</label>
                <div class="controls">
                    <input type="text" id="contacts" name="contacts" placeholder="Контакты аэропорта" value="{$item_info.contacts}" class="span5">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="email">E-mail</label>
                <div class="controls">
                    <input type="text" id="email" name="email" placeholder="E-mail аэропорта" value="{$item_info.email}" class="span5">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="url">Сайт</label>
                <div class="controls">
                    <input type="text" id="url" name="url" placeholder="www.name.ru" value="{$item_info.url}" class="span5">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="flag">Направление флага</label>
                <div class="controls">
                    <select name="flag" id="flag">
                        <option value="right"{if $item_info.flag == 'right'} selected{/if}>Направо</option>
                        <option value="left"{if $item_info.flag == 'left'} selected{/if}>Налево</option>
                    </select>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="file_main">Фото - главная</label>
                <div class="controls">
                    <input type="file" id="file_main" name="file_main" class="span5">
                    {if !empty($img_main)}
                        <div><img src="{$img_main}" width="150" /></div>
                    {/if}
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="file_map">Фото - карта</label>
                <div class="controls">
                    <input type="file" id="file_map" name="file_map" class="span5">
                    {if !empty($img_map)}
                        <div><img src="{$img_map}" width="150" /></div>
                    {/if}
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="emails">E-mail адреса для рассылки, через запятую</label>
                <div class="controls">
                    <textarea id="emails" name="emails">{$item_info.emails}</textarea>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="city">Город</label>
                <div class="controls">
                    <input type="text" id="city" name="city" value="{$item_info.city}" class="span5">
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    <button type="submit" class="btn">Сохранить изменения</button>
                </div>
            </div>
        </form>
    </div>

</div>
<script type="application/javascript">
    $("#sortable").sortable({
        helper: function(e, ui) {
            ui.children().each(function() {
                $(this).width($(this).width());
            });
            return ui;
        },
        axis: "x",
        items: "> [data-id]",
        delay: 100,
        stop: function() {
            var data = { ids:[] };
            $('#sortable li[data-id]').each(function(i, el){
                data.ids[i] = $(el).data('id');
            });
            $.post('?doc_id={$doc_id}&module&id={$smarty.get.id}&action=sort_sections', data);
        }
    }).disableSelection();
</script>