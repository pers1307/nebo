<div class="padding-block">
    <h3>Список аэропортов</h3>
    <a class="btn btn-success" href="?doc_id={$doc_id}&module&action=airport_add">
        <i class="icon-plus icon-white"></i>
        Добавить аэропорт
    </a>

    <a class="btn btn-warning" href="#" data-toggle="modal" data-target="#editModal">
        <i class="icon-map-marker icon-white"></i>
        Редактировать карту
    </a>
</div>
{if !empty($airports)}
    <table class="table table-striped">
        <thead>
        <tr>
            <th style="width: 20px;">#</th>
            <th>Название аэропорта</th>
            <th style="width: 40px;">&nbsp;</th>
            <th style="width: 90px;">&nbsp;</th>
        </tr>
        </thead>
        <tbody id="sortable">
        {foreach $airports as $item}
            <tr id="tr_{$item.id}"{if $item.visible == 0} class="info muted"{/if} data-id="{$item.id}">
                <td><a href="?doc_id={$doc_id}&module&action=airport&id={$item.id}"{if $item.visible == 0} class="info muted"{/if}>{$item.id}</a></td>
                <td><a href="?doc_id={$doc_id}&module&action=airport&id={$item.id}"{if $item.visible == 0} class="info muted"{/if}>{$item.title}</a></td>
                <td class="center"><span rel="visible" href="#" class="btn btn-mini{if !$item.visible} btn-info{/if}" onclick="return visibleItem({$item.id})"><i class="{if $item.visible}icon-eye-open{else}icon-eye-close icon-white{/if}"></i></span></td>
                <td class="center"><span onclick="news_delete('{$item.id}','{$item.title}')" class="btn btn-mini btn-danger"><i class="icon-remove icon-white"></i> Удалить</span></td>
            </tr>
        {/foreach}
        </tbody>
    </table>

{else}
    <div class="padding-block"><p>Аэропорты не созданы</p></div>
{/if}

<div id="editModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="width: 1215px; height: 705px; margin: -365px 0 0 -615px;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Карта аэропортов</h3>
    </div>
    <div class="modal-body" style="max-height: 970px;">
        {foreach $points as $item}
            <span data-airid="{$item.id}" style="position: absolute; color: #FFF; background: #0353a5 url(/_bo/i/target.gif) no-repeat; padding: 5px; opacity: .7; top: {(($item.position[0] + 250) * 0.7)|replace:',':'.'}px; left: {(($item.position[1] + 185) * 0.615)|replace:',':'.'}px;">{$item.title}</span>
        {/foreach}
        <select name="airport" id="airport" style="display: none; position: absolute;">
            <option value="">Выберите аэропорт</option>
            {foreach $airports as $item}
                <option value="{$item.id}">{$item.title}</option>
            {/foreach}
        </select>
        <img class="map-image" src="/i/map_admins_airports.jpeg" alt="map"  style="width: 1215px; height: 565px;"/>
    </div>
    <div class="modal-footer">
        <p class="pull-left">Для изменения положения точки на карте, щелкните в новом месте и выберите аэропорт, который нужно переместить.</p>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Закрыть</button>
    </div>
</div>

<script type="text/javascript">
    function news_delete(id, title) {
        if (confirm('Удалить "'+title+'"?')) {
            $.post('?doc_id='+doc_id+'&module&action=airport_delete', { id:id }, function(data) {
                if (data == 1) {
                    $('#tr_'+id).fadeOut('fast');
                }
            } )
        }
    }

    visibleItem = function(i) {
        $.get('?doc_id={$doc_id}&module&id='+i+'&action=item_visibility', function(r){
            if (r==1) {
                $('table #tr_'+i+' span[rel=visible]').removeClass('btn-info');
                $('table #tr_'+i+' span[rel=visible] i').removeClass('icon-eye-close icon-white').addClass('icon-eye-open');
                $('table #tr_'+i+' ').removeClass('info muted');
                $('table #tr_'+i+' td a').removeClass('muted');
            }
            else {
                $('table #tr_'+i+' span[rel=visible]').addClass('btn-info');
                $('table #tr_'+i+' span[rel=visible] i').addClass('icon-eye-close icon-white').removeClass('icon-eye-open');
                $('table #tr_'+i+' ').addClass('info muted');
                $('table #tr_'+i+' td a').addClass('muted');
            }
        });
    }
    
    $(function(){
        $(document)
            .on('click', '.map-image', function(e) {
                var offset_t = $(this).offset().top - $(window).scrollTop();
                var offset_l = $(this).offset().left - $(window).scrollLeft();

                var left = Math.round( (e.clientX - offset_l) );
                var top = Math.round( (e.clientY - offset_t) );

                $('#airport').css({
                    display: 'block',
                    top: top + 64,
                    left: left + 15
                }).data('top', top).data('left', left);
            })
            .on('change', '#airport', function() {
                var self = $(this),
                    top  = Math.round( ((self.data('top') + 64) / 0.7) - 250),
                    left = Math.round( ((self.data('left') + 15) / 0.615) - 185);

                    var data = {
                        id: self.val(),
                        map_position: top + ',' + left
                    };

                if (data.id)
                    $.post('?doc_id={$doc_id}&module&action=edit_map', data, function(res) {
                        if (res.success) {
                            self.val('').hide();
                            $('[data-airid="'+data.id+'"]').remove();
                            $('<span data-airid="'+data.id+'"></span>')
                                .text($('option[value="'+data.id+'"]', '#airport').text())
                                .css({
                                    'color': '#FFF',
                                    'background': '#0353a5 url(/_bo/i/target.gif) no-repeat',
                                    'padding': '5px',
                                    'opacity': '.7',
                                    'position': 'absolute',
                                    'top': (self.data('top') + 64),
                                    'left': (self.data('left') + 15)
                                })
                                .prependTo('.modal-body');
                        }
                    }, 'json');
            });

        $("#sortable").sortable({
            helper: function(e, ui) {
                ui.children().each(function() {
                    $(this).width($(this).width());
                });
                return ui;
            },
            axis: 'y',
            delay: 100,
            stop: function() {
                var data = { ids:[] };
                $('#sortable tr').each(function(i, el){
                    data.ids[i] = $(el).data('id');
                });
                $.post('?doc_id={$doc_id}&module&action=sort', data);
            }
        }).disableSelection();
    })
</script>