<div class="points">
    {foreach $airports as $airport}
        <div class="point-airport {$airport.flag}" style="top: {$airport.position[0]+168}px; left: {if $airport.flag == 'right'}{$airport.position[1] - 15}{else}{$airport.position[1] - 168}{/if}px;">
            <div class="label custom-clip">
                <a href="/{$airport_link}/{$airport.id}/{$airport.section}/">
                    <div class="_point-wrapper">
                        <span class="title">{$airport.title}</span>
                        <div class="info" style="display: none">
                            <img src="{$airport.map}" />
                            <p>{$airport.description}</p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="tail"></div>
        </div>
    {/foreach}
</div>

<img src="/i/map_airports.jpg" class="map-image" />