<div class="gallery inner">
    <div class="title">{$album.title} <a href="/airports/{$airport.id}/gallery" class="back">назад</a></div>
    {foreach $photos as $photo}
        <div class="photo{if $photo@iteration is div by 4} last{/if}">
            <a href="/u/album/{$photo.id_album}/{$photo.hash}.{$photo.ext}" rel="gallery[pp_gal]">
                {if !empty($photo.title)}<div class="title-img">{$photo.title}</div>{/if}
                <img src="/u/album/{$photo.id_album}/{$photo.hash}_thumb.{$photo.ext}" alt="{$photo.title}" />
            </a>
        </div>

    {/foreach}
    <br class="clear" />
</div>
<script type="application/javascript">
    $(function(){
        createPrettyPhoto();
    });

    $(document).on('mouseover', '.photo', function(){
        $(this).find('.title-img').css('display','block');
    });

    $(document).on('mouseout', '.photo', function(){
        $(this).find('.title-img').css('display','none');
    });

</script>