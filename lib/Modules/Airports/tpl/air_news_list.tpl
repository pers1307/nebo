<div class="news-page" style="margin-top: 12px">
    {if !empty($news_list)}
        {foreach from=$news_list item=news_item}
            <div class="news-list">
                <div class="image">
                    <a href="{$news_link}/?n={$news_item.id}">
                        {if !empty($news_item.img_src)}
                            <img src="{$news_item.img_src}" />
                        {else}
                            <img src="/i/nophoto_news.jpg" />
                        {/if}
                    </a>
                </div>
                <div class="date">{$news_item.publish_date|date_format:'%d.%m.%Y'}</div>
                <a href="{$news_link}/?n={$news_item.id}"><h3>{$news_item.title}</h3></a>
                <div class="anounce">{$news_item.short_text|truncate:200}</div>
            </div>
        {/foreach}

        <div class="pagination">
            {if ($pages.count > 1)}
                {section name=pages loop=$pages.count start=0}
                    {assign var=pi value="`$smarty.section.pages.iteration - 1`"}
                    <a href="{$news_link}/{if ($pi|intval > 0)}?p={$pi}{/if}" class="pager{if ($pi == $pages.current)} active{/if}" >{$pi+1}</a>
                {/section}
            {/if}
        </div>

    {else}
        <div><b>Новостей нет</b></div>
    {/if}
</div>