﻿<?
class Airports extends Module {

    protected $name = __CLASS__;

    public $airport;
    public $section;

    private $limit_news = 10;

    private $link_news;
    private $folder_news = 'news/';
    private $path_news;

    function getContent(){
        $Location = Location::getInstance();
        $link = $Location->first();
        $this->smarty->assign('airport_link', $link);
        $this->airport = (int)$Location->get(1);
        $this->section = $Location->get(2);
        $this->path_news = USER_FILES_DIRECTORY.$this->folder_news;
        $this->folder_news = USER_FILES_PATH.$this->folder_news;
        $this->link_news = '/'.$link.'/'.$this->airport.'/'.$this->section;

        if (!empty($this->airport)) {
            if ($this->section == 'gallery') {
                if (!empty($_GET['album']))
                    $this->show_album();
                else
                    $this->show_gallery();
            } elseif ($this->section == 'news'){
                if (!empty($_GET['n']))
                    $this->show_news();
                else
                    $this->list_news();
            } else {
                $this->section = (int)$this->section;
                $this->show_item();
            }
        } else {
            $this->show_map();
        }
        return true;
    }

    function get_menu_lv2($airport, $items) {
        $items[] = array(
            'id' => 'gallery',
            'id_airport' => $this->airport,
            'name' => 'Фотогалерея',
            'sort' => '9'
        );
        $items[] = array(
            'id' => 'news',
            'id_airport' => $this->airport,
            'name' => 'Новости',
            'sort' => '0'
        );
        if (empty($this->section)) {
            $item = reset($items);
            header("Location: /airports/{$airport['id']}/{$item['id']}/");
            exit();
        }
        foreach ($items as $i) {
            $menu2 = array(
                'fullpath'=>'/airports/'.$i['id_airport'].'/'.$i['id'],
                'id'=>$i['id'],
                'head'=>$i['name'],
                'active'=>0,
                'sort'=>$i['sort']
            );
            if (!empty($this->section) && $i['id'] == $this->section) {
                $menu2['active'] = 1;
            }
            $this->page->doc['menu_lv2'][] = $menu2;
        }
        function sort_menu_lv2($a, $b) {
            return strcmp($a["sort"], $b["sort"]);
        }
        usort($this->page->doc['menu_lv2'], 'sort_menu_lv2');
    }

    function show_map() {
        $this->page->doc['head'] = '';
        $airports = $this->get_main_info();
        $this->smarty->assign('airports', $airports);
        $this->page->doc['body'] = $this->smarty->fetch($this->tpl_dir.'air_map.tpl');
    }

    function show_item() {
        $airport = $this->db->getRow("SELECT * FROM airports WHERE id = ?", array($this->airport)); //Выбираем активный аэропорт
        $items = $this->db->getAll("SELECT * FROM airports_section WHERE id_airport = ? AND visible ORDER BY sort", array($this->airport)); //Выбираем нужную страницу

        if (!PEAR::isError($airport) && !empty($airport)) $this->page->doc['head'] = $airport['title'];
        else $this->page->doc['head'] = '';

        $item = array();
        if (!PEAR::isError($items) && !empty($items)) {
            $items[] = array(
                'id' => 'gallery',
                'id_airport' => $this->airport,
                'name' => 'Фотогалерея',
                'sort' => '9'
            );
            $items[] = array(
                'id' => 'news',
                'id_airport' => $this->airport,
                'name' => 'Новости',
                'sort' => '0'
            );
            if (empty($this->section)) {
                $item = reset($items);
                header("Location: /airports/{$airport['id']}/{$item['id']}/");
                exit();
            }
            foreach ($items as $i) {
                $menu2 = array(
                    'fullpath'=>'/airports/'.$i['id_airport'].'/'.$i['id'],
                    'id'=>$i['id'],
                    'head'=>$i['name'],
                    'active'=>0,
                    'sort'=>$i['sort']
                );
                if (!empty($this->section) && $i['id'] == $this->section) {
                    $item = $i;
                    $menu2['active'] = 1;
                }
                $this->page->doc['menu_lv2'][] = $menu2;
            }
            function sort_menu_lv2($a, $b) {
                return strcmp($a["sort"], $b["sort"]);
            }
            usort($this->page->doc['menu_lv2'], 'sort_menu_lv2');
        }

        $gal = $this->section;

        /* Image Page */
        $img_head_name = $this->page->doc['id'] . '_' . $item['id'] . '_air';
        $this->page->doc['img_head'] = str_replace(ROOT, URL, Utils::findImage($img_head_name, ROOT.'u/tree/'));



        $this->smarty->assign('item', $item);
        $this->smarty->assign('airport', $airport);
        $this->smarty->assign('gal', $gal);
        $this->page->doc['body'] = $this->smarty->fetch($this->tpl_dir.'air_item.tpl');
    }

    function show_gallery() {
        $airport = $this->db->getRow("SELECT * FROM airports WHERE id = ?", array($this->airport)); //Выбираем активный аэропорт
        $items = $this->db->getAll("SELECT * FROM airports_section WHERE id_airport = ? AND visible ORDER BY sort ASC", array($this->airport)); //Выбираем нужную страницу
        $albums = $this->db->getAll("SELECT al.*, aph.hash, aph.ext, aph.on_main FROM airport_albums AS al LEFT JOIN airport_photos AS aph ON al.id = aph.id_album AND aph.on_main = 1 WHERE al.id_airport = {$this->airport} ORDER BY sort");

        if (!PEAR::isError($items) && !empty($items)) {
            $this->get_menu_lv2($airport, $items);
        }

        if (!PEAR::isError($airport) && !empty($airport))
            $this->page->doc['head'] = $airport['title'];

        $this->smarty->assign('item', 'gallery');
        $this->smarty->assign('airport', $airport);
        $this->smarty->assign('albums', $albums);
        $this->page->doc['body'] = $this->smarty->fetch($this->tpl_dir.'air_gallery.tpl');
    }

    public function show_album()
    {
        $airport = $this->db->getRow("SELECT * FROM airports WHERE id = ?", array($this->airport)); //Выбираем активный аэропорт
        $items = $this->db->getAll("SELECT * FROM airports_section WHERE id_airport = ? AND visible ORDER BY sort ASC", array($this->airport)); //Выбираем нужную страницу
        $album = $this->db->getRow("SELECT * FROM airport_albums WHERE id = ?", array($_GET['album']));
        $photos = $this->db->getAll("SELECT * FROM airport_photos WHERE id_album = {$_GET['album']} ORDER BY sort");

        if (!PEAR::isError($items) && !empty($items)) {
            $this->get_menu_lv2($airport, $items);
        }

        if (!PEAR::isError($airport) && !empty($airport))
            $this->page->doc['head'] = $airport['title'];

        $this->smarty->assign('item', 'album');
        $this->smarty->assign('airport', $airport);
        $this->smarty->assign('album', $album);
        $this->smarty->assign('photos', $photos);
        $this->page->doc['body'] = $this->smarty->fetch($this->tpl_dir.'air_gallery_album.tpl');
    }

    function get_main_info() {
        $main_airport_info = $this->db->getAll("
        SELECT a.id, a.title, a.description, a.description_main, a.map_position, a.flag, a.visible, ais.id as section FROM airports a
        LEFT JOIN airports_section ais ON ais.id_airport = a.id AND ais.visible
        WHERE a.visible = 1 AND a.map_position <> 0 GROUP BY a.id ORDER BY a.sort ASC, ais.sort ASC");

        if (is_array($main_airport_info)) {

            foreach ($main_airport_info as &$airport_info) {
                $airport_info['map'] = $this->_getPhoto($airport_info['id'], 'map');
                $airport_info['main'] = $this->_getPhoto($airport_info['id'], 'main');
                $airport_info['position'] = explode(',', $airport_info['map_position']);
            }
        }

        return (is_array($main_airport_info)) ? $main_airport_info : false;
    }

    private function _getPhoto($id, $type)
    {
        if (file_exists(USER_FILES_DIRECTORY . "airports/{$type}_{$id}.jpg"))
            return USER_FILES_PATH . "airports/{$type}_{$id}.jpg";
    }

    function list_news() {
        $airport = $this->db->getRow("SELECT * FROM airports WHERE id = ?", array($this->airport)); //Выбираем активный аэропорт
        $items = $this->db->getAll("SELECT * FROM airports_section WHERE id_airport = ? AND visible ORDER BY sort ASC", array($this->airport)); //Выбираем нужную страницу

        $p = (isset($_GET['p'])) ? intval($_GET['p']) : 0;

        $count = $this->db->getOne('SELECT COUNT(*) FROM news WHERE airport_id = ?', array($this->airport));
        $limit_from =  $this->limit_news * $p;
        $count = ceil($count / $this->limit_news);
        $this->smarty->assign('pages', array('current'=>$p, 'count'=>$count));

        $news_list = $this->db->getAll("SELECT * FROM news WHERE airport_id = ? AND visible = 1 ORDER BY publish_date DESC LIMIT ?, ?", array($this->airport, $limit_from, $this->limit_news));
        if (!PEAR::isError($news_list) && !empty($news_list)) {
            foreach ($news_list as $k=>$row) {
                $url = $row['id'].'_list.jpg';
                if (file_exists($this->path_news . $url))
                    $news_list[$k]['img_src'] = $this->folder_news . $url;
            }
        }

        if (!PEAR::isError($items) && !empty($items)) {
            $this->get_menu_lv2($airport, $items);
        }

        if (!PEAR::isError($airport) && !empty($airport))
            $this->page->doc['head'] = $airport['title'];

        $this->smarty->assign('item', 'news');
        $this->smarty->assign('news_list', $news_list);
        $this->smarty->assign('news_link', $this->link_news);
        $this->page->doc['body'] = $this->smarty->fetch($this->tpl_dir.'air_news_list.tpl');
    }

    function show_news() {
        $id = intval($_GET['n']);
        if (!$id) {
            $this->page->doc['body'] = $this->smarty->fetch($this->tpl_dir.'news_item.tpl');
            return;
        }
        $airport = $this->db->getRow("SELECT * FROM airports WHERE id = ?", array($this->airport)); //Выбираем активный аэропорт
        $items = $this->db->getAll("SELECT * FROM airports_section WHERE id_airport = ? AND visible ORDER BY sort ASC", array($this->airport)); //Выбираем нужную страницу
        $item = $this->db->getRow("SELECT * FROM news WHERE id = $id");

        if (!is_array($item) or !count($item)) {
            $this->page->doc['top_body'] = $this->smarty->fetch($this->tpl_dir.'news_top_item.tpl');
            $this->page->doc['body'] = $this->smarty->fetch($this->tpl_dir.'news_item.tpl');
            return;
        }

        foreach (array('left', 'right') as $type) {
            $url = $item['id']."_{$type}.jpg";
            if (file_exists($this->path_news . $url)) {
                $item["img_{$type}_src"] = $this->folder_news . $url;
            }
        }
        $item['video'] = array();
        foreach (glob($this->path_news . "{$item['id']}_video.*") as $file) {
            $type = substr($file, -3);
            $link = str_replace($this->path_news, $this->folder_news, $file);
            if ($type == 'jpg')
                $item['thumb'] = $link;
            else
                $item['video'][$type] = $link;
        }

        if (!PEAR::isError($items) && !empty($items)) {
            $this->get_menu_lv2($airport, $items);
        }

        if (!PEAR::isError($airport) && !empty($airport))
            $this->page->doc['head'] = $airport['title'];

        $this->smarty->assign('item', 'news');
        $this->smarty->assign('link_back', $this->link_news);
        $this->smarty->assign('news_item', $item);
        $this->page->doc['body'] = $this->smarty->fetch($this->tpl_dir.'air_news_item.tpl');
    }
}
