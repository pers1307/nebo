{if (($doc.editable != '1') && ($user_status != 'admin'))}
    <b style="color:red">Редактирование данного раздела запрещено!</b>
{else}
    {include file="tabs.tpl"}
    <div class="dinamic-edit" id="bo_body">
        <input type="hidden" name="doc_id" id="doc_id" value="{$doc.id}">

        {* Text *}
        <div id="de_text" class="dinamic-edit-item">
            <textarea name="body_top" id="body_top">{$doc.body_top}</textarea>

            {* image *}
            <div class="control-group padding-block">

                <div class="ajax-fileuploader-form"{if !empty($doc.img_src)} style="display: none;"{/if}>
                    <span class="btn btn-success btn-mini fileinput-button">
                        <i class="icon-picture icon-white"></i>
                        <span>Изображение страницы</span>
                        <input id="fileupload" type="file" name="file" />
                    </span>
                    <div id="progress" class="progress">
                        <div class="bar"></div>
                    </div>
                </div>

                <div id="images" class="ajax-fileuploader-img"{if empty($doc.img_src)} style="display: none;"{/if}>
                    <ul class="thumbnails" id="img" style="margin-left: 0;">
                        {if !empty($doc.img_src)}
                            <li>
                                <a class="thumbnail"><img src="{$doc.img_src}"></a>
                                <span class="btn btn-mini btn-danger" style="margin-top: 5px;" onclick="top_img_delete('{$doc.parent}_{$doc.id}', this)"><i class="icon-white icon-remove"></i> Удалить изображение</span>
                            </li>
                        {/if}
                    </ul>
                </div>
            </div>
            {* image *}

            <textarea name="body" id="body">{$doc.body}</textarea>
        </div>

        {* SEO *}
        <div id="de_seo" class="dinamic-edit-item" style="display: none;">
            <table class="edit-page">
                <tr>
                    <td valign="top" width="160"><label for="title">Заголовок браузера:</label></td>
                    <td>
                        <a href="#" tabindex="-1" rel="tooltip" title="Заголовок браузера - показывается в заголовке окна браузера. Рекомендуемая длина заголовка: 70-80 символов и 12 слов."><img src="i/icon_quest.png"/></a>
                    </td>
                    <td><input type="text" name="title" id="title" class="txt-field" value="{$doc.title}" autocomplete="off"/></td>
                </tr>
                <tr>
                    <td valign="top" width="160"><label for="title_h1">Заголовок H1:</label></td>
                    <td><a href="#" tabindex="-1" class="tooltip" title="Заголовок страницы - показывается на странице в тэге H1"><img src="i/icon_quest.png"/></a></td>
                    <td><input type="text" name="title_h1" id="title_h1" class="txt-field" value="{$doc.title_h1}" autocomplete="off"/></td>
                </tr>
                <tr>
                    <td>
                        <label for="meta_description">Meta description</label>
                        <div class="info">Описание страницы</div>
                    </td>
                    <td>
                        <a href="#" tabindex="-1" class="tooltip" title="Описание страницы - выводится в результатах поиска в поисковых системах. Рекомендуется вводить не более 200 символов."><img src="i/icon_quest.png"/></a>
                    </td>
                    <td>
                        <textarea name="meta_description" id="meta_description" rows="2" class="txt-field forcounter">
                            {$doc.meta_description}
                        </textarea>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="meta_keywords">Meta keywords</label>
                        <div class="info">Ключевые слова страницы</div>
                    </td>
                    <td>
                        <a href="#" tabindex="-1" class="tooltip" title="Список слов, встречающихся на данной странице, перечисленных через запятую - позволяет более точно индексировать страничку поисковыми системами"><img src="i/icon_quest.png"/></a>
                    </td>
                    <td>
                        <textarea name="meta_keywords" id="meta_keywords" rows="3" class="txt-field">
                            {$doc.meta_keywords}
                        </textarea>
                    </td>
                </tr>
                <tr>
                    <td><label for="changefreq">Частота сканирования:</label></td>
                    <td><a href="#" tabindex="-1" class="tooltip" title="Значение «всегда» для страниц, которые изменяются при каждом доступе. Значение «никогда» для архивных URL-адресов."><img src="i/icon_quest.png"/></a></td>
                    <td>
                        <select name="changefreq" id="changefreq" class="select-field">
                            {foreach from=$changefreq item=chfrq_val key=chfrq_key}
                                <option {if ($chfrq_key == $doc.changefreq)}selected="selected"{/if} value="{$chfrq_key}">{$chfrq_val}</option>
                            {/foreach}
                        </select>
                    </td>
                </tr>
                <tr>
                    <td><label for="sitemap">Sitemap XML:</label></td>
                    <td>&nbsp;</td>
                    <td style="padding-bottom: 10px;">
                        <input type="checkbox" name="sitemap" id="sitemap" {if ($doc.sitemap)}checked="checked"{/if} />
                        <span class="info2">Если снять галочку, то страница не будет отображаться в Sitemap XML</span>
                    </td>
                </tr>
            </table>
            <br/>
        </div>

        {* Options *}
        <div id="de_options" class="dinamic-edit-item" style="display: none;">
            <table class="edit-page">
                <tr>
                    <td valign="top" width="160">
                        <label>Название страницы:</label>
                        <div class="info">Не более 30 символов</div>
                    </td>
                    <td><a href="#" tabindex="-1" class="tooltip" title="Заголовок страницы"><img src="i/icon_quest.png"/></a></td>
                    <td><input type="text" name="head" id="head" value="{$doc.head}" size="80" class="txt-field" autocomplete="off"/></td>
                </tr>
                {if ($user_status == 'admin')}
                    <tr>
                        <td><label>URL:</label></td>
                        <td><a href="#" tabindex="-1" class="tooltip" title="пишется в адресной строке и должно состоять только из английских букв, цифр, дефиса и подчеркивания"><img src="i/icon_quest.png"/></a></td>
                        <td class="form-inline" style="padding-bottom: 10px;">
                            <input type="text" name="fname" id="fname" value="{$doc.fname}" autocomplete="off" size="80" class="txt-field">
                            <a href="{$doc.uri}" tabindex="-1" target="_blank" class="btn btn-small btn-info"><i class="icon-file icon-white"></i> Открыть страницу на сайте</a>
                        </td>
                    </tr>
                    <tr>
                        <td><label>Перенаправление:</label></td>
                        <td>&nbsp;</td>
                        <td>
                            <input type="text" name="redirect" id="redirect" value="{$doc.redirect}" size="80" class="txt-field" autocomplete="off"/>

                            {function name=treeOptRedirect level=0}
                                {foreach $data as $tree_item}
                                    <option {if ($tree_item.path == $doc.fullpath)}disabled="disabled"{/if} {if ($doc.redirect == $tree_item.path)}selected="selected"{/if}
                                            value="{$tree_item.path}">{section name=margin loop=$level start=0}-&nbsp;{/section}{$tree_item.name}</option>
                                    {if ($tree_item.childs)}
                                        {call name=treeOptRedirect data=$tree_item.childs level=$level+1}
                                    {/if}
                                {/foreach}
                            {/function}
                            <select class="select-field" onchange="$('input#redirect').val($(this).val())">
                                <option {if (!$doc.id)}selected="selected"{/if} value=""> -</option>
                                <option {if ($doc.redirect == '/')}selected="selected"{/if} value="/">[Корень]</option>
                                {call name=treeOptRedirect data=$_tree level=1}
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td><label>Модуль:</label></td>
                        <td></td>
                        <td class="form-inline" style="padding-bottom: 10px;">
                            <select name="module" id="selcet_module" class="select-field">
                                <option value="">Стандартный обработчик</option>
                                {if (!empty($modules))}
                                    {foreach from=$modules item=m_item key=m_key}
                                        <option value="{$m_key}"
                                                {if ($m_key == $doc.module)}selected="selected"{/if}>{$m_item.name}</option>
                                    {/foreach}
                                {/if}
                            </select>
                            <button class="btn btn-small btn-info" type="button" onclick="refresh_modules()">
                                <i class="icon-refresh icon-white"></i> Обновить модули
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="menushow">Отображать в меню:</label></td>
                        <td></td>
                        <td style="padding-bottom: 10px;">
                            <input type="checkbox" name="menushow" id="menushow" {if ($doc.menushow)}checked="checked"{/if} />
                            <span class="info2">Если снять галочку, то страница останется доступной по прямой ссылке</span>
                        </td>
                    </tr>

                    {*
                        <tr>
                            <td>Доступ:</td>
                            <td></td>
                            <td><input type="checkbox" name="editable" id="editable" {if ($doc.editable)}checked="checked"{/if} /><label for="editable">Разрешить редактирование</label></td>
                        </tr>
                    *}
                {/if}
            </table>
            <br/>
        </div>
    </div>

    {* System Panel Bottom *}
    <div class="container-fluid system-panel">
        <div class="container">
            <div class="row">
                <div class="span3">
                    {if ($user_status == 'admin')}<button class="btn btn-info" onclick="$('#add_page').slideToggle()">Новая страница</button>{/if}
                </div>
                <div class="span7">
                    <button class="btn btn-primary b-save" onclick="saveDoc('text')" id="spb_text">Сохранить страницу</button>
                    <button class="btn btn-primary b-save" onclick="saveDoc('seo')" id="spb_seo" style="display: none;">Сохранить изменения</button>
                    <button class="btn btn-primary b-save" onclick="saveDoc('options')" id="spb_options"style="display: none;">Сохранить настройки</button>
                    {if ($user_status == 'admin')}
                        <button class="btn btn-danger b-delete" type="button" name="delete" onClick="del({$doc.id})">
                            Удалить страницу
                        </button>
                    {/if}
                </div>
                <div class="span2 sys-info">Последние изменения: {$doc.mtime}</div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        var doc_id = '{$doc.id}';

        CKEDITOR.replace('body_top', { "filebrowserBrowseUrl": "\/_bo\/ckfinder\/ckfinder.html", "filebrowserImageBrowseUrl": "\/_bo\/ckfinder\/ckfinder.html?type=Images", "filebrowserUploadUrl": "\/_bo\/ckfinder\/core\/connector\/php\/connector.php?command=QuickUpload&type=Files", "filebrowserImageUploadUrl": "\/_bo\/ckfinder\/core\/connector\/php\/connector.php?command=QuickUpload&type=Images", "toolbar": "Top_Text", "width": "100%", "height": "150" });
        CKEDITOR.replace('body', { "filebrowserBrowseUrl": "\/_bo\/ckfinder\/ckfinder.html", "filebrowserImageBrowseUrl": "\/_bo\/ckfinder\/ckfinder.html?type=Images", "filebrowserFlashBrowseUrl": "\/_bo\/ckfinder\/ckfinder.html?type=Flash", "filebrowserUploadUrl": "\/_bo\/ckfinder\/core\/connector\/php\/connector.php?command=QuickUpload&type=Files", "filebrowserImageUploadUrl": "\/_bo\/ckfinder\/core\/connector\/php\/connector.php?command=QuickUpload&type=Images", "filebrowserFlashUploadUrl": "\/_bo\/ckfinder\/core\/connector\/php\/connector.php?command=QuickUpload&type=Flash", "toolbar": "Text", "width": "100%", "height": "450" });


        $.router(/(.+)$/, function (m, str) {
            var exist = $('#de_' + str).hasClass('dinamic-edit-item')
            if (exist) {
                $('.dinamic-edit-item').hide()
                $('#de_' + str).show()
                $('.system-panel-buttons').hide()
                $('button.b-save').hide()
                $('#spb_' + str).show()
                $('.page-tabs a').removeClass('act')
                $('.page-tabs a#pt_' + str).addClass('act')
            }
        });

        $(document).ready(function () {
            function countCharacters(el) {
                var charNum = $(el).val().length;
                $(el).next().text(charNum);
            }

            $('#fileupload').fileupload({
                url: '?doc_id=' + doc_id + '&action=top_img_upload',
                dataType: 'json',
                done: function (e, data) {
                    if (data.result.img) {
                        $('<li><a class="thumbnail"><img src="' + data.result.img + '"></a>' +
                                '<span class="btn btn-mini btn-danger" style="margin-top: 5px;" onclick="top_img_delete(\'{$doc.parent}_{$doc.id}\', this)"><i class="icon-white icon-remove"></i> Удалить изображение</span></li>').appendTo('.thumbnails');
                        $('.ajax-fileuploader-form').css({ 'display': 'none' });
                        $('#images').css({ 'display': 'block' });
                    }
                    if (data.result.success == false) {
                        alert(data.result.error)
                    }

                },
                progressall: function (e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('#progress .bar').css('width', progress + '%');
                }
            }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');

        });

        function top_img_delete(id, t) {
            if (confirm('Удалить изображение?')) {
                $.post('?doc_id=' + doc_id + '&action=top_img_delete', { id: id }, function (data) {
                    if (data == 1) {
                        $(t).parents('li').fadeOut(function(){ $(t).parents('li').remove(); });
                        $('.ajax-fileuploader-form').css({ 'display': 'block' })
                    }
                });
            }
        }
    </script>
{/if}
