<div class="page-area">
 <form method="post" action="?action=settings" enctype="multipart/form-data" id="bo_body" class="form-horizontal">
 <input type="hidden" name="act" value="saveSettings"/>
  {foreach from=$sitevars item=var_item}
  <div class="control-group">
   <label class="control-label" for="{$var_item.name|stripslashes}">{if ($var_item.title)}{$var_item.title}{else}{$var_item.name}{/if}</label>
   
   <div class="controls">
   {if ($var_item.type == 'str')}
    <input type="text" name="{$var_item.name|stripslashes}" id="{$var_item.name|stripslashes}" value="{$var_item.value}" class="span5" />
   {elseif ($var_item.type == 'int')}
    <input type="text" name="{$var_item.name|stripslashes}" id="{$var_item.name|stripslashes}" value="{$var_item.value|intval}" class="input-xlarge" autocomplete="off" onkeypress="return digit_only(event)" />
   {elseif ($var_item.type == 'text')}
    <textarea id="{$var_item.name}" name="{$var_item.name}" rows="2" class="span5">{$var_item.value}</textarea>
   {elseif ($var_item.type == 'html')}
    <textarea id="{$var_item.name}" name="{$var_item.name}" class="input-xlarge">{$var_item.value|stripslashes}</textarea>
   {/if}
   {if ($var_item.description)}<a href="#" rel="popover" title="{$var_item.title}" data-content="{$var_item.description}" data-trigger="hover" class="quest-tooltip"><i class="icon-question-sign"></i></a>{/if}
   </div>
  </div>
  {/foreach}
  <button type="submit" name="save" class="btn btn-primary" >Сохранить изменения</button>  
 </form>
</div>
{literal}
<script type="text/javascript">
//<![CDATA[
window.CKEDITOR_BASEPATH = '/_bo/ckeditor/'
//]]>
</script>
<script type="text/javascript">
$(document).ready(function() {
{/literal}
  {foreach from=$sitevars item=var_item}
    {if ($var_item.type == 'html'  && $var_item.name != 'footer_text')}
      CKEDITOR.replace('{$var_item.name}', { "filebrowserBrowseUrl": "\/_bo\/ckfinder\/ckfinder.html", "filebrowserImageBrowseUrl": "\/_bo\/ckfinder\/ckfinder.html?type=Images", "filebrowserFlashBrowseUrl": "\/_bo\/ckfinder\/ckfinder.html?type=Flash", "filebrowserUploadUrl": "\/_bo\/ckfinder\/core\/connector\/php\/connector.php?command=QuickUpload&type=Files", "filebrowserImageUploadUrl": "\/_bo\/ckfinder\/core\/connector\/php\/connector.php?command=QuickUpload&type=Images", "filebrowserFlashUploadUrl": "\/_bo\/ckfinder\/core\/connector\/php\/connector.php?command=QuickUpload&type=Flash", "toolbar": "Text", "width": "100%", "height": "200" });
    {else if ($var_item.type == 'html' && $var_item.name == 'footer_text' )}
      CKEDITOR.replace('{$var_item.name}', { "toolbar": "Info", "width": "100%", "height": "100" });
    {/if}
  {/foreach}
  $(".collapse").collapse()
  $('.controls').popover()
  $("a[rel=popover]")
      .popover()
{literal}
})
</script>
{/literal}