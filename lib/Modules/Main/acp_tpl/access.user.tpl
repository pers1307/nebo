<a href="?action=access"><b style="font-size: 10pt;">← Все пользователи</b></a>
<br />
<br />
{if ($u)}
    <h1>{$u.login}{if ($u.email)} ({$u.email}){/if}</h1>
    <p>
        <span class="ajx" onclick="$('#user_data').slideToggle()">Параметры пользователя</span>
        <div id="user_data" style="display:none">
            {include file="access.user_data.tpl"}
        </div>
    </p>
    <p>
        <span class="ajx" onclick="$('#user_access_div').slideToggle()">Доступ к разделам сайта</span>
        <div id="user_access_div">
            {$access_div}
        </div>
    </p>
    <p>
        <span class="ajx" onclick="$('#user_access_mod').slideToggle()">Доступ к разделам бэкофиса</span>
        <div id="user_access_mod" style="display:none">
            <form action="?action=saveUserModules" method="POST" id="user_modules">
                <input type="hidden" name="login" value="{$u.login}" />
                {foreach from=$modules item=module_data key=module_name}
                    <input type="checkbox" name="mod[]" value="{$module_name}" {if in_array($module_name, $user_modules)}checked="checked"{/if} />{$module_data.name}<br/>
                {/foreach}
                <p><input type="submit" value="Сохранить"/></p>
            </form>
        </div>
    </p>
{else}
    Пользователь не найден
{/if}