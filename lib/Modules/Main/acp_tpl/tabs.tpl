<div class="page-tabs rounded-top">
 <ul>
  <li class="space-tab t-center"><i class="icon-question-sign icon-white"></i></li>
  <li class="tab"><a id="pt_text" class="act" href="?doc_id={$doc.id}#text">Текст</a></li>
  <li class="tab"><a id="pt_seo" href="?doc_id={$doc.id}#seo">SEO</a></li>
  <li class="tab"><a id="pt_options" href="?doc_id={$doc.id}#options">Настройки</a></li>
  <li class="tab"><a id="pt_module" href="?doc_id={$doc.id}&module" {if (!$doc.acp_module)}style="display: none;"{/if}>Модуль</a></li>
  <li class="tab"><a href="{$doc.fullpath}" id="site_page_link" target="_blank"><img src="/_bo/i/a_blank_fff.gif" title="Посмотреть на сайте" alt="Посмотреть на сайте" style="margin: 2px 2px 1px 2px;" /></a></li>
 </ul> 
</div>
