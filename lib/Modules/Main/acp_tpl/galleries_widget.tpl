<table class="acp_files widget" id="galleries_widget" style="width: 600px;">
{if ($fl_folder.id)}
    <tr>
        <td colspan="5" style="padding-bottom: 3px;">
            <img src="/_bo/images/folder_16.png" /> <span title="{$fl_folder.title}">{$fl_folder.short_title}</span>
            &nbsp;
            <img src="images/cat_tree_up.gif" />
            <span class="ajx" onclick="CKEDITOR.tools.callFunction(ck_get_gallerylink, {$fl_folder.parent_id}, '{$fl_ck_name}')">На уровень выше</span>
        </td>
    </tr>
{/if}

{if ($fl_galleries)}
	{foreach from=$fl_galleries item=file}
        <tr>
            <td class="td-title" colspan="3">
	            <input type="hidden" id="fl_link_{$file.id}" value="{$file.full_link}" />
	            <input type="hidden" id="fl_title_{$file.id}" value="{$file.title}" />
	            <input type="hidden" id="fl_ext_{$file.id}" value="{$file.ext}" />
                <span class="ajx icon-folder-open" onclick="ck_insert_gallery({$file.id}, '{$fl_ck_name}')">{$file.title_short}</span>
            </td>
            <td class="td-checkbox">
                Изображений: {$file.children}
            </td>
            <td class="td-date">{$file.create_date|date_format:'%d.%m.%Y  %H:%M'}</td>
        </tr>
	{/foreach}
{/if}
</table>