<div class="padding-block">
    <h3>Редактирование баннера</h3>
    <a class="btn btn-info" href="/_bo/?action=mainpage">
        <i class="icon-circle-arrow-left icon-white"></i>
        назад
    </a>
</div>

<form method="post" enctype="multipart/form-data" action="/_bo/?action=mainpage&act=edit_banner&id={$item.id}" class="form-horizontal">
    <div class="control-group">
        <label class="control-label" for="title">Заголовок</label>
        <div class="controls">
            <input type="text" class="span6" id="title" name="title" value="{$item.title}" autocomplete="off" />
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="short_text">Ссылка</label>
        <div class="controls">
            <input type="text" name="link" id="link" class="span6" value="{$item.link}" />
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="full_text">Текст</label>
        <div class="controls">
            <textarea name="text" id="text">{$item.text}</textarea>
        </div>
    </div>

    <div class="control-group">
        <div class="controls">
            <label class="control-label" for="important">
                <input type="checkbox" name="important" id="important" value="{$item.important}" {if ($item.important)}checked="checked"{/if} autocomplete="off" />
                Важно
            </label>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label">Фото</label>
        <div class="controls">
            <input type="file" name="banner" id="banner" />
            <span class="help-block">Соотношение сторон должно быть 4:3</span>
            {if ($item.thumb)}
                <div class="thumbnail" style="width: 200px; margin-top: 20px;">
                    <img src="{$item.thumb}">
                </div>
            {/if}
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="publish_date"></label>
        <div class="controls">
            <button class="btn btn-primary b-save" onclick="submit">Сохранить</button>
        </div>
    </div>
</form>