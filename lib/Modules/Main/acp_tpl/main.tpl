<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="author" content="Vivo Group" />
	<meta name="robots" content="noindex, nofollow" />
	<title>Welcome Vivo Content</title>

    <link rel="icon" href="/i/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="/i/favicon.ico" type="image/x-icon">

	<link href="/_bo/s/bootstrap.css" rel="stylesheet">
	<link href="/_bo/s/bootstrap-responsive.css" rel="stylesheet">
	<link href="/_bo/s/design.css" rel="stylesheet">

	<script src="/_bo/js/jquery-1.10.2.min.js" type="text/javascript"></script>
	<script src="/_bo/js/ui-jquery-1.8.23.min.js" type="text/javascript"></script>
	<script src="/_bo/js/bo.js" type="text/javascript"></script>
	<script src="/_bo/js/jquery.router.js" type="text/javascript"></script>
	<script src="/_bo/js/bootstrap.js" type="text/javascript"></script>
	<script src="/_bo/js/files.js" type="text/javascript"></script>
	<script src="/_bo/ckeditor/ckeditor.js" type="text/javascript"></script>
	<script src="/_bo/ckeditor/adapters/jquery.js" type="text/javascript"></script>

    <script src="/_bo/js/fileupload/load-image.min.js" type="text/javascript"></script>
    <script src="/_bo/js/fileupload/jquery.ui.widget.js" type="text/javascript"></script>
    <script src="/_bo/js/fileupload/jquery.fileupload.js" type="text/javascript"></script>
    <script src="/_bo/js/fileupload/jquery.fileupload-process.js" type="text/javascript"></script>
    <script src="/_bo/js/fileupload/jquery.fileupload-image.js" type="text/javascript"></script>
	<script type="text/javascript">
        var doc_id = {if ($doc.id)}{$doc.id}{else}false{/if};
        $(function(){
            $('*[title]').tooltip()
        });
    </script>
</head>
<body>
    <div class="container">
        <div class="row">

            {* Logo CMS *}
            <div class="span3 logo">
                <a href="/_bo/" class="logo_bo"></a>
            </div>

            {* Navigation Top *}
            <div class="span9 top_nav">
                <a href="?action=logout" class="menulink logout last"><img src="/_bo/i/logout.gif" alt="&rarr;]" title="Выход" /></a>
                <span class="separate"></span>

                {foreach from=$top_menu item=tm_item}
                    <a href="/_bo/{$tm_item.url}" class="menulink{if ($module && ($module == $tm_item.name))} active{/if}">{$tm_item.head}</a>
                    <span class="separate"></span>
                {/foreach}

                {if ($user_status == 'admin')}
                    <a href="#add_page" onclick="$('#add_page').slideToggle()" class="menulink left first"><i class="icon-file icon-white"></i></a>
                    <span class="separate left"></span>
                    <a href="#refresh_modules" onclick="refresh_modules()" title="Обновить модули" alt="Обновить модули" class="menulink left"><i class="icon-refresh icon-white"></i></a>
                    <span class="separate left"></span>
                {else}
                    <span class="menulink left first">&nbsp;</span>
                {/if}

                <span class="doc-head">{if ($doc.head)}{$doc.head}{/if}</span>
            </div>
        </div>


        <div class="row content-block">
            <div class="span3">

                {* Tree *}
                <div class="tree-site disable_sort">
                    {if $_tree}
                        {function name=tree level=0 tree_parent_id=0}
                            {if ($data)}
                                <div class="nav-collapse">
                                    <ul class="drop_here" id="tree_parent_{$tree_parent_id}">
                                        {foreach $data as $tree_item}
                                            <li class="movable" id="tree_item_{$tree_item.id}"><i class="icon-marrow icon-tree"></i><span class="drag"><i class="icon-move"></i></span>
                                            <a class="{if ($doc.id == $tree_item.id)}active{/if} {if (!$tree_item.menushow)}menu_hide{/if} {if ($tree_item.module)}module{/if}" href="?doc_id={$tree_item.id}{if ($tree_item.acp_module)}&module{/if}">{$tree_item.name}</a>
                                            {call name=tree data=$tree_item.childs level=$level+1 tree_parent_id=$tree_item.id}
                                            </li>
                                        {/foreach}
                                    </ul>
                                </div>
                            {/if}
                        {/function}
                        {call name=tree data=$_tree}
                    {/if}

                    {if ($user_status == 'admin')}
                        <p style="color:#80BAF1;" class="t-center tree-edit">[ <span class="ajx" onclick="toggle_sortet()">редактировать дерево</span> ]</p>
                    {/if}

                {* Add Page *}
                {if ($user_status == 'admin')}
                    <div id="add_page" class="rounded-top">
                        <form method="POST" action="?action=createDoc">
                            <table>
                                <tr>
                                    <td colspan="3"><h1>Создать новую страницу</h1></td>
                                </tr>
                                <tr>
                                    <td>Название:</td>
                                    <td><input type="text" name="new_head" class="txt-field" value="" autocomplete="off" /></td>
                                    <td><a href="#" tabindex="-1" class="tooltip" title="Заголовок страницы. Может состоять как из английских, так и из русских букв и символов"><img src="i/icon_quest.png" /></a></td>
                                </tr>
                                <tr>
                                    <td>URL:</td>
                                    <td><input type="text" name="new_fname" class="txt-field" value="" autocomplete="off" /></td>
                                    <td><a href="#" tabindex="-1" class="tooltip" title="пишется в адресной строке и должно состоять только из английских букв, цифр, дефиса и подчеркивания"><img src="i/icon_quest.png" /></a></td>
                                </tr>
                            <tr>
                                <td>Родитель:</td>
                                <td>
                                    <select name="parent" class="select-field">
                                        <option {if (!$doc.id)}selected="selected"{/if} value="0">[Корень]</option>
                                        {function name=treeOpt level=0}
                                            {foreach $data as $tree_item}
                                                <option {if ($doc.id == $tree_item.id)}selected="selected"{/if} value="{$tree_item.id}">{section name=margin loop=$level start=0}-&nbsp;{/section}{$tree_item.name}</option>
                                                {if ($tree_item.childs)}
                                                    {call name=treeOpt data=$tree_item.childs level=$level+1}
                                                {/if}
                                            {/foreach}
                                        {/function}
                                        {call name=treeOpt data=$_tree}
                                    </select>
                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>
                                    <input type="submit" name="save" value="Создать" class="b-create" />&nbsp;&nbsp;
                                    <span class="ajx" onclick="$('#add_page').fadeOut()">Отмена</span>
                                </td>
                                <td><br /><br /></td>
                            </tr>
                            </table>
                        </form>
                    </div>
                {/if}

            </div>
        </div>

        {* Content *}
        <div class="span9">
            <div class="content-page">{$body}</div>
        </div>
    </div>
</div>

{* Messages Callback *}
<div class="alert callback"></div>

</body>
</html>