<div class="w995">
 <h1>Ошибка 404</h1>
 <p>Страница не найдена. Её больше не существует, а может, никогда и не существовало.</p>

 <h3>Воспользуйтесь картой сайта:</h3>
 <div class="sitemap">

  {function name=sitemap level=0}
   <ul>
     {foreach $data as $tree_item}
       <li><a href="{$tree_item.path}">{$tree_item.name}</a>
       {if ($tree_item.childs)}
         {call name=sitemap data=$tree_item.childs level=$level+1}
       {/if}
       </li>
     {/foreach}
   </ul>
   {/function}
   {call name=sitemap data=$sitemap}

</div>

</div>