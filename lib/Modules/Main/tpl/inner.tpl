{include file="header.tpl"}

    <div class="wrapper-content">
        <div class="content">
            {* Контент над картинкой *}
            {if !empty($doc.body_top)}
                <div class="top-text">{$doc.body_top}</div>
            {/if}

            {* Картинка раздела *}
            {if !empty($doc.img_head)}
                <div class="image-inner-head"><img src="{$doc.img_head}" /></div>
            {/if}




            {* Заголовок страницы *}
            {if !empty($doc.head)}
                <h1>{$doc.head}</h1>
            {/if}

            {* Контент *}
            {if $doc.fname == "contacts"}
                <div id="mapzg" style="width: 100%; height: 500px;"></div>
                <script src="http://api-maps.yandex.ru/2.0-stable/?load=package.full&lang=ru-RU" type="text/javascript"> </script>
                <script type="text/javascript">
                    ymaps.ready(init);
                    function init() {
                        var myMap = new ymaps.Map("mapzg", {
                            center: [55.730617, 37.628147],
                            zoom: 17
                        });

                        // Элементы управления
                        myMap.controls.add('smallZoomControl'); //масштаб
                        //myMap.controls.add('trafficControl'); //пробки


                        // Создаем метку.
                        myMap.balloon.open([55.730738, 37.6281477], '{$vars.map_text|strip}', {
                            closeButton: false
                        });
                    }
                </script>
            {/if}
            {$doc.body}



            {if $doc.module == 'Job'}
                {$doc.job}
            {/if}
        </div>
    </div>
    <div class="sub-footer"></div>
</div>


<script type="text/javascript">
    $(document).on('mouseenter', '.file', function(){
        $(this).children('.hover').css('display','block');
    });
    $(document).on('mouseleave', '.file', function(){
        $(this).children('.hover').css('display','none');
    });
</script>


{include file="footer.tpl"}
