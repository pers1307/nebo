<div class="footer">
    <div class="content">
        <div class="right">
            <div class="social-button">
                <a href="https://www.facebook.com/AirportsofRegions"><img src="/i/facebook-icon.png" alt="Facebook page" /></a>
            </div>
            <p>Создано в Tengo</p>
        </div>
        <div class="left">
            <a href="http://www.renova.ru/" target="_blank"><img src="/i/logo_renova.jpg" class="logo-renova" /></a>
            {$vars.footer_text}
        </div>

        {if $doc.module == 'Airports' && $airport}
            <div class="contacts-airport">
                <p>
                    {if $airport.contacts}
                        {$airport.contacts}
                    {/if}
                    {if $airport.email}
                        e-mail: <a href="mailto:{$airport.email}">{$airport.email}</a>
                    {/if}
                    {if $airport.url}
                        <a target="_blank" href="http://{$airport.url}">{$airport.url}</a>
                    {/if}
                </p>
            </div>
        {/if}
    </div>
</div>

{*
{literal}
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-59391587-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter28256121 = new Ya.Metrika({
                    id:28256121,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/28256121" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
{/literal}
*}
</body>
</html>
