<!DOCTYPE html>
<html>
<head>
    <title>{$vars.name_site}</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="{$doc.meta_description}" />
    <meta name="keywords" content="{$doc.meta_keywords}" />

    <link rel="icon" href="/i/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="/i/favicon.ico" type="image/x-icon">

    {* Base scripts *}
    <script type="text/javascript" src="/js/jquery.1.7.2.min.js"></script>
    <script type="text/javascript" src="/js/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="/js/jquery.prettyPopin.js"></script>
    <script type="text/javascript" src="/js/document.ready.js"></script>

    {* Custom scripts *}
    <script type="text/javascript" src="/js/jquery.flexslider.js"></script>
    <script type="text/javascript" src="/js/jquery.actual.min.js"></script>
    <script type="text/javascript" src="/js/jqueryui-datepicker.min.js"></script>

    {* Base Styles *}
    <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700&subset=latin,cyrillic-ext" />
    <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=PT+Sans+Caption:400,700&subset=latin,cyrillic-ext" />
    <link type="text/css" rel="stylesheet" href="/s/style.css" />
    <link type="text/css" rel="stylesheet" href="/s/prettyPhoto.css" />
    <link type="text/css" rel="stylesheet" href="/s/prettyPopin.css" />

    {* Custom Styles *}
    <link type="text/css" rel="stylesheet" href="/s/flexslider.css" />
    <link type="text/css" rel="stylesheet" href="/s/jqueryui-datepicker.css" />
	
<!-- Yandex.Metrika counter --> <script type="text/javascript" > (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter49064465 = new Ya.Metrika({ id:49064465, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks"); </script> <noscript><div><img src="https://mc.yandex.ru/watch/49064465" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->

</head>
<body{if $doc.module == 'Airports' && !$item} class="map-airports"{/if}>
<script type="text/javascript">

    $(function() {

        $('.navigation ul li ul').hide().removeClass('hide');
        $('.navigation ul li').hover(
                function () {
                    $('ul', this).stop().slideDown(100);
                },
                function () {
                    $('ul', this).stop().slideUp(100);
                }
        );

        $('.menu-lv2 .menu-lv3').hide().removeClass('hide');
        $('.menu-lv2 li').hover(
                function () {
                    $('.menu-lv3', this).stop().slideDown(100);
                },
                function () {
                    $('.menu-lv3', this).stop().slideUp(100);
                }
        );

        {if !($_path_link|@count)}
            $('.airports .item p').hide().removeClass('hide');
            $('.airports .item').hover(
                    function () {
                        $('p', this).stop().fadeIn(200);
                    },
                    function () {
                        $('p', this).stop().fadeOut(200);
                    }
            );
            $('.main-image-slider').flexslider({
                animation: "fade",
                controlNav: false,
                slideshow: true,
                animationLoop: true,
                slideshowSpeed: 5000
            });

            $('.flexslider').flexslider({
                animation: "slide",
                animationLoop: false,
                itemWidth: 233,
                itemMargin: 1,
                minItems: 4
            });
        {/if}

        {if $doc.module == 'Airports'}
            $('.point-airport').each(function(){
                $(this).css({
                    height: $(this).siblings('._point-wrapper').actual('width')
                });

                /*
                var tail = $('<div class="tail"></div>');
                $('.tail').css({ top: parseInt($(this).css('top'))+25, left: $(this).css('left') });
                $(this).after(tail);
                */
            });

            $(document).on('mouseenter', '._point-wrapper', function(){

                var parentPoint       = $(this).parents('.point-airport');
                var parentPointHeight = $(parentPoint).css('top');
                parentPointHeight     = parseInt(parentPointHeight);

                $(this).closest('.point-airport').css('z-index','100');
                $(this).children('.info').fadeIn(200);

                var infoHeight = $(this).children('.info').height();
                infoHeight += 65;

                if (parentPointHeight > 300) {
                    $(this).stop().animate({ top: ($(this).actual('height')-25)*-1 }, 200);
                } else {
                    $(this).parents('.label').addClass('clip-transform');
                    $(parentPoint).children('.tail').hide();
                }
            });
            $(document).on('mouseleave', '._point-wrapper', function(){

                var parentPoint       = $(this).parents('.point-airport');
                var parentPointHeight = $(parentPoint).css('top');
                parentPointHeight     = parseInt(parentPointHeight);

                $(this).closest('.point-airport').css('z-index','1');
                $(this).children('.info').fadeOut(200);

                if (parentPointHeight > 300) {
                    $(this).stop().animate({ top:0 }, 200);
                } else {
                    $(this).parents('.label').removeClass('clip-transform');
                    setTimeout(function() {
                        $(parentPoint).children('.tail').show();
                    }, 200);
                } 
            });
        {/if}
    });

</script>
<div class="wrapper{if !($_path_link|@count)} main-bg{/if}">
    <div {if !($_path_link|@count)}class="header-main"{else}class="header-inner"{/if}>
        <div class="navigation">
            <ul>
                {foreach $doc.menu_lv1 as $i}
                    <li>
                        <a href="{$i.fullpath}" {if $doc.tree_selected.0==$i.id}class="active"{/if}>{$i.head|stripslashes}</a>
                        {if !empty($i.childs)}
                        <ul class="hide">
                            {foreach $i.childs as $im}
                                <li>
                                    <a href="{$im.fullpath}">{$im.head|stripslashes}</a>
                                    {if !empty($im.childs)}
                                        <ul class="hide">
                                            {foreach $im.childs as $imn}
                                                <li><a href="{$imn.fullpath}">{$imn.head|stripslashes}</a></li>
                                            {/foreach}
                                        </ul>
                                    {/if}
                                </li>
                            {/foreach}
                        </ul>
                        {/if}
                    </li>
                {/foreach}
            </ul>
            <div class="search-panel" style="margin-left: 306px;">
                <form method="get" enctype="application/x-www-form-urlencoded" action="/search/">
                    <input type="text" name="q" id="q" placeholder="Поиск" />
                    <button type="submit"></button>
                </form>
            </div>
            <div class="lang" style="margin-left:255px">
                <!--a href="#" class="flag ru"></a-->
                <a href="http://en.ar-management.ru{$location.string}" class="flag en"></a>
            </div>
        </div>
        {if !($_path_link|@count)}
            <div class="header-content">
                <div class="logo">
                    <a href="/">Ведущий аэропортовый холдинг России</a>
                </div>
                {if !empty($main_news)}
                <div class="news">
                    <div class="title">{$main_news.title}</div>
                    <p>{$main_news.short_text}</p>
                </div>
                {if empty($main_news.airport_id)}
                    <a href="/pressroom/news/?n={$main_news.id}" class="news-btn">Подробнее</a>
                {else}
                    <a href="/airports/{$main_news.airport_id}/news/?n={$main_news.id}" class="news-btn">Подробнее</a>
                {/if}
                {/if}
            </div>
            <div class="main-image-slider flexslider"{if empty($headers)} style="background: #ccc;"{/if}>
                <ul class="slides">
                    {if !empty($headers)}
                        {foreach $headers as $header}
                            <li><img src="/u/header/{$header.hash}.{$header.ext}" /></li>
                        {/foreach}
                    {else}
                        <li>&nbsp;</li>
                    {/if}
                </ul>
            </div>
            <div class="clear"></div>
        {else}
            <div class="header-content" {if empty($doc.menu_lv2)}style="margin-bottom:52px;"{/if}>
                <div class="logo">
                    <a href="/">Ведущий аэропортовый холдинг России</a>
                </div>
                {if !empty($doc.menu_lv2)}
                    <ul class="menu-lv2">
                        {if ($link_back)}
                            <li class="back"><a href="{$link_back}">Назад</a></li>
                        {/if}
                        {if $doc.module == 'Airports'}
                            <li class="back"><a href="{$doc.fullpath}">Карта</a></li>
                        {/if}
                        {foreach $doc.menu_lv2 as $i}
                            <li>
                                <a href="{$i.fullpath}" {if ($doc.tree_selected.1 == $i.id || $i.active)}class="active"{/if} >{$i.head|stripslashes}</a>
                                {if $i.module == 'Presskit'}
                                    <ul class="menu-lv3">
                                        {foreach $doc.menu_lv3 as $i3}
                                            <li><a href="{$i3.fullpath}">{$i3.head}</a></li>
                                        {/foreach}
                                    </ul>
                                {/if}
                            </li>
                        {/foreach}
                    </ul>
                {/if}
            </div>
        {/if}
    </div>
