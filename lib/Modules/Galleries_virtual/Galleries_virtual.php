<?
class Galleries_virtual extends Module {

    protected $name = __CLASS__;

	function getContent() {
        $id = (int) $_GET['id'];
        if (empty($id)) return false;

        $gal = new Galleries();
        $gal->front($id);

        echo $this->smarty->fetch($this->tpl_dir.'galleries_front.tpl');
        exit();
	}

}
?>