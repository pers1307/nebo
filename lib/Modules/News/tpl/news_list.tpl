<div class="news-page">
    <div class="left-col">
        {if !empty($news_list)}
            {foreach from=$news_list item=news_item}
                <div class="news-list">
                    <div class="image">
                        <a href="{$news_link}/?n={$news_item.id}">
                            {if !empty($news_item.img_src)}
                                <img src="{$news_item.img_src}" />
                            {else}
                                <img src="/i/nophoto_news.jpg" />
                            {/if}
                        </a>
                    </div>
                    <div class="date">{$news_item.publish_date|date_format:'%d.%m.%Y'}</div>
                    <a href="{$news_link}/?n={$news_item.id}"><h3>{$news_item.title}</h3></a>
                    <div class="anounce">{$news_item.short_text|truncate:200}</div>
                </div>
            {/foreach}

            <div class="pagination">
                {if ($pages.count > 1)}
                    {section name=pages loop=$pages.count start=0}
                        {assign var=pi value="`$smarty.section.pages.iteration - 1`"}
                        <a href="{$news_link}/{if ($pi|intval > 0)}?p={$pi}{if $date}&d={$date}{/if}{/if}{if $date}?d={$date}{/if}" class="pager{if ($pi == $pages.current)} active{/if}" >{$pi+1}</a>
                    {/section}
                {/if}
            </div>

        {else}
            <div><b>Новостей нет</b></div>
        {/if}
    </div>
    <div class="right-col">
        <a href="{$news_link}" class="btn-all-news">Все новости</a>
        <div id="datepicker"></div>
    </div>
</div>

<script type="text/javascript">
    {if $alldates}var selectable = {$alldates};{/if}
    $('#datepicker').datepicker({
        firstDay: 0,
        dateFormat: 'dd.mm.yy',
        monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
        dayNamesMin: ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'],
        dayNames: ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье'],
        showWeek: false,
        onSelect: function(v) {
            document.location.href = (document.location.href.indexOf('d=')==-1) ? document.location.href+'?d='+v : document.location.href.replace('d={$date}', 'd='+v);
        },
        {if $date}defaultDate:'{$date}',{/if}
        {if $alldates}
            beforeShowDay: function(v) {
                var day = v.getDate()+'';
                var month = v.getMonth()+1+'';
                var m = v.getFullYear()+'-'+(month.length==1?'0'+month:month)+'-'+(day.length==1?'0'+day:day);
                if (selectable.indexOf(m) != -1) return [true, ""];
                else return [false, 'grey']
            }
        {/if}
    });
    $(function() {
});
</script>