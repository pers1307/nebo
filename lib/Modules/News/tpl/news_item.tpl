<div class="news-page one-news">
    {if ($news_item)}
        {if $news_item.img_left_src || $news_item.img_right_src}
            <div class="top-image">
                {if ($news_item.img_left_src)}
                    <div><img src="{$news_item.img_left_src}" /></div>
                {/if}
                {if ($news_item.img_right_src)}
                    <div><img src="{$news_item.img_right_src}" /></div>
                {/if}
            </div>
        {/if}
        <h3>{$news_item.title}</h3>
        {if (!$news_item.full_text)}{$news_item.short_text}{else}{$news_item.full_text}{/if}
        {if $news_item.video.mp4}
            <br class="clear" />
            <div class="videos item" style="margin: 35px auto;">
                <video width="640" height="360" poster="{$news_item.thumb}" controls="controls" preload="none">
                    <source type="video/mp4" src="{$news_item.video.mp4}" />
                    <object width="640" height="360" type="application/x-shockwave-flash" data="flashmediaelement.swf">
                        <param name="movie" value="/js/me/flashmediaelement.swf" />
                        <param name="flashvars" value="controls=true&file={$news_item.video.mp4}" />
                        <!-- Image as a last resort -->
                        <img src="{$news_item.thumb}" width="640" height="360" title="No video playback capabilities" />
                    </object>
                </video>
                <br class="clear" />
            </div>
        {/if}
    {else}
        <p>Новость не найдена</p>
    {/if}
</div>
<script type="text/javascript">
    createPrettyPhoto();
</script>
<script src="/js/me/mediaelement-and-player.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="/js/me/mediaelementplayer.css" />
<script>
    $('video,audio').mediaelementplayer();
</script>