<?
class News extends Module {

	protected $name = __CLASS__;

	private $limit = 10;

	private $folder = 'news/';
	private $path;
    private $link;


	function __construct() {
		parent::__construct();
		$this->path = USER_FILES_DIRECTORY.$this->folder;
		$this->folder = USER_FILES_PATH.$this->folder;

        $Location = Location::getInstance();
        $this->link = '/'.$Location->first().'/'.$Location->last();
	}

	function getContent() {
		$this->smarty->assign('news_link', $this->link);

        $this->page->doc['head'] = '';

		if (isset($_GET['n'])) {
			$this->show_item();
		} else {
			$this->show_list();
		}

		return true;
	}

	function show_list() {
        $p = (isset($_GET['p'])) ? intval($_GET['p']) : 0;
        $d = (isset($_GET['d'])) ? Utils::rus2iso($_GET['d']) : false;

        $alldates = $this->db->getCol('SELECT DISTINCT DATE(FROM_UNIXTIME(publish_date)) as d FROM news WHERE airport_id IS NULL OR onlist = 1 AND visible = 1 ORDER BY publish_date');
        if (!PEAR::isError($alldates) && !empty($alldates)) {
            $alldates = json_encode($alldates);
        } else $alldates = array();

        $count = $this->db->getOne('SELECT COUNT(*) FROM news WHERE (airport_id IS NULL OR onlist = 1) and visible=1'.($d? ' AND DATE(FROM_UNIXTIME(publish_date))="'.$d.'"':''));
        $limit_from =  $this->limit * $p;
		$count = ceil($count / $this->limit);
		$this->smarty->assign('pages', array('current'=>$p, 'count'=>$count));

		$news_list = $this->db->getAll("SELECT * FROM news WHERE (airport_id IS NULL OR onlist = 1) AND visible = 1 ".($d? 'AND DATE(FROM_UNIXTIME(publish_date))="'.$d.'"':'')." ORDER BY publish_date DESC LIMIT ?, ?", array($limit_from, $this->limit));
		if (!PEAR::isError($news_list) && !empty($news_list)) {
			foreach ($news_list as $k=>$row) {
                $url = $row['id'].'_list.jpg';
				if (file_exists($this->path . $url))
                    $news_list[$k]['img_src'] = $this->folder . $url;
			}
		}
		if ($d) $this->smarty->assign('date', preg_replace('/[^0-9\.]/', '', $_GET['d']));
		$this->smarty->assign('news_list', $news_list);
		$this->smarty->assign('alldates', $alldates);
		$this->page->doc['body'] = $this->smarty->fetch($this->tpl_dir.'news_list.tpl');
	}

	function show_item() {
		$id = intval($_GET['n']);
		if (!$id) {
			$this->page->doc['top_body'] = $this->smarty->fetch($this->tpl_dir.'news_top_item.tpl');
			$this->page->doc['body'] = $this->smarty->fetch($this->tpl_dir.'news_item.tpl');
			return;
		}
		$item = $this->db->getRow("SELECT * FROM news WHERE id = $id");
		if (!is_array($item) or !count($item)) {
			$this->page->doc['top_body'] = $this->smarty->fetch($this->tpl_dir.'news_top_item.tpl');
			$this->page->doc['body'] = $this->smarty->fetch($this->tpl_dir.'news_item.tpl');
			return;
		}

        foreach (array('left', 'right') as $type) {
            $url = $item['id']."_{$type}.jpg";
            if (file_exists($this->path . $url)) {
                $item["img_{$type}_src"] = $this->folder . $url;
            }
        }
        $item['video'] = array();
        foreach (glob($this->path . "{$item['id']}_video.*") as $file) {
            $type = substr($file, -3);
            $link = str_replace($this->path, $this->folder, $file);
            if ($type == 'jpg')
                $item['thumb'] = $link;
            else
                $item['video'][$type] = $link;
        }

        $this->smarty->assign('link_back', $this->link);
		$this->smarty->assign('news_item', $item);
		$this->page->doc['top_body'] = $this->smarty->fetch($this->tpl_dir.'news_top_item.tpl');
		$this->page->doc['body'] = $this->smarty->fetch($this->tpl_dir.'news_item.tpl');
	}

    //Одна новость на главную
    function get_main_news() {
        $main_news = $this->db->getRow("SELECT id, airport_id, title, short_text, publish_date FROM news WHERE onmain = 1 AND visible = 1 ORDER BY publish_date DESC");
        return (is_array($main_news)) ? $main_news : false;
    }

}
