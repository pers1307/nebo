<?

class Utils extends Vars {

	// Дата/время в формате ДД месяц(рус) / ЧЧ:ММ
	public static function formDate($t = 0) {
		$tt = getdate((empty($t)) ? time() : $t);
		return $tt['mday'] . '&nbsp;' . self::ruMonth($tt['mon']) . '&nbsp;/&nbsp;' . $tt['hours'] . ':' . str_pad($tt['minutes'],2,'0',STR_PAD_LEFT);
	}

	// конвертирование 2001-01-01 12:12:12 -> массив
	public static function pregTime($t) {
		if (preg_match('/(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})/',$t,$m)) return $m;
		return $t;
	}

	// Русские название месяцев (в род. падеже)
	public static function ruMonth($n) {
		$n = intval($n);
		$months = array(1=>'января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря');
		if (($n >= 1) and ($n <= 12)) return $months[$n];
		return $months;
	}

	public static function ruMonth2($n) {
		$n = intval($n);
		$months = array(1=>'янв', 'фев', 'мрт', 'апр', 'май', 'июн', 'июл', 'авг', 'сен', 'окт', 'нбр', 'дек');
		if (($n >= 1) and ($n <= 12)) return $months[$n];
		return $months;
	}

	/*
	 * public static function ruMonth2($n) { $n = (int) $n-1; $months = array( 0 => 'январь',
	 * 1 => 'февраль', 2 => 'март', 3 => 'апрель', 4 => 'май', 5 => 'июнь', 6 =>
	 * 'июль', 7 => 'август', 8 => 'сентябрь', 9 => 'октябрь', 10=> 'ноябрь',
	 * 11=> 'декабрь' ); return $months[$n]; }
	 */

	// Перенаправление браузера
	public static function redirect($location, $message = '', $delay = 0, $ret = false) {
		$exo = "<meta http-equiv=\"refresh\" content=\"$delay; URL=$location\"><p class=thanks><a href=\"$location\">$message</a></p>";
		if ($ret) return $exo;
		else die($exo);
	}

	// шаг назад
	public static function stepBack() {
		die("<html><body><script language='JavaScript'><!--//\nhistory.back()\n//--></script></body></html>\n");
	}

	// Перенаправление браузера Back.history
	public static function redirectBack($mess = 'Несовпадение паролей', $prn = false) {
		$exo = "<script language='JavaScript'><!--//\n
				alert('$mess')\n
				history.back()\n
				//--></script>\n";
		if ($prn) return $exo;
		else die($exo);
	}

	// Перенаправление браузера Back.history
	public static function go($location, $mess = 'Несовпадение паролей', $prn = false) {
		$exo = "<html><body><meta http-equiv=\"refresh\" content=\"1; URL=$location\">";
		$exo .= "<script language='JavaScript'><!--//\n
				alert('$mess');\n
				//--></script>\n";
		$exo .= "</body></html>\n";
		if ($prn) return $exo;
		else die($exo);
	}

	// окно-самоубийца
	public static function selfKiller($prn = false) {
		$exo = "<html><head><script language=\"JavaScript\" type=\"text/JavaScript\"><!-- //
    			public static function killMe() { window.close(); }
    			// --></script></head>
    			<body onLoad=\"killMe()\"></body></html>";
		if ($prn) return $exo;
		else die($exo);
	}

	// кол-во непустых элементов массива
	public static function _arrNoneEmpty($arr) {
		$out = 0;
		foreach ( $arr as $k => $v ) {
			$s = (is_string($v)) ? trim($v) : $v;
			$out += (! empty($s)) ? 1 : 0;
		}
		return $out;
	}

	// информация о граф.файле
	public static function getImgSize($f) {
		$t = array('', 'gif', 'jpg', 'png', 'swf');
		if (file_exists($f)) {
			$arr = GetImageSize($f);
			if (empty($arr)) return false;
			$arr[4] = $t[$arr[2]];
			return $arr;
		}
		return false;
	}

	public static function myfile($tpl) {
		$a = @fopen($tpl,"r");
		if ($a) {
			$b = fread($a,filesize($tpl));
			fclose($a);
			return $b;
		} else
			die("cannot open template, $tpl");
	}

	public static function dmp($a, $die = 0) {
		echo "<pre>";
		var_dump($a);
		echo "</pre>";
		if ($die) die();
	}

	// удобоваримая временная метка
	public static function timeC($t) {
		$a = self::pregTime($t);
		$d = $a[3] . '.' . $a[2] . '.' . $a[1];
		return (($d == self::today_date()) ? 'сегодня' : $d) . ' /' . ' ' . $a[4] . ':' . $a[5];
	}

	public static function timeAndDate($t) {
		$a = self::pregTime($t);
		return $a[3] . '.' . $a[2] . '.' . $a[1] . ' ' . $a[4] . ':' . $a[5];
	}

	// удобоваримая временная метка без времени
	public static function timeNoDate($t) {
		$a = self::pregTime($t);
		$d = $a[3] . '.' . $a[2] . '.' . $a[1];
		return $d;
	}

	// Приемлемый формат даты
	public static function today_date() {
		$ret = date("d.m.Y",time());
		return ($ret);
	}

	// Приемлемый формат времени
	public static function today_time() {
		$ret = date("G:i",time());
		return ($ret);
	}

	// читаем get, чистим. VERSION 0.2 !!!
	public static function parseGet($name, $check_int = true) {
		if (isset($_GET[$name])) {
			$val = $_GET[$name];
			if ($check_int) $val = intval($val);
			else {
				$val = self::cleanData($val);
				$val = @trim($val);
			}
			return $val;
		} else return false;
	}

	// читаем post, чистим
	public static function parsePost($name, $check_int = true) {
		if (isset($_POST[$name]) and ($val = $_POST[$name])) {
			if ($check_int) {
				$val = intval($val);
			}
		} else {
			$val = @self::cleanData($val);
			$val = @trim($val);
		}
		return $val;
	}

    // читаем post, чистим
    public static function parsePostFloat($name) {
        if (isset($_POST[$name]) and ($val = $_POST[$name])) return self::parseFloat($val);
        else return 0;
    }

    public static function parseFloat($val) {
        return str_replace(',', '.', floatval($val));
    }

	public static function array_clean($ar, $ext = false) {
		self::$ext_filter = $ext;
		if (! is_array($ar)) {
			return $ar;
		} else {
			$ar = array_filter($ar,array(self,	"subclean"));
			return $ar;
		}
	}

	public static function cleanData($str) {
		$str = preg_replace('/[^\w\x7F-\xFF\s]/'," ",$str);
		$str = preg_replace('!\s+!',' ',$str);
		$str = addslashes($str);
		return $str;
	}

	// эта функция хитро юзается в array_clean (см. выше)
	public static function subclean($val) {
		$val = trim($val);
		if (self::$ext_filter) {
			$preg = "/^[_a-z0-9]/i";
			if (! empty($val) && preg_match($preg,$val)) return $val;
		} else {
			if (! empty($val)) return $val;
		}
	}

	public static function str_split_common($chaine, $length = 1) {
		$retour = FALSE;
		$incrmt = (int) $length;
		if (0 < $incrmt) {
			$retour = array();
			$offset = 0;
			$limite = strlen($chaine);
			while ( $offset < $limite ) {
				$retour[] = substr($chaine,$offset,$incrmt);
				$offset += $incrmt;
			}
		} // if (0 < $incrmt)
		return ($retour);
	}

	public static function mytime() {
		return date("Y-m-d H:i:s");
	}

	public static function pregtrim($str) {
		return preg_replace("/[^\x20-\xFF]/","",@strval($str));
	}

	public static function checkmail($mail) {
		$mail = trim(Utils::pregtrim($mail));
		if (strlen($mail) == 0) {
			return 1;
		}

		if (! preg_match('/^[^\.][a-z0-9_\-\.]{1,20}@(([a-z0-9-]+\.)+(com|net|org|mil|' . 'edu|gov|arpa|info|biz|inc|name|[a-z]{2})|[0-9]{1,3}\.[0-9]{1,3}\.[0-' . '9]{1,3}\.[0-9]{1,3})$/is',$mail)) {
			return - 1;
		}

		return $mail;
	}

	public static function getUserIP() {
		return $_SERVER['REMOTE_ADDR'];
	}

	public static function ru2uni($isoline) {
		$uniline = "";
		for($i = 0; $i < strlen($isoline); $i ++) {
			$thischar = substr($isoline,$i,1);
			$charcode = ord($thischar);
			if ($charcode == 168) {
				$uniline .= "&#" . (26 * 32 + 3 + 12 + 10 + ($charcode)) . ";";
			} elseif ($charcode == 184) {
				$uniline .= "&#" . (26 * 32 + 3 + 12 + 10 + 60 + 4 + ($charcode)) . ";";
			} elseif (($charcode >= 192) and ($charcode <= 255)) {
				$uniline .= "&#" . (26 * 32 + 3 + 12 + 1 + ($charcode)) . ";";
			} else {
				$uniline .= $thischar;
			}
		}
		return $uniline;
	}

	public static function array_clean_multi(&$arr) {
		$temp = array();
		reset($arr);
		if (count($arr) == 0) {
			return '';
		}
		foreach ( $arr as $key => $val ) {
			if (is_array($val)) {
				self::array_clean_multi($val);
			}
			if ($val) {
				$temp[$key] = $val;
			}
		}
		$arr = $temp;
		reset($arr);
	}

	public static function parseHtml($s_str) {
		$i_indicatorL = 0;
		$i_indicatorR = 0;
		$s_tagOption = "";
		$i_arrayCounter = 0;
		$a_html = array();
		// Search for a tag in string
		while ( is_int(($i_indicatorL = strpos($s_str,"<",$i_indicatorR))) ) {
			// Get everything into tag...
			$i_indicatorL ++;
			$i_indicatorR = strpos($s_str,">",$i_indicatorL);
			$s_temp = substr($s_str,$i_indicatorL,($i_indicatorR - $i_indicatorL));
			$a_tag = explode(' ',$s_temp);
			// Here we get the tag's name
			list(,$s_tagName,,) = each($a_tag);
			$s_tagName = strtoupper($s_tagName);
			// Well, I am not interesting in <br>, </font> or anything else like
			// that...
			// So, this is false for tags without options.
			$b_boolOptions = is_array(($s_tagOption = each($a_tag))) && $s_tagOption[1];

			if ($b_boolOptions) {
				// Without this, we will mess up the array
				$i_arrayCounter = (int) count($a_html[$s_tagName]);
				// get the tag options, like src="htt://". Here, s_tagTokOption
				// is 'src' and s_tagTokValue is '"http://"'
				do {
					$s_tagTokOption = strtoupper(strtok($s_tagOption[1],"="));
					$s_tagTokValue = trim(strtok("="));
					$a_html[$s_tagName][$i_arrayCounter][$s_tagTokOption] = $s_tagTokValue;
					$b_boolOptions = is_array(($s_tagOption = each($a_tag))) && $s_tagOption[1];
				} while ( $b_boolOptions );
			}
		}
		return $a_html;
	}

	public static function rus2iso($dd) {
		// d.m.Y -> Y-m-d
		$dd = preg_replace('/[^\d\.]/','',$dd);
		@list($d,$m,$y) = explode('.',$dd);
        $dc = new DateCalc();
		if ($d && $m && $y && $dc->isValidDate($d, $m, $y)) return $y."-".$m."-".$d;
        else return false;
	}

	public static function iso2rus($dd) {
		// d.m.Y -> Y-m-d
		$dd = preg_replace('/[\d\-]/','',$dd);
		list($y,$m,$d) = explode('\-',$dd);
		return $d . "." . $m . "." . $y;
	}

	public static function array_search_r($needle, $haystack) {
		$match = false;
		foreach ( $haystack as $value ) {
			if (is_array($value)) $match = self::array_search_r($needle,$value);
			if ($value == $needle) $match = 1;
			if ($match) return 1;
		}
		return 0;
	}

	/**
	 *
	 * @todo refactor me
	 */
	public static function findImage($img_name, $img_folder, $all=false, $cutroot=true) {
		if (! $img_name) return false;
		if (substr($img_folder,- 1) != "/") $img_folder .= "/";
		$match = $img_folder . $img_name . ".*";
		$ar = glob($match);

		$res = array();
		$allowed = array('jpg', 'gif','png','swf');
		if (is_array($ar)) {
			foreach ( $ar as $r ) {
				$pt = pathinfo($r);
				if (isset($pt['extension']) && in_array($pt['extension'], $allowed) && is_readable($r)) {
					if (!$all) return $r;
					$res[] = ($cutroot) ? str_replace(ROOT, URL, $r) : $r;
				}
			}
		}
		return $res;
	}

	public static function parseFilter($ar = '') {
		if (empty($ar)) $ar = $_GET;
		// возвращаем урлу со всей этой байдой, которая в нее пишется
		if (is_array($ar) && ! empty($ar)) {
			$str = array();
			foreach ( $ar as $k => $v ) {
				if ($k == 'p') continue;
				if ($k == 'submit') continue;
				if (is_array($v)) continue;
				$str[] = $k . "=" . $v;
			}
			if (! empty($str)) return implode("&",$str);
			else return false;
		} else
			return false;
	}

	public static function cleanR($t) {
		return preg_replace("/(\r|\n)/","",$t);
	}

	public static function undoSlashes($input) {

		function inUndoSlashes($v) {
			return (is_array($v)) ? array_map("inUndoSlashes",$v) : stripslashes($v);
		}

		if (get_magic_quotes_gpc() == 1) {
			$input = array_map('inUndoSlashes',$input);
		}
		return $input;
	}

	public static function getFileExt($name) {
		$pt = pathinfo($name);
		if (isset($pt['extension'])) return $pt['extension'];
	}

    public static function buildTrigrams($keyword) {
        $t = "__" . $keyword . "__";
        $trigrams = "";
        for ($i = 0; $i < mb_strlen($t) - 2; $i++) $trigrams .= mb_substr($t, $i, 3) . " ";
        return $trigrams;
    }

    public static function generatepassword ($length = 8) {
        $password = "";
        $possible = "0123456789abcdfghjkmnpqrstvwxyzABCDFGHJKMNPQRSTVWXYZ-_";
        $len = strlen($possible) - 1;
        $i = 0;
        while ($i < $length) {
            $char = substr($possible, mt_rand(0, $len), 1);
            if (!strstr($password, $char)) {
                $password .= $char;
                $i++;
            }
        }
        return $password;
    }
}
