<?
require_once('Vars.php');
require_once('phpmailer/class.phpmailer.php');


class MailSend extends Vars {

    /**
     * Обвязка для отправки почты
     *
     * @param string $to
     * @param string $body
     * @param string $subject
     * @param array $hdrs
     * @return bool|int
     */
    public static function send($to = '', $body = '', $subject = '', $hdrs = array()) {
		if (!EMAIL_SEND) {
			$body = "to: {$to}\r\n\r\n___________________\r\n\r\n{$body}\r\n\r\n___________________\r\n\r\n".date("d.m.Y H:i");
			return file_put_contents(R_Lib."email.txt", $body);
		}

        $mail = new PHPMailer;

        if (EMAIL_METHOD == 'smtp') {
            $mail->IsSMTP();
            $mail->Host = EMAIL_SMTP;
            $mail->From = EMAIL_FROM;
            $mail->FromName = EMAIL_FROM_NAME;
        }

        $mail->CharSet = 'utf-8';
        $mail->AddAddress($to);
        $mail->IsHTML(true);
        $mail->Subject = $subject;
        $mail->MsgHTML($body);

        if (!empty($hdrs)) foreach ($hdrs as $k=>$h) $mail->AddCustomHeader($k, $h);
        return $mail->Send();
	}
}
?>
