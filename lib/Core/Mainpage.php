<?
class Mainpage {

	/**
	 * @var SmartyHolder
	 */
	var $smarty;
	
	/**
	 * @var MyDB2
	 */
	var $db;
	
	/**
	 * @var ControlArea
	 */
	var $controller;

    private $folder = 'banners/';
    private $path;
    private $header_folder = 'header/';
    private $header_path;

	function __construct() {
		$this->db = MyDB2::GetInstance();
		$this->smarty = SmartyHolder::GetInstance();

        $this->path = USER_FILES_DIRECTORY . $this->folder;
        $this->folder = USER_FILES_PATH . $this->folder;
        $this->header_path = USER_FILES_DIRECTORY . $this->header_folder;
        $this->header_folder = USER_FILES_PATH . $this->header_folder;
	}
    
    public function rewrite(&$controller) {
		$this->controller = &$controller;
        $doc = array('head'=>'Главная страница');
        $this->smarty->assign('doc', $doc);

        $act = isset($_GET['act']) ? $_GET['act'] : null;
        switch ($act) {
            case 'edit_banner':
                $this->edit_banner();
                break;

            case 'upload_header':
                $this->upload_header();
                return false;
                break;

            case 'delete_header':
                $this->delete_header();
                return false;
                break;

            case 'update_text':
                $this->update_text();
                return false;
                break;

            case 'sort_image_header':
                $this->sort_image_header();
                return false;
                break;

            default:
                $this->showMain();
        }
        return true;
	}
    
    function showMain() {
        $banners = $this->getBanners();
        $headers = $this->getHeaders();

        $text = $this->db->getOne("SELECT value FROM vars WHERE name = 'index_text'");

        $this->smarty->assign('main_text', $text);
        $this->smarty->assign('banners', $banners);
        $this->smarty->assign('headers', $headers);
        $this->controller->body = $this->smarty->fetch('main_edit.tpl');
	}

    public function getBanners()
    {
        $banners = $this->db->getAll("SELECT * FROM banners");

        $path = $this->path;
        $folder = $this->folder;
        array_walk($banners, function(&$item) use ($path, $folder) {
            if (file_exists("{$path}banner{$item['id']}.jpg"))
                $item['thumb'] = "{$folder}banner{$item['id']}.jpg";
        });

        return $banners;
    }

    private function edit_banner()
    {
        $id = Utils::parseGet('id');
        if ($id) {
            $item = $this->db->getRow("SELECT * FROM banners WHERE id = ?", array($id));
        }
        if(!isset($item) || PEAR::isError($item)) {
            header("Location: /_bo/?action=mainpage");
            exit();
        }

        if (!empty($_POST['title']) && !empty($_POST['link'])) {
            $item['title'] = $_POST['title'];
            $item['link'] = $_POST['link'];
            if (isset($_POST['text']))
                $item['text'] = $_POST['text'];
            $item['important'] = (int)isset($_POST['important']);

            $this->db->update('banners', $item, "id = {$item['id']}");

            if (!empty($_FILES['banner']) && $_FILES['banner']['error'] == 0) {
                if (!file_exists($this->path)) {
                    Tools::create_directory($this->path, 0777);
                }
                $img = Image::CreateFromFile($_FILES['banner']['tmp_name']);
                $img->resize(210);
                $path = "{$this->path}banner{$item['id']}.jpg";
                $img->save($path, 'jpeg');
            }

            header("Location: /_bo/?action=mainpage");
        }

        if (file_exists("{$this->path}banner{$item['id']}.jpg"))
            $item['thumb'] = "{$this->folder}banner{$item['id']}.jpg";

        $this->smarty->assign('item', $item);
        $this->controller->body = $this->smarty->fetch('banner_edit.tpl');
    }

    private function upload_header()
    {
        //config
        $img_w = '2560';
        $img_h = '500';

        if (isset($_FILES['file'])) {

            //get file ext
            $file_ext = strrchr($_FILES['file']['name'], '.');

            $file_name = $_FILES['file']['name'];
            $dest_path = $this->header_path.'/';

            $uploaded_file = $_FILES['file']['tmp_name'];

            //check if its allowed or not
            $whitelist = array('.jpg','.jpeg','.png');
            if (!(in_array($file_ext, $whitelist))) {
                echo json_encode(array('success' => false, 'error' => 'Не верный тип файла. Разрешены только jpg, jpeg, png'));
                return;
            }

            //check upload type
            $pos = strpos($_FILES['file']['type'],'image');
            if($pos === false) {
                echo json_encode(array('success' => false, 'error' => 'Файл не является картинкой'));
                return;
            }

            $imageinfo = getimagesize($uploaded_file);

            //check size image
            if ($imageinfo[0] < $img_w || $imageinfo[1] < $img_h) {
                echo json_encode(array('success' => false, 'error' => 'Размер изображения должен быть не менее 2560x500 пикселей'));
                return;
            }

            if ($imageinfo['mime'] != 'image/jpeg'&& $imageinfo['mime'] != 'image/jpg'&& $imageinfo['mime'] != 'image/png') {
                echo json_encode(array('success' => false, 'error' => 'Не верный тип файла. Разрешены только jpg, jpeg, png'));
                return;
            }

            //check directory
            if (!file_exists($this->header_path))
                Tools::create_directory($this->header_path, 0777);

            //save db
            $data = array();
            $data['hash'] = md5($file_name);
            $data['ext'] = substr($file_ext, 1);
            $this->db->insert('main_image', $data);

            $file = $dest_path . $data['hash'] . $file_ext;

            //save file
            if ($uploaded_file && is_uploaded_file($uploaded_file)) {
                $img = Image::CreateFromFile($uploaded_file);
                $img->save($file, 'jpeg');


            } else {
                echo json_encode(array('success' => false, 'error' => 'Ошибка загрузки файла. Попробуйте еще раз.'));
                return;
            }

            echo json_encode(array('success' => true, 'link' => $this->header_folder . $data['hash'] . $file_ext));
        }
    }


    private function delete_header()
    {
        if (isset($_GET['p'])) {
            $id = $_GET['p'];
            $file = $this->db->getRow("SELECT * FROM main_image WHERE id = {$id}");
            $file = str_replace(USER_FILES_PATH, USER_FILES_DIRECTORY, $this->header_folder.$file['hash'].'.'.$file['ext']);

            $this->db->delete('main_image', "id = {$id}");
            if (file_exists($file))
                unlink($file);
            echo json_encode(array('success' => true));
        }
    }




    private function update_text()
    {
        if (!empty($_POST['text']))
            $this->db->update('vars', array('value' => $_POST['text']), "name = 'index_text'");
        echo json_encode(array('success' => true));
    }

    public function getHeaders()
    {
        /*
        $files = glob($this->header_path . '*');
        array_walk($files, function(&$item) {
            $item = str_replace(USER_FILES_DIRECTORY, USER_FILES_PATH, $item);
        });

        return $files;
        */

        $files = $this->db->getAll("SELECT * FROM main_image ORDER BY sort");

        return $files;
    }

    private function sort_image_header ()
    {
        Utils::dmp($_POST);
        if (!empty($_POST['ids'])) {
            $this->db->simpleQuery("SET @sort = 0;");
            $ret = $this->db->simpleQuery("
            UPDATE main_image SET sort = (@sort:=@sort + 1)
            ORDER BY FIELD(id," . implode(',', $_POST['ids']) . ")
            ");
            echo json_encode(array('success' => !PEAR::isError($ret)));
        }
    }
}