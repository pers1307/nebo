<?
class Galleries {

	public $smarty;
	public $db;
	public $controller;

    protected $createThumb = true;
    protected $thumbSizeX = 170;
    protected $thumbSizeY = 170;
    protected $thumbCropAuto = true;
    protected $thumbCropManual = true;
    protected $thumbCropFrom = 'top'; // [top*, middle, bottom]

	function __construct() {
		$this->db = MyDB2::GetInstance();
		$this->smarty = SmartyHolder::GetInstance();

        $this->smarty->assign('createThumb', $this->createThumb);
        $this->smarty->assign('thumbSizeX', $this->thumbSizeX);
        $this->smarty->assign('thumbSizeY', $this->thumbSizeY);
        $this->smarty->assign('thumbCropManual', $this->thumbCropManual);
	}

	public function rewrite(&$controller) {
		$up_files_root = USER_FILES_DIRECTORY.'galleries/';
		if (!file_exists($up_files_root) or !is_dir($up_files_root)) {
			@mkdir($up_files_root);
			@chmod($up_files_root, 0777);
		}

		$this->controller = &$controller;
		if (isset($_POST['act'])) {
			$act = trim($_POST['act']);
			if (!$act) exit;
			if (method_exists($this, $act) and is_callable(array($this, $act))) {
				call_user_func(array($this, $act));
			}
			return false;
		} else {
			$this->show_main();
			return true;
		}
	}

	private function show_main() {
		$folder_id = (isset($_GET['fid'])) ? intval($_GET['fid']) : 0;
		$this->get_files($folder_id);
		$doc = array('head'=>'Галереи');
		$this->smarty->assign('doc', $doc);
        $this->controller->body = $this->smarty->fetch('galleries.tpl');
	}

	private function get_files($folder_id = 0) {
		$folder = $this->db->getRow("SELECT * FROM galleries WHERE id = {$folder_id} AND type = 'folder' ORDER BY create_date DESC");
		if (is_array($folder) and count($folder)) {
			$folder['short_title'] = (mb_strlen($folder['title']) > 80) ? mb_substr($folder['title'], 0, 80).'...' : $folder['title'];
			$this->smarty->assign('fl_folder', $folder);
		}

        $list = $this->db->getAll("SELECT * FROM galleries WHERE parent_id = {$folder_id} ORDER BY sort, create_date DESC");
		if (!is_array($list) or !count($list)) {
			return false;
		}

		foreach ($list as $k=>$row) {
			$title = $row['title'];
			if ($row['type'] == 'file') {
				$list[$k]['title_short'] = (mb_strlen($title) > 55) ? mb_substr($title, 0, 55).'...' : $title;
				$link = USER_FILES_PATH.$row['fullpath'].$row['hash'].".".$row['ext'];
                $thumbs = glob(ROOT.USER_FILES_PATH.$row['fullpath'].$row['hash']."_thumb_*.png");
                $list[$k]['thumb'] = !empty($thumbs) ? str_replace(ROOT, URL, current($thumbs)) : false;
                @$size = getimagesize(ROOT.$link);
                $list[$k]['width'] = $size[0];
                $list[$k]['height'] = $size[1];
				$list[$k]['link'] = $link;
				$list[$k]['size'] = Tools::convert_file_size($row['size']);
				$list[$k]['full_link'] = $link."/{$row['title']}.{$row['ext']}";
			} elseif ($row['type'] == 'folder') {
				$list[$k]['title_short'] = (mb_strlen($title) > 80) ? mb_substr($title, 0, 80).'...' : $title;
				$list[$k]['children'] = $this->db->getOne("SELECT COUNT(*) FROM galleries WHERE parent_id = {$row['id']}");
			}
		}

		$this->smarty->assign('fl_galleries', $list);
	}

	private function upload_files() {
		if (!isset($_POST['folder_id'])) {
			header("location: /_bo/?action=galleries");
			exit;
		}

		$fullpath = 'galleries/';

		$folder_id = $parent_id = intval($_POST['folder_id']);
		if ($folder_id) {
			$folder = $this->db->getRow("SELECT * FROM galleries WHERE id = {$folder_id} AND type = 'folder'");
			if (!is_array($folder) or !count($folder)) {
				header("location: /_bo/?action=galleries");
				exit;
			}
			$fullpath = str_replace('../', '', $folder['fullpath']);
		}

		$dest_path = USER_FILES_DIRECTORY.$fullpath;

		if (!file_exists($dest_path)) {
			header("location: /_bo/?action=galleries");
			exit;
		}

		$files = $_FILES['file'];
		foreach ($files['name'] as $key=>$name) {
			$error = $files['error'][$key];
			$tmp_name = $files['tmp_name'][$key];
			$size = $files['size'][$key];
			$type = $files['type'][$key];
			$title = trim($_POST['file_title'][$key]);

			if (!$name or !$tmp_name or !$size or $error) continue;

			$file_info = pathinfo($name);
			$ext = strtolower($file_info['extension']);
			$deny_extensions = array('js', 'php', 'html', 'htm', 'tpl', 'as', 'exe', 'cab', 'cmd', 'bat');
			if (in_array($ext, $deny_extensions)) continue;

			$hash = md5($name);
			$destination = $dest_path.$hash.'.'.$ext;

			if (file_exists($destination)) {
				unlink($destination);
			}

			if (!$title) {
				$p = strrpos($name, '.');
				$title = substr($name, 0, $p);
			}

			$res = @rename($tmp_name, $destination);
			if ($res) {
				@chmod($destination, 0644);
				$create_date = time();
				$this->db->insert('galleries', compact('parent_id', 'hash', 'title', 'ext', 'create_date', 'size', 'fullpath'));

                if ($this->createThumb) $this->createThumb($destination, $dest_path.$hash.'_thumb_'.$this->thumbSizeX.'x'.$this->thumbSizeY);
			}
		}

		header("location: /_bo/?action=galleries&fid={$folder_id}");
		exit;
	}

    private function createThumb($filepath, $thumbPath, $resampled=array()) {
        $gd_formats	= array('jpg','jpeg','png','gif');//web formats
        $file_name	= pathinfo($filepath);

        if(!in_array(strtolower($file_name['extension']), $gd_formats)) {
            return false;
        }

        list($width_orig, $height_orig) = getimagesize($filepath);
        if ($width_orig>0 && $height_orig>0) {
            $ratioX	= $this->thumbSizeX/$width_orig;
            $ratioY	= $this->thumbSizeY/$height_orig;

            $ratio = (!$this->thumbCropAuto) ? min($ratioX, $ratioY) : 0;
            $ratio = ($ratio==0) ? max($ratioX, $ratioY):$ratio;

            $newW = $width_orig*$ratio;
            $newH = $height_orig*$ratio;

            // Resample
            $thumb = imagecreatetruecolor($newW, $newH);
            $image = imagecreatefromstring(file_get_contents($filepath));

            imagecopyresampled($thumb, $image, 0, 0, 0, 0, $newW, $newH, $width_orig, $height_orig);

            if ($this->thumbCropAuto) {
                // Crop
                $cropped = imagecreatetruecolor($this->thumbSizeX, $this->thumbSizeY);

                $fromX = $fromY = 0;

                if ($newW > $this->thumbSizeX) {
                    switch ($this->thumbCropFrom) {
                        case 'middle':
                            $fromX = ceil(($newW - $this->thumbSizeX)/2);
                            break;
                        case 'bottom':
                            $fromX = $newW - $this->thumbSizeX;
                            break;
                    }
                }

                if ($newH > $this->thumbSizeY) {
                    switch ($this->thumbCropFrom) {
                        case 'middle':
                            $fromY = ceil(($newH - $this->thumbSizeY)/2);
                            break;
                        case 'bottom':
                            $fromY = $newH - $this->thumbSizeY;
                            break;
                    }
                }

                // Finally!
                imagecopyresampled($cropped, $thumb, 0, 0, $fromX, $fromY, $this->thumbSizeX, $this->thumbSizeY, $this->thumbSizeX, $this->thumbSizeY);
                imagepng($cropped, $thumbPath.'.png', 9);
                imagedestroy($cropped);
            } else {
                imagepng($thumb, $thumbPath.'.png', 9);
            }

            imagedestroy($image);
            imagedestroy($thumb);
            return true;

        } else {
            return false;
        }
    }

	private function delete_file() {
		if (!isset($_POST['id'])) exit;
		$id = intval($_POST['id']);
		if (!$id) exit;
		$row = $this->db->getRow("SELECT * FROM galleries WHERE id = $id");
		if (!is_array($row) or !count($row)) exit;

		if ($row['type'] == 'folder') {
			$children_exist = $this->db->getOne("SELECT 1 FROM galleries WHERE parent_id = {$id}");
			if ($children_exist) exit;
		}

		$this->db->delete('galleries', "id = $id");
		extract($row);
		$path = USER_FILES_DIRECTORY.$fullpath;
		if ($row['type'] == 'folder') {
			if (file_exists($path)) {
				@rmdir($path);
			}
		} elseif ($row['type'] == 'file') {
			$file = $path.$hash.'.'.$ext;
			if (file_exists($file)) @unlink($file);

            $thumb = $path.$hash.'_thumb_*.png';
            $thumbs = glob($thumb);
            if (!empty($thumbs)) foreach ($thumbs as $th) if (file_exists($th)) @unlink($th);
		}

		echo 1;
	}

	private function rename_file() {
		if (!isset($_POST['id']) or !isset($_POST['title'])) exit;
		$id = intval($_POST['id']);
		$title = trim($_POST['title']);
		$description = trim($_POST['description']);
		if (!$id or !$title) exit;

		$data = $this->db->getRow("SELECT * FROM galleries WHERE id = $id");
		if ($data['type'] == 'folder') {
			$isset = $this->db->getOne("SELECT 1 FROM galleries WHERE parent_id = {$data['parent_id']} AND type = 'folder' AND title = '{$title}' AND id <> {$id}");
			if ($isset) {
				echo 'Галерея с таким именем уже существует';
				exit;
			}
		} elseif ($data['type'] == 'file') {
//			$isset = $this->db->getOne("SELECT 1 FROM galleries WHERE parent_id = {$data['parent_id']} AND type = 'file' AND ext = '{$data['ext']}' AND title = '{$title}' AND id <> {$id}");
//			if ($isset) {
//				echo 'Файл с таким именем уже существует';
//				exit;
//			}
		}

		$this->db->update('galleries', compact('title', 'description'), "id = {$id}");
		echo 1;
	}

	private function rotate_file() {
		$id = Utils::parsePost('id');
		if (empty($id)) exit();

		$data = $this->db->getRow("SELECT * FROM galleries WHERE id = $id");
		if ($data['type'] == 'file') {
            $path = USER_FILES_DIRECTORY.$data['fullpath'];
            $file = $path.$data['hash'].'.'.$data['ext'];
            if (file_exists($file)) {

                // rotate main image
                $image = imagecreatefromstring(file_get_contents($file));
                $rotate = imagerotate($image, -90, 0);

                if (function_exists('image'.$data['ext'])) $fn = 'image'.$data['ext'];
                else if ($data['ext'] == 'jpg') $fn = 'imagejpeg';
                else $fn = 'imagepng';

                $fn($rotate, $file, 100);
                imagedestroy($image);
                unset($rotate, $image, $fn);

                // rotate / create thumb
                if ($this->createThumb) {
                    $thumb = $path.$data['hash'].'_thumb_'.$this->thumbSizeX.'x'.$this->thumbSizeY;
                    $thumb_file = $thumb.'.png';

                    if (is_file($thumb_file)) {
                        $th = imagecreatefromstring(file_get_contents($thumb_file));
                        $rotate = imagerotate($th, -90, 0);
                        $res = imagepng($rotate, $thumb_file);
                        imagedestroy($rotate);
                    } else {
                        $this->createThumb($file, $thumb);
                    }
                }
                echo 1;
            }
        }

		exit();
	}

    private function crop_file() {
		$id = Utils::parsePost('id');
		if (empty($id)) exit();

        $select = $_POST['select'];
        $select = array_map(function($e) { return round(floatval($e)); }, $select);

		$data = $this->db->getRow("SELECT * FROM galleries WHERE id = $id");
		if ($data['type'] == 'file') {
            $path = USER_FILES_DIRECTORY.$data['fullpath'];
            $file = $path.$data['hash'].'.'.$data['ext'];
            if (file_exists($file)) {
                if ($this->createThumb && $this->thumbCropManual) {
                    $image = imagecreatefromstring(file_get_contents($file));
                    $cropped = imagecreatetruecolor($this->thumbSizeX, $this->thumbSizeY);
                    imagecopyresampled($cropped, $image, 0, 0, $select['x'], $select['y'], $this->thumbSizeX, $this->thumbSizeY, $select['w'], $select['h']);
                    imagepng($cropped, $path.$data['hash'].'_thumb_'.$this->thumbSizeX.'x'.$this->thumbSizeY.'.png', 9);
                    imagedestroy($image);
                    imagedestroy($cropped);
                    echo 1;
                }
            }
        }

		exit();
	}

	private function create_folder() {
		if (!isset($_POST['title'])) exit;
		$folder_id = isset($_POST['folder_id']) ? intval($_POST['folder_id']) : 0;
		$title = str_replace(array('../', '/', './', '\\', '*', ':', '@', '#', '%', '^', '$', '?', '&', '"', "'", '№', '>', '<'), '', trim($_POST['title']));

		if (!$title) {
			echo "Недопустимое имя файла";
			exit;
		}

		$exist = $this->db->getOne("SELECT 1 FROM galleries WHERE parent_id = {$folder_id} AND type = 'folder' AND title = '{$title}'");
		if ($exist) {
			echo 'Галерея с таким именем уже существует';
			exit;
		}

		if ($folder_id) {
			$parent = $this->db->getRow("SELECT * FROM galleries WHERE id = {$folder_id} AND type = 'folder'");
			if (!is_array($parent) or !count($parent)) {
				echo 'Галерея не найдена';
				exit;
			}
			$fullpath = str_replace('../', '', $parent['fullpath']);
		} else {
			$fullpath = 'galleries/';
		}

		$parent_id = $folder_id;
		$type = 'folder';
		$create_date = time();
		$id = $this->db->insert('galleries', compact('parent_id', 'type', 'create_date', 'title'));

		if (!$id or (PEAR::isError($id))) {
			echo 'Ошибка базы данных. Невозможно создать запись';
			exit;
		}

		$fullpath .= $id.'/';

		$path = USER_FILES_DIRECTORY.$fullpath;
		if (file_exists($path)) {
			echo "Галерея с таким идентификатором уже существует [{$id}]";
			exit;
		}

		$res = @mkdir($path);
		if ($res) {
			@chmod($path, 0777);
			$this->db->update('galleries', array('fullpath'=>$fullpath), "id = {$id}");
			echo 1;
		} else {
			echo "!{$path}]\r\n";
		}
	}

	private function get_content() {
		$folder_id = (isset($_POST['folder_id'])) ? intval($_POST['folder_id']) : 0;
		$this->get_files($folder_id);
		if (isset($_POST['ck_name'])) {
			$ck_name = trim($_POST['ck_name']);
			$this->smarty->assign('fl_ck_name', $ck_name);
		}
		$this->smarty->display('galleries_widget.tpl');
	}

    public function front($gal_id) {
        $this->get_files($gal_id);
    }

    private function sort_thumb ()
    {
        if (!empty($_POST['ids']) && !empty($_POST['id'])) {
            $this->db->simpleQuery("SET @sort = 0;");
            $ret = $this->db->query("
            UPDATE galleries SET sort = (@sort:=@sort + 1)
            WHERE parent_id = ?
            ORDER BY FIELD(id," . implode(',', $_POST['ids']) . ")
            ", array($_POST['id']));

            echo json_encode(array('success' => !PEAR::isError($ret)));
        }
    }

}