<?
class Settings {

	/**
	 * @var SmartyHolder
	 */
	var $smarty;
	
	/**
	 * @var MyDB2
	 */
	var $db;
	
	/**
	 * @var ControlArea
	 */
	var $controller;


	function __construct() {
		$this->db = MyDB2::GetInstance();
		$this->smarty = SmartyHolder::GetInstance();
	}
    
    public function rewrite(&$controller) {
		$this->controller = &$controller;
       
		if (isset($_POST['act'])) {
			$act = trim($_POST['act']); 
			if (!$act) exit;
			if (method_exists($this, $act) and is_callable(array($this, $act))) {
				call_user_method($act, $this);
			}
			return false;
		} else {
			$this->showMain();
			return true;
		}
        
 
	}
    
    function showMain() {
        $doc = array('head'=>'Настройки');
        $vars = $this->db->getAll("SELECT * FROM vars WHERE visibility = 1 ORDER BY sort");
        
        $this->smarty->assign('doc', $doc);   
        $this->smarty->assign('sitevars', $vars);        
		$this->controller->body = $this->smarty->fetch('settings.tpl');
        
	}
    
    private function showSettings() {   
       
    }
    
    private function saveSettings() {
		if ($this->controller->user['status'] != 'admin') {
			header('location: ?action=list');
			exit();
		}
		if (!empty($_POST)) {
			foreach ($_POST as $key=>$val) {
				$type = $this->db->getOne("SELECT type FROM vars WHERE name = '$key' AND visibility = 1");
				if (!PEAR::isError($type) and strlen($type)) {
					if ($type == 'text') {
						$val = stripslashes(trim($val));
					} elseif ($type == 'html') {
						$val = stripslashes(trim($val));
					} elseif ($type == 'str') {
						$val = stripslashes(htmlspecialchars(trim($val)));
					} elseif ($type == 'int') {
						$val = intval($val);
					}
					$this->db->update('vars', array('value'=>$val), "name = '$key'");
				}
			}
		}
		header("location: ?action=settings");
		exit;
	}
    
}