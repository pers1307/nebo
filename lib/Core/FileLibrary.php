<?
class FileLibrary {

	/**
	 * @var SmartyHolder
	 */
	var $smarty;

	/**
	 * @var MyDB2
	 */
	var $db;

	/**
	 * @var ControlArea
	 */
	var $controller;


	function __construct() {
		$this->db = MyDB2::GetInstance();
		$this->smarty = SmartyHolder::GetInstance();
	}


	public function rewrite(&$controller) {
		$up_files_root = USER_FILES_DIRECTORY.'files/';
		if (!file_exists($up_files_root) or !is_dir($up_files_root)) {
			@mkdir($up_files_root);
			@chmod($up_files_root, 0777);
		}

		$this->controller = &$controller;

		/*
        if (isset($_POST['act'])) {
			$act = trim($_POST['act']);
			if (!$act) exit;
			if (method_exists($this, $act) and is_callable(array($this, $act))) {
				call_user_func(array($this, $act));
			}
			return false;
		} else {
			$this->show_main();
			return true;
		}
		*/

        $act = isset($_GET['act']) ? $_GET['act'] : null;
        switch ($act) {
            case 'create_folder':
                $this->create_folder();
                break;

            case 'upload_files':
                $this->upload_files();
                break;

            case 'upload_cover':
                $this->upload_cover();
                return false;
                break;

            case 'delete_cover':
                $this->delete_cover();
                return false;
                break;

            case 'rename_file':
                $this->rename_file();
                return false;
                break;

            case 'delete_file':
                $this->delete_file();
                return false;
                break;

            case 'get_content':
                $this->get_content();
                return false;
                break;

            default:
                $this->show_main();
        }
        return true;
	}


	private function show_main() {
		$folder_id = (isset($_GET['fid'])) ? intval($_GET['fid']) : 0;
		$this->get_files($folder_id);

		$doc = array('head'=>'Загрузка файлов');
		$this->smarty->assign('doc', $doc);

		$this->controller->body = $this->smarty->fetch('files.tpl');
	}


	private function get_files($folder_id = 0) {
//		$path = 'http://'.$_SERVER['HTTP_HOST'].USER_FILES_PATH;
		$path = USER_FILES_PATH;
//		echo '&nbsp;&nbsp;'.$path;

		$folder = $this->db->getRow("SELECT * FROM files WHERE id = {$folder_id} AND type = 'folder'");
		if (is_array($folder) and count($folder)) {
			$folder['short_title'] = (mb_strlen($folder['title']) > 80) ? mb_substr($folder['title'], 0, 80).'...' : $folder['title'];
			$this->smarty->assign('fl_folder', $folder);
		}

		$list = $this->db->getAll("SELECT * FROM files WHERE parent_id = {$folder_id} ORDER BY type DESC");
		if (!is_array($list) or !count($list)) {
			return false;
		}

		foreach ($list as $k=>$row) {
			$title = $row['title'];
			if ($row['type'] == 'file') {
				$list[$k]['title_short'] = (mb_strlen($title) > 55) ? mb_substr($title, 0, 55).'...' : $title;
				$link = "{$path}{$row['fullpath']}{$row['hash']}.{$row['ext']}";
				$list[$k]['link'] = $link;
				$list[$k]['size'] = Tools::convert_file_size($row['size']);
				$list[$k]['full_link'] = $link."/{$row['title']}.{$row['ext']}";
			} elseif ($row['type'] == 'folder') {
				$list[$k]['title_short'] = (mb_strlen($title) > 80) ? mb_substr($title, 0, 80).'...' : $title;
				$list[$k]['children'] = $this->db->getOne("SELECT COUNT(*) FROM files WHERE parent_id = {$row['id']}");
			}
		}

		$this->smarty->assign('fl_files', $list);
	}


	private function upload_files() {
		if (!isset($_POST['folder_id'])) {
			header("location: /_bo/?action=filelibrary");
			exit;
		}

		$fullpath = 'files/';

		$folder_id = $parent_id = intval($_POST['folder_id']);
		if ($folder_id) {
			$folder = $this->db->getRow("SELECT * FROM files WHERE id = {$folder_id} AND type = 'folder'");
			if (!is_array($folder) or !count($folder)) {
				header("location: /_bo/?action=filelibrary");
				exit;
			}
			$fullpath = str_replace('../', '', $folder['fullpath']);
		}

		$dest_path = USER_FILES_DIRECTORY.$fullpath;

		if (!file_exists($dest_path)) {
			header("location: /_bo/?action=filelibrary");
			exit;
		}

		$files = $_FILES['file'];
		foreach ($files['name'] as $key=>$name) {
			$error = $files['error'][$key];
			$tmp_name = $files['tmp_name'][$key];
			$size = $files['size'][$key];
			$type = $files['type'][$key];
			$title = trim($_POST['file_title'][$key]);

			if (!$name or !$tmp_name or !$size or $error) continue;

//			$size = str_replace('.', ',', round($size/1024, 2));

			$file_info = pathinfo($name);
			$ext = strtolower($file_info['extension']);
			$deny_extensions = array('js', 'php', 'html', 'htm', 'tpl', 'as', 'exe', 'cab', 'cmd', 'bat');
			if (in_array($ext, $deny_extensions)) continue;

			$hash = md5($name);
			$destination = $dest_path.$hash.'.'.$ext;

			if (file_exists($destination)) {
				unlink($destination);
			}

			if (!$title) {
				$p = strrpos($name, '.');
				$title = substr($name, 0, $p);
			}

			$res = @rename($tmp_name, $destination);
			if ($res) {
				@chmod($destination, 0644);
				$create_date = time();
				$this->db->insert('files', compact('parent_id', 'hash', 'title', 'ext', 'create_date', 'size', 'fullpath'));
			}
		}

		header("location: /_bo/?action=filelibrary&fid={$folder_id}");
		exit;
	}

    private function upload_cover() {
        if (!isset($_POST['id'])) exit;
        $id = intval($_POST['id']);
        if (!$id) exit;

        $file_path = USER_FILES_DIRECTORY . 'files/';
        $file_folder = USER_FILES_PATH . 'files/';

        //config
        $img_w = '230';

        if (isset($_FILES['file'])) {

            //get file ext
            $file_ext = strrchr($_FILES['file']['name'], '.');

            //hash search by id
            $data = $this->db->getRow("SELECT hash, cover FROM files WHERE id = {$id}");

            //get file name
            $file = $file_path . $data['hash'] . '_cover' . $file_ext;
            $file_name = $file_folder . $data['hash'] . '_cover' . $file_ext;
            $uploaded_file = $_FILES['file']['tmp_name'];

            //check if its allowed or not
            $whitelist = array('.jpg','.jpeg','.png');
            if (!(in_array($file_ext, $whitelist))) {
                echo json_encode(array('success' => false, 'error' => 'Не верный тип файла. Разрешены только jpg, jpeg, png'));
                return;
            }

            //check upload type
            $pos = strpos($_FILES['file']['type'],'image');
            if($pos === false) {
                echo json_encode(array('success' => false, 'error' => 'Файл не является картинкой'));
                return;
            }

            //check directory
            if (!file_exists($file_path))
                Tools::create_directory($file_path, 0777);


            if ($uploaded_file && is_uploaded_file($uploaded_file)) {
                $img = Image::CreateFromFile($uploaded_file);
                $img->resize($img_w);
                $img->save($file, 'jpeg');

                $data['cover'] = 1;

            } else {
                echo json_encode(array('success' => false, 'error' => 'Ошибка загрузки файла. Попробуйте еще раз.'));
                return;
            }


            $this->db->update('files', $data, 'id = '.$id);
            echo json_encode(array('success' => true, 'link' => $file, 'img' => $file_name));

        }
    }

    private function delete_cover() {
        if (!isset($_POST['id'])) exit;
        $id = intval($_POST['id']);
        if (!$id) exit;

        $file_path = USER_FILES_DIRECTORY . 'files/';

        //hash search by id
        $data = $this->db->getRow("SELECT hash, cover FROM files WHERE id = ?", array($id));

        //get file name
        $file = $file_path . $data['hash'] . '_cover.jpg';
        @unlink($file);
        $data['cover'] = 0;
        $this->db->update('files', $data, 'id = '.$id);
        echo json_encode(array('success' => true));
    }

	private function delete_file() {
		if (!isset($_POST['id'])) exit;
		$id = intval($_POST['id']);
		if (!$id) exit;
		$row = $this->db->getRow("SELECT * FROM files WHERE id = $id");
		if (!is_array($row) or !count($row)) exit;

		if ($row['type'] == 'folder') {
			$children_exist = $this->db->getOne("SELECT 1 FROM files WHERE parent_id = {$id}");
			if ($children_exist) exit;
		}

		$this->db->delete('files', "id = $id");
		extract($row);
		$path = USER_FILES_DIRECTORY.$fullpath;
		if ($row['type'] == 'folder') {
			if (file_exists($path)) {
				@rmdir($path);
			}
		} elseif ($row['type'] == 'file') {
			$file = $path.$hash.'.'.$ext;
			if (file_exists($file)) {
				@unlink($file);
			}
		}

		echo 1;
	}


	private function rename_file() {
		if (!isset($_POST['id']) or !isset($_POST['title'])) exit;
		$id = intval($_POST['id']);
		$title = trim($_POST['title']);
		if (!$id or !$title) exit;

		$data = $this->db->getRow("SELECT * FROM files WHERE id = $id");
		if ($data['type'] == 'folder') {
			$isset = $this->db->getOne("SELECT 1 FROM files WHERE parent_id = {$data['parent_id']} AND type = 'folder' AND title = '{$title}' AND id <> {$id}");
			if ($isset) {
				echo 'Папка с таким именем уже существует';
				exit;
			}
		} elseif ($data['type'] == 'file') {
			$isset = $this->db->getOne("SELECT 1 FROM files WHERE parent_id = {$data['parent_id']} AND type = 'file' AND ext = '{$data['ext']}' AND title = '{$title}' AND id <> {$id}");
			if ($isset) {
				echo 'Файл с таким именем уже существует';
				exit;
			}
		}

		$this->db->update('files', compact('title'), "id = {$id}");
		echo 1;
	}


	private function create_folder() {
		if (!isset($_POST['folder_id']) or !isset($_POST['title'])) exit;
		$folder_id = intval($_POST['folder_id']);
		$title = str_replace(array('../', '/', './', '\\', '*', ':', '@', '#', '%', '^', '$', '?', '&', '"', "'", '№', '>', '<'), '', trim($_POST['title']));

		if (!$title) {
			echo "Недопустимое имя файла";
			exit;
		}

		$exist = $this->db->getOne("SELECT 1 FROM files WHERE parent_id = {$folder_id} AND type = 'folder' AND title = '{$title}'");
		if ($exist) {
			echo 'Директория с таким именем уже существует';
			exit;
		}

		if ($folder_id) {
			$parent = $this->db->getRow("SELECT * FROM files WHERE id = {$folder_id} AND type = 'folder'");
			if (!is_array($parent) or !count($parent)) {
				echo 'Родительская директория не найдена';
				exit;
			}
			$fullpath = str_replace('../', '', $parent['fullpath']);
		} else {
			$fullpath = 'files/';
		}

		$parent_id = $folder_id;
		$type = 'folder';
		$create_date = time();
		$id = $this->db->insert('files', compact('parent_id', 'type', 'create_date', 'title'));

		if (!$id or (PEAR::isError($id))) {
			echo 'Ошибка базы данных. Невозможно создать запись';
			exit;
		}

		$fullpath .= $id.'/';

		$path = USER_FILES_DIRECTORY.$fullpath;
		if (file_exists($path)) {
			echo "Директория с таким идентификатором уже существует [{$id}]";
			exit;
		}

		$res = @mkdir($path);
		if ($res) {
			@chmod($path, 0777);
			$this->db->update('files', array('fullpath'=>$fullpath), "id = {$id}");
			echo 1;
		} else {
			echo "!{$path}]\r\n";
		}
	}


	private function get_content() {
		$folder_id = (isset($_POST['folder_id'])) ? intval($_POST['folder_id']) : 0;
		$this->get_files($folder_id);
		if (isset($_POST['ck_name'])) {
			$ck_name = trim($_POST['ck_name']);
			$this->smarty->assign('fl_ck_name', $ck_name);
		}
		$this->smarty->display('files_widget.tpl');
	}


}