<?
class Page {
	
	/**
	 * @var SmartyHolder
	 */
	var $smarty;

	private static $instance = null;
	public $doc = array();
	public $vars = array();
	private $title = array();
	

	/**
	 * @return Page
	 */
	public static function GetInstance() {
		if (is_null(self::$instance)) {
			self::$instance = new Page();
		}
		return self::$instance;
	}

	function __construct() {
		$this->doc['body'] = '';
	}

	function init() {
		$db = MyDB2::GetInstance();
        $loc = Location::getInstance();
		$fullUri = $loc->get_string();

		$sql = "SELECT * FROM tree WHERE publish_time < NOW() AND fullpath = '{$fullUri}'";
		$doc = $db->getRow($sql);
		if (is_array($doc) and count($doc)) {
			foreach ($doc as $k=>$val) {
				$this->doc[$k] = $val;
			}
			return $doc['id'];
        } else {
            /**
             * todo: костыль к показу на встрече
             */
        	if ($fullUri == '/company/leaders/') {
        		return null;
        	}

            $fullUri = '/'.$loc->first().'/';
            $sql = "SELECT * FROM tree WHERE publish_time < NOW() AND fullpath = '{$fullUri}'";
            $doc = $db->getRow($sql);
            if (is_array($doc) and count($doc)) {
                foreach ($doc as $k=>$val) {
                    $this->doc[$k] = $val;
                }
                return $doc['id'];
            }
        }
		
		return false;
	}

	public function add_title($str) {
		$this->title[] = $str;
	}

	public function show_title($sep = ' » ') {
		if (count($this->title)) {
			echo implode($sep, array_reverse($this->title));
		} elseif (isset($this->vars['name_site'])) {
			echo $this->vars['name_site'];
		} else {
			echo 'Заголовок сайта';
		}
	}

	public function get_browser() {
		if (!isset($_SERVER['HTTP_USER_AGENT'])) return;
		$browser = '';
		$user_agent = $_SERVER['HTTP_USER_AGENT'];
		if (strpos($user_agent, 'MSIE 6')) {
			$browser = 'ie6';
		} elseif (strpos($user_agent, 'MSIE 7')) {
			$browser = 'ie7';
		} elseif (strpos($user_agent, 'MSIE 8')) {
			$browser = 'ie8';
		} elseif (strpos($user_agent, 'MSIE 9')) {
			$browser = 'ie9';
		} elseif (strpos($user_agent, 'Gecko') and strpos($user_agent, 'Firefox')) {
			$browser = 'firefox';
		} elseif (strpos($user_agent, 'Presto') and strpos($user_agent, 'Opera')) {
			$browser = 'opera';
		} elseif (strpos($user_agent, 'AppleWebKit') and strpos($user_agent, 'Chrome')) {
			$browser = 'chrome';
		} elseif (strpos($user_agent, 'AppleWebKit') and strpos($user_agent, 'Safari') and !strpos($user_agent, 'Chrome')) {
			$browser = 'safari';
		}
		return $browser;
	}

	function show_404() {
		header("HTTP/1.0 404 Not Found");
		$this->add_title('Страница не найдена');
		$smarty = SmartyHolder::GetInstance(); /** @var SmartyHolder */
		$tm = TreeMenu::GetInstance();
		$sitemap = $tm->get_tree();
		$smarty->assign('sitemap', $sitemap);
		$this->doc['body'] = $smarty->fetch('e404.tpl');
		$smarty->assign('doc', $this->doc);
		$smarty->display('inner.tpl');
	}


}