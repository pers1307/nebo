<?
class Load_Modules {

	private $dir;
	private $file;

	private $arr = array();

	private static $instance = null;

	/**
	 * @return Load_Modules
	 */
	public static function GetInstance() {
		if (is_null(self::$instance)) {
			self::$instance = new Load_Modules();
		}
		return self::$instance;
	}


	function __construct() {
		$this->file = R_Lib.'modules.txt';
		$this->dir = R_Modules;
		$this->research();
		$this->read_arr();
//		echo '<pre>'; var_dump($this->arr); exit;
	}


	public function research() {
		$arr = array();
		if (file_exists($this->dir) and is_dir($this->dir)) {
			$d = dir($this->dir);
			while (false !== ($entry = $d->read())) {
				if (($entry == '.') or ($entry == '..') or ($entry == 'Main')) continue;
				$arr[$entry] = array();
				$external = $this->dir."{$entry}/{$entry}.php";
				if (file_exists($external)) {
					$arr[$entry]['ext'] = $external;
					$arr[$entry]['ext_class'] = $entry;
				}
				$internal = $this->dir."{$entry}/ACP_{$entry}.php";
				if (file_exists($internal)) {
					$arr[$entry]['acp'] = $internal;
					$arr[$entry]['acp_class'] = 'ACP_'.$entry;
				}
				$name_file = $this->dir."{$entry}/name.txt";
				$name = $entry;
				if (file_exists($name_file)) {
					$name = (is_readable($name_file)) ? file_get_contents($name_file) : $entry;
				}
				$arr[$entry]['name'] = $name;
			}
			$d->close();
		}
//		echo '<pre>';var_dump($arr);
// 		file_put_contents($this->file, serialize($arr));
		$this->arr = $arr;
	}


	private function read_arr() {
		if (!file_exists($this->file)) {
			$this->research();
		} else {
			$str = file_get_contents($this->file);
			$this->arr = unserialize($str);
		}
	}


	public function get_ext($name) {
		if (!count($this->arr)) return false;
		if (array_key_exists($name, $this->arr)) {
			if (isset($this->arr[$name]['ext'])) {
				return $this->arr[$name]['ext'];
			}
		}
		return false;
	}


	public function get_acp($name) {
		if (!count($this->arr)) return false;
		if (array_key_exists($name, $this->arr)) {
			if (isset($this->arr[$name]['acp'])) {
				return $this->arr[$name]['acp'];
			}
		}
		return false;
	}


	/**
	 * Возвращаем массив всех модулей для админки
	 * в виде имя_класса=>массив([file]=>путь_к_файлу, [name]=>название)
	 *
	 * @return array
	 */
	public function get_acp_array() {
		$out = array();
		if (count($this->arr)) {
			foreach ($this->arr as $name=>$arr) {
				if (!isset($arr['acp'])) continue;
				$out[$name] = array();
				$out[$name]['file'] = $arr['acp'];
				$out[$name]['class'] = $arr['acp_class'];
				$out[$name]['name'] = $arr['name'];
			}
		}
		return $out;
	}


	/**
	 * Возвращаем массив всех модулей для сайта
	 * в виде имя_класса=>массив([file]=>путь_к_файлу, [name]=>название)
	 *
	 * @return array
	 */
	public function get_ext_array() {
		$out = array();
		if (count($this->arr)) {
			foreach ($this->arr as $name=>$arr) {
				if (!isset($arr['ext'])) continue;
				$out[$name]['file'] = $arr['ext'];
				$out[$name]['class'] = $arr['ext_class'];
				$out[$name]['name'] = $arr['name'];
			}
		}
		return $out;
	}

}