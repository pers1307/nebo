<?php
class ControlArea extends MyAuth {

	var $tpl;
	var $utils;
	var $Dests = array();
	var $user;
	var $doc_id;
	var $module;
	var $action;
	var $lmenu = array();
	var $_tree = array();
	var $body;
	var $acp_modules = array();
	var $ext_modules = array();

    private $folder = 'tree/';
    private $path;



	var $changefreq = array('monthly'=>'Ежемесячно', 'weekly'=>'Еженедельно', 'daily'=>'Ежедневно', 'hourly'=>'Ежечасно', 'yearly'=>'Ежегодно', 'always'=>'Всегда', 'never'=>'Никогда');

	function __construct() {
		parent::__construct();

		$this->clearPreviews();
		$this->tpl = SmartyHolder::GetInstance(R_tpl_acp);
		$this->utils = new Utils();
        $this->startAuth(true);

		$Load_Modules = Load_Modules::GetInstance();
		$this->acp_modules = $Load_Modules->get_acp_array();
		$this->ext_modules = $Load_Modules->get_ext_array();
		$this->_tree = $this->get_tree();

        $this->path = USER_FILES_DIRECTORY . $this->folder;
        $this->folder = USER_FILES_PATH . $this->folder;

		$this->tpl->assign('_tree', $this->_tree);
		$this->tpl->assign('modules_dir', R_Modules);
		$this->tpl->assign('user_status', $this->user['status']);
		$this->tpl->assign('user_login', $this->user['login']);


	}

	function displayEditor() {
		$id = Utils::parseGet("doc_id");
		$action = Utils::parseGet('action', false);
		$this->action = $action;

		if ($id) {
			$page = $this->db->getRow("SELECT id, head, module, fullpath FROM tree WHERE id = $id");
			if (is_array($page) and count($page)) {
				$this->doc_id = $id;
				if (isset($_GET['module'])) {
					$module = $this->db->getOne("SELECT module FROM tree WHERE id = $id");
					if ($module and (array_key_exists($module, $this->acp_modules))) {
						$module_file = $this->acp_modules[$module]['file'];
						if ($module_file and file_exists($module_file)) {
							require_once($module_file);
							$class = $this->acp_modules[$module]['class'];
							$handler = new $class;
							if (is_callable(array($handler, 'getContent'))) {
								$this->module = $module;
								$page['acp_module'] = 1;
								$this->tpl->assign('doc', $page);
								$this->tabs = $this->tpl->fetch('tabs.tpl');
								if (!$handler->getContent($this)) exit;
								$this->displayBO();
								exit;
							}
						}
					}
				}
			}
		}

		// -------------------------------------

		if (method_exists($this, $action)) {
			call_user_func(array($this, $action));
			exit;
		}

		if ($id) {
			$this->editDoc($id);
		} else {
			$this->splashEditor();
		}
	}

	function displayBO() {
		$top_menu = $this->get_top_menu();

		if ($this->module) {
			$this->tpl->assign('body', $this->body);
			$this->body = $this->tpl->fetch('module.tpl');
		}

		$this->tpl->assign('top_menu', $top_menu);
		$this->tpl->assign('body', $this->body);
		$this->tpl->assign("URL", URL);
		$this->tpl->display('main.tpl');
	}

	private function get_top_menu() {
		$user_modules = explode(';', $this->user['bo']);
		$menu = array();
		/*
		// в верхнем меню показывать модули
		foreach ($this->acp_modules as $module=>$val) {
			if (($this->user['status'] != 'admin') and !in_array($module, $user_modules)) continue;
			$menu[] = array('head'=>$val['name'], 'url'=>'?module='.$module, 'name'=>$module);
		}*/

		if ($this->user['status'] == 'admin') {
			//$menu[] = array('head'=>'Пользователи','url'=>'?action=boAccess');
            $menu[] = array('head'=>'Главная страница','url'=>'?action=mainpage');
            $menu[] = array('head'=>'Настройки','url'=>'?action=settings');
            $menu[] = array('head'=>'Загрузка файлов','url'=>'?action=filelibrary');
            $menu[] = array('head'=>'Галереи','url'=>'?action=galleries');
		}
		return $menu;
	}

	function formatChilds($id) {
		$childs = $this->getChilds($id, false, false);

		$hidden = '<img src="/_bo/images/eye.gif" width="16" height="10" alt="не отображать в меню" border="0" hspace="1" />';
		$locked = '<img src="images/locked.gif" width="11" height="12" alt="Редактирование запрещено" border="0" hspace="1" />';
		$dynamic = '<span style="color:red;" title="Страница обрабатывается модулем">М&nbsp;</span>';

		if (!is_array($childs) or empty($childs)) {
			return array();
		} else {
			$out = array();
			foreach ($childs as $child) {
				if (($child['editable'] == 0) && ($this->user['status'] != 'admin')) continue;
				if (($child['module'] != '') && ($this->user['status'] != 'admin')) continue;

				$ac = 1;
				if (!empty($ac)) {
					$access = $this->checkAccess($child['id'], "e");
					if (empty($access)) continue;
				}

				$attrs = '';

				if ($module = $child['module']) {
					if (in_array($module, $this->acp_modules)) {
						$attrs .= $dynamic;
					}
				}

				if ($child['editable'] == 0) $attrs .= $locked;
				if ($child['menushow'] == 0) $attrs .= $hidden;

				$head = $attrs.$child['head'];

				$out[] = array('head'=>$head,'url'=>"?id=".$child['id']);
			}
			if (!empty($out)) $out[]=array('head'=>'<img src="images/change_order.gif" border="0" width="140" height="15" alt="изменить порядок">','url'=>'?action=sort&id='.$id);
			return $out;
		}
	}

	function getChilds($id) {
		$where = (is_array($id)) ? 'parent IN ('.implode(",", $id).')' : 'parent = '.intval($id);
		$sql = "SELECT id, parent, head, fname, editable, menushow, priority, fullpath, module FROM tree WHERE $where ORDER BY priority";
		$res = $this->db->getAll($sql);
		if (PEAR::isError($res)) die($res->getMessage());
		return $res;
	}

	function formatAcentors($id) {
		$ac = $this->getAcentors($id, false);
		$out = array();

		if (empty($ac)) return $out;

		$ac = explode("-", $ac);
		$ac = $this->array_clean($ac);

		foreach ($ac as $k=>$v) {
			$hh = $this->getDocHead($v, false);
			$out[] = array('head'=>$hh['head'], 'url'=>'?id='.$v);
		}
		return $out;
	}


    function splashEditor() {
		$user = $this->getUserParams();

        if ($user['id'] == '9') { /* TODO Редирект для пользователя hr. Временно */
            header("location: ?doc_id=15&module");
        }


        // Блок добавления новости
        // Определяем с каким id раздел новостей
        $newsId = 0;
        foreach ($this->_tree as $arr) {
            if (strtolower($arr['module']) == 'news')  {
                $newsId = $arr;
                break;
            }
        }

        foreach ($this->_tree as $arr) {
            if (strtolower($arr['module']) == 'feedback')  {
                $feedbackId = $arr;
                break;
            }
        }


        $modules = $this->db->getAll('SELECT id, module FROM tree WHERE module<>""');
        if (!PEAR::isError($modules)) {
            foreach ($modules as $km=>$m) {
                $module_acp = R_Modules.'/'.$m['module'].'/ACP_'.$m['module'].'.php';
                if (!file_exists($module_acp)) {
                    unset($modules[$km]);
                    continue;
                }

                $modules[$km]['name'] = '';
                $module_name = R_Modules.'/'.$m['module'].'/name.txt';
                if (file_exists($module_name)) {
                    $modules[$km]['name'] = trim(file_get_contents($module_name));
                }
            }
        }



        // Непрочитанные сообщения
        $msg_sql = "SELECT id, fio, message, create_date, ready FROM feedback WHERE ready IS NULL ORDER BY create_date DESC LIMIT 5";
        $msg_count = "SELECT count(*) as total from feedback WHERE ready IS NULL";
        $messages = $this->db->getAll($msg_sql);
        $messages_count = $this->db->getOne($msg_count);

        $this->tpl->assign('news_id', $newsId); // Отправляем id раздела в шаблон
        $this->tpl->assign('feedback_id', $feedbackId); // Отправляем id раздела в шаблон
        $this->tpl->assign('messages', $messages);
        $this->tpl->assign('messages_count', $messages_count);
        $this->tpl->assign('modules_list', $modules);
		$this->body = $this->tpl->fetch('editor_index.tpl');
		$this->displayBO();
	}

	function editDoc($id) {
		$access = $this->checkAccess($id, "e");

		if (!$access) {
			echo Utils::redirectBack('У Вас нет прав на изменение данного подраздела');
			exit;
		}

		$id = intval($_GET['doc_id']);
        $doc = $this->getDoc($id, false);
		$access = $this->checkAccess($id, "e");
		if (!$access) {
			echo Utils::redirectBack('У Вас нет прав на изменение данного подраздела');
			exit;
		}

		if ($doc = $this->getDoc($id, false)) {
			$doc = array_map('htmlspecialchars', $doc);
			$doc['uri'] = $this->makeUri($id);
			$doc['mtime'] = $this->utils->timeC($doc['mod_time']);
			$doc['ctime'] = $this->utils->timeC($doc['create_time']);
			if (empty($doc['publish_time']) || $doc['publish_time']=='0000-00-00 00:00:00') $doc['publish_time'] = date('Y-m-d H:i:s');

            /* Изображение для страницы */
            $img_head_name = $doc['parent'].'_'.$doc['id'];
            $doc['img'] = $doc['img_src'] = str_replace(ROOT, URL, Utils::findImage($img_head_name, ROOT.$this->folder));

			if ($doc['module']) $doc['acp_module'] = array_key_exists($doc['module'], $this->acp_modules);

			$user = $this->getUserParams();
			$this->tpl->assign('modules', $this->ext_modules);
			$this->tpl->assign('doc', $doc);
			$this->tpl->assign('changefreq', $this->changefreq);
			$this->body = $this->tpl->fetch("editor.tpl");
			$this->self = 'news';
			$this->displayBO();
		} else {
			$this->splashEditor();
		}
	}

	function getDoc($id, $filter = true) {
		if (empty($id)) return false;
		$where = ($filter) ? "AND publish_time < NOW()" : '';
		if (is_array($id)) {
			$ids = implode(",", $id);
			$res = $this->db->getAll("SELECT * FROM tree WHERE id IN ({$ids}) $where");
			if (PEAR::isError($res)) die($res->getMessage());
		} else {
			$id = intval($id);
			if ($id == 0) return false;
			$res = $this->db->getRow("SELECT * FROM tree WHERE id = {$id} $where"); // one row
			if (PEAR::isError($res)) die($res->getMessage());
		}
		return $res;
	}

	function makeUri($id, $filter = true) {
		if (empty($id)) return false;

		$where = ($filter)? "AND publish_time < NOW()" : "";
		$doc = $this->db->getRow("SELECT 1 FROM tree WHERE id = $id $where");
		if (PEAR::isError($doc)) return false;

		$ac = $this->getAcentors($id, false);
		$ac = explode("-", $ac);
		$uri = array();
		foreach ($ac as $a) {
			if ($a == 0) {
				$uri[] = "";
			} else {
				$d = $this->getDocHead($a);
				$uri[] = $d['fname'];
			}
		}
		$out = implode("/", $uri);
		return $out."/";
	}

	function getDocHead($id, $filter = true) {
		if (empty($id)) return false;
		$id = intval($id);
		$where = ($filter) ? "AND publish_time < NOW()" : '';
		$res = $this->db->getRow("SELECT id, head, fname, parent, fullpath, module, editable FROM tree WHERE id = {$id} $where");
		if (PEAR::isError($res)) die($res->getMessage());
		return $res;
	}

	function getAcentors($id, $filter = true) {
		$res = $this->getDocHead($id, $filter);
		if (!empty($res)) {
			if ($res['parent'])  {// or intval($r['parent']) === 0)
				return $this->getAcentors($res['parent'], $filter).'-'.$res['id'];
			} else {
				return $res['parent'].'-'.$res['id'];
			}
		}
		else return null;
	}

	function createDoc() {
		if (empty($_POST)) exit;
		$fname = strtolower(strip_tags(trim($_POST['new_fname'])));
		$head = strip_tags(trim($_POST['new_head']));
		$parent = intval($_POST['parent']);
		if (!$fname or !$head) {
			echo Utils::redirectBack('Введите название и URL');
			exit;
		}
		if (($parent == 0) and ($this->user['status'] != 'admin')) {
			echo Utils::redirectBack("Невозможно создать раздел первого уровня");
			exit;
		} elseif ($parent) {
			$access = $this->checkAccess($parent, "c");
			if (!$access) {
				echo Utils::redirectBack('У Вас нет прав на создание подраздела в данном разделе');
				exit;
			}
		}

		if (preg_match("/[^a-z0-9_]/", $fname)) {
			echo Utils::redirectBack("В имени файла допустимы только английские буквы, цифры и символ подчеркивания. Пробелы недопустимы");
			exit;
		}
		if ($parent) {
			$father = $this->getDoc($parent, false);
		} else {
			$father = array();
			$father['id'] = 0;
			$father['rights'] = '';
		}
		$wdata['title'] = $head;
		$wdata['title_h1'] = $head;
		$wdata['module'] = '';
		$wdata['rights'] = $father['rights'];
		$wdata['head'] = $head;
		$wdata['fname'] = $fname;
		$wdata['parent'] = $parent;
		$wdata['menushow'] = 0;
		$fatherUri = $this->makeUri($parent, false);
		if ($fatherUri) {
			$wdata['fullpath'] = $fatherUri.$fname.'/';
		} else {
			$wdata['fullpath'] = '/'.$fname.'/';
		}
		$childs = $this->getChilds($parent, false); // берем детей в т.ч. неопубликованных

		if (!empty($childs) && $this->utils->array_search_r($fname, $childs)) {
			echo Utils::redirectBack("Документ с таким именем уже существует в данном разделе");
			exit;
		}
		if (!empty($childs) && $this->utils->array_search_r($head, $childs)) {
			echo Utils::redirectBack("Документ с таким названием уже существует в данном разделе");
			exit;
		}
		$wdata['mod_time'] = Utils::mytime();
		$wdata['create_time'] = Utils::mytime();
		$wdata['editable'] = 1;
		$wdata['publish_time'] = date("Y-m-d H:i:s", ( time()+DOC_UNPUBLISH_TIMEOUT ) );

		$wdata = $this->utils->undoSlashes($wdata);

		$id = $this->db->insert('tree', $wdata);
		if (PEAR::isError($id)) {
			die($id->getMessage());
		}
		$this->refreshUrl();
		header("location: ?doc_id={$id}");
	}

	function deleteDoc() {
		$id = intval($_GET["id"]);
		$access = $this->checkAccess($id, "d");
		if (!$access) {
			echo Utils::redirectBack('У Вас нет прав на удаление данного подраздела');
			exit;
		}
		if ($id) {
			$doc = $this->getDoc($id, false);
			if (empty($doc)) {
				Utils::redirectBack("Документ не найден");
			}

			$level = $this->getCurrentLevel($id);
			$user = $this->getUserParams();
			if (($level < 2) && ($user['status'] != 'admin')) {
				Utils::redirectBack("Невозможно удалить корневой раздел");
			}

			$a = $this->deleteDocRecursive($doc['id']);
			if ($a) {
				header("location: ?doc_id={$doc['parent']}");
			} else {
				header("location: ?doc_id={$id}");
			}
		}
	}

	function deleteDocRecursive($id) {
		$id = intval($id);
		$doc = $this->db->getRow("SELECT 1 FROM tree WHERE id = $id");
		if (PEAR::isError($doc)) return false;
		$ch = $this->getChilds($id, false);

		if (!empty($ch) && is_array($ch)) {
			foreach ($ch as $cc) $this->deleteDocRecursive($cc['id']);
		}
		$this->db->delete('tree', 'id='.$this->db->quoteSmart($id));
		return true;
	}

	function saveDoc() {
		$id = intval($_POST['id']);

		$access = $this->checkAccess($id, "e");
		if (!$access) {
			echo json_encode(array('type'=>'info', 'title'=>'Нет доступа', 'answer'=>'У Вас нет прав на изменение данного подраздела'));
			return;
		}

		$_POST = array_map("trim", $_POST);
		$category = $_POST['category'];
		if (!$id or !$category) return;

		$doc = $this->getDocHead($id, false);
		if ($doc and (($doc['editable'] != 1) && ($this->user['status'] != 'admin'))) {
			echo json_encode(array('type'=>'info', 'title'=>'Нет доступа', 'answer'=>'Редактирование данного раздела запрещено'));
			return;
		}

		$data = array();
		if ($category == 'text') {
			$data['body_top'] = stripslashes($_POST['body_top']);
			$data['body'] = stripslashes($_POST['body']);
		}
		if ($category == 'seo') {
			$data['title'] = stripslashes($_POST['title']);
			$data['title_h1'] = stripslashes($_POST['title_h1']);
			$data['meta_description'] = stripslashes($_POST['meta_description']);
			$data['meta_keywords'] = stripslashes($_POST['meta_keywords']);
			$data['changefreq'] = null;
			if (isset($_POST['changefreq'])) {
				$changefreq = $_POST['changefreq'];
				if (isset($this->changefreq[$changefreq])) {
					$data['changefreq'] = $changefreq;
				}
			}
            $data['sitemap'] = ($_POST['sitemap'] == 'true') ? 1 : 0;
		}
		if ($category == 'options') {
			$fname = preg_replace('/[^a-z0-9\_\-]/', "", $_POST['fname']);
			if (!$fname) {
				echo json_encode(array('type'=>'error', 'title'=>'Ошибка', 'answer'=>'Не указан URL'));
				return;
			}
			if (!$_POST['head']) {
				echo json_encode(array('type'=>'error', 'title'=>'Ошибка', 'answer'=>'Не указано название страницы'));
				return;
			}

			$redirect = $_POST['redirect'];
			if ($doc['fullpath'] != $redirect) {
				$data['redirect'] = $redirect;
			}
			$data['fname'] = $fname;
			$data['head'] = stripslashes($_POST['head']);
			$data['module'] = $_POST['module'];
			$data['menushow'] = ($_POST['menushow'] == 'true') ? 1 : 0;
			if ($this->user['status'] == 'admin') {
				$data['editable'] = (isset($_POST['editable']) && $_POST['editable'] == 'true') ? 1 : 0;
			}
		}

		$data['mod_time'] = Utils::mytime();
		// $data['publish_time'] = date('Y-m-d', strtotime($_POST['publish_date']));

		if ($doc && (array_key_exists($doc['module'], $this->ext_modules) && ($this->user['status'] != 'admin'))) {
// 			unset($data['editable']);
			unset($data['module']);
			unset($data['publish_time']);
			unset($data['fname']);
//			unset($data['fullpath']);
		}

		$r = $this->db->update('tree', $data, "id = $id");
		if (PEAR::isError($r)) {
			if ($this->user['status'] != 'admin') {
                echo json_encode(array('type'=>'error', 'title'=>'Ошибка', 'answer'=>'Системная ошибка при сохранении'));
                return;
            }
		}
		$this->refreshUrl();

		$module = 0;

        if ($category == 'options') { // TODO: Вроде поправил
            // Тут надо поправить, ругается на $data['module'] т.к. его нет нигде, кроме как на вкладке Настройки
            if (($data['module']) and array_key_exists($data['module'], $this->acp_modules)) {
                $module = 1;
            }
        }

		echo json_encode(array('type'=>'success', 'title'=>'Выполнено', 'answer'=>'Страница успешно сохранена', 'module'=>$module));
		exit;
	}


    function delete_img() {
        if (!isset($_POST['id']) or !isset($_POST['imgkey'])) exit;
        $id = intval($_POST['id']);
        $f = Utils::findImage($id, ROOT.$this->folder, false, false);
        if (is_file($f)) @unlink($f);
        echo 1;
    }

	function previewDoc() {
		$id = intval($_POST['id']);
		$access = $this->checkAccess($id, "e");
		if (!$access) {
			echo Utils::redirectBack('У Вас нет прав на изменение данного подраздела');
			exit;
		}

		$_POST = array_map("trim", $_POST);

		$_POST['fname'] = "tmp".md5(mktime().$_POST['fname']);
		$_POST['fullpath'] = '/'.$_POST['fname'].'/';

		if (empty($_POST['fname'])) Utils::redirectBack("Не указано имя файла");
		if (empty($_POST['head'])) Utils::redirectBack("Не указано название файла");

		$_POST['menushow'] = 0;

		unset($_POST['save']);
		unset($_POST['id']);

		$_POST['mod_time'] = Utils::mytime();

		$_POST['publish_time'] = $_POST['pYear']."-".$_POST['pMonth']."-".$_POST['pDay']." ".$_POST['pHour'].":".$_POST['pMinute'].":00";

		unset($_POST['pYear'],$_POST['pMonth'],$_POST['pDay'],$_POST['pHour'],$_POST['pMinute']);

		$data = $this->undoSlashes($_POST);

		// INSERT
		$r = $this->db->insert('tree', $data);
		if (PEAR::isError($r)) die($r->getMessage());
		$id = $this->db->getLastId();
		$loc = "location: http://".$_SERVER['HTTP_HOST'].$this->makeUri($id)."";

		header($loc);
	}


	function clearPreviews() {
		$this->db->delete('tree', "fname LIKE 'tmp%'");
	}


	function rehash() {
		if ($this->user['status'] != 'admin') {
			header('location: ?action=list');
			exit();
		}
		$this->refreshUrl();
		$this->lmenu = array();
		$this->body = 'Хэши разделов обновлены';
		$this->displayBO();
	}

    /*
	function saveIndex() {
		if ($this->user['status'] != 'admin') {
			header('location: ?action=list');
			exit();
		}
		if (!empty($_POST)) {
			foreach ($_POST as $key=>$val) {
				$type = $this->db->getOne("SELECT type FROM vars WHERE name = '$key' AND visibility = 1");
				if (!PEAR::isError($type) and strlen($type)) {
					if ($type == 'text') {
						$val = stripslashes(trim($val));
					} elseif ($type == 'html') {
						$val = stripslashes(trim($val));
					} elseif ($type == 'str') {
						$val = stripslashes(htmlspecialchars(trim($val)));
					} elseif ($type == 'int') {
						$val = intval($val);
					}
					$this->db->update('vars', array('value'=>$val), "name = '$key'");
				}
			}
		}
		header("location: /_bo/");
		exit;
	}
    */


	function printSiteTree() {
		$this->body = $this->tpl->fetch('tree.tpl').$this->printMapExt(0,0);
		$this->displayBO();
	}


	function sorter() {
		$id = intval($_GET['id']);
		if (empty($_POST)) {
			if (empty($id)) {
				$doc['head'] = 'Корень сайта';
				$doc['id'] = 0;
			} else {
				$doc = $this->getDocHead($id);
			}

			$pp = $this->getChilds($id);
			$this->tpl->assign('rows', $pp);
			$this->body = $this->tpl->fetch('sorter.tpl');
			$this->displayBO();
		} else {
			foreach ($_POST as $pk => $pv) {
				if (!eregi("pr", $pk)) continue;
				$w = array();
				list($jnk, $id) = split("_", $pk);
				$w['priority'] = $pv;

				$this->db->update('tree', $w, "id=".$id);
				unset($w, $id);
			}
			header("location: ?doc_id=".$id);
		}
	}


	function boAccess() {
		if ($this->user['status'] != 'admin') {
			$this->body = 'У вас нет прав доступа к этому разделу';
			$this->displayBO();
			return;
		}

		$u = Utils::parseGet('login', 0);
		if ($u) {
			$user = $this->getUserParams($u);
			if (is_array($user) and ($user['status'] != 'admin')) {
				$user_modules = explode(';', $user['bo']);
				$access_table = $this->buildAccessTable($u);
				$this->tpl->assign('access_div', $access_table);
				$this->tpl->assign('u', $user);
				$this->tpl->assign('modules', $this->acp_modules);
				$this->tpl->assign('user_modules', $user_modules);
			}
			$this->body = $this->tpl->fetch('access.user.tpl');
		} else {
			$users = $this->db->getAll("SELECT uid, login, email FROM users WHERE status <> 'admin' ORDER BY login");
			if (PEAR::isError($users)) {
				$users = array();
			}
			$this->tpl->assign('user_list', $users);
			$this->body = $this->tpl->fetch('access.user.list.tpl');
		}

		$this->displayBO();
	}


	function saveUser() {
		if ($this->user['status'] != 'admin') {
			$this->body = 'У вас нет прав доступа к этому разделу';
			$this->displayBO();
			return;
		}

		$wdata = array();
		if (!isset($_POST['login'])) {
			header("Location: ?action=boAccess");
			exit;
		}
		$login = str_replace(array('"', "'", '%', '<', '>', '*', '&'), '', trim($_POST['login']));
		if ($login) {
			$wdata['login'] = $login;
			if (!empty($_POST['pwd1']) and (strlen($_POST['pwd1']) > 0)) {
				$wdata['passw'] = md5($_POST['pwd1']);
			}
			$wdata['name'] = Utils::parsePost('name', 0);
			$wdata['email'] = Utils::parsePost('email', 0);
			$wdata['regdate'] = Utils::mytime();
			$wdata['status'] = 'user';

			$uid = Utils::parsePost('uid');
			if (!$uid) {
				$this->db->insert('users', $wdata, 'uid');
			} else {
				$this->db->update('users', $wdata, "uid = {$uid}");
			}
		}
		header("Location: ?action=boAccess&login={$login}");
	}


	function saveUserModules() {
		if ($this->user['status'] != 'admin') {
			$this->body = 'У вас нет прав доступа к этому разделу';
			$this->displayBO();
			return;
		}

		$login = Utils::parsePost('login', 0);
		if (!isset($_POST['mod'])) {
			$bo = '';
		} else {
			$bo = implode(';', $_POST['mod']);
		}
		$r = $this->db->update('users', compact('bo'), "login=".$this->db->quoteSmart($login));
		header("Location: ?action=boAccess&login=$login");
		return;
	}


    private function top_img_upload() {
        $id = intval($_GET['doc_id']);
        $doc = $this->getDoc($id, false);


        //config
        $img_w = '935';
        $img_h = '240';


        if (isset($_FILES['file'])) {

            //get file ext
            $file_ext = strrchr($_FILES['file']['name'], '.');

            //get file rename
            $file = $this->path . $doc['parent'] . '_' . $doc['id'] . $file_ext;
            $file_name = $this->folder . $doc['parent'] . '_' . $doc['id'] . $file_ext;
            $uploaded_file = $_FILES['file']['tmp_name'];

            //check if its allowed or not
            $whitelist = array('.jpg','.jpeg','.png');
            if (!(in_array($file_ext, $whitelist))) {
                echo json_encode(array('success' => false, 'error' => 'Не верный тип файла. Разрешены только jpg, jpeg, png'));
                return;
            }

            //check upload type
            $pos = strpos($_FILES['file']['type'],'image');
            if($pos === false) {
                echo json_encode(array('success' => false, 'error' => 'Файл не является картинкой'));
                return;
            }

            $imageinfo = getimagesize($uploaded_file);

            //check size image
            if ($imageinfo[0] < $img_w || $imageinfo[1] < $img_h) {
                echo json_encode(array('success' => false, 'error' => 'Размер изображения должен быть не менее 935x240 пикселей'));
                return;
            }

            if ($imageinfo['mime'] != 'image/jpeg'&& $imageinfo['mime'] != 'image/jpg'&& $imageinfo['mime'] != 'image/png') {
                echo json_encode(array('success' => false, 'error' => 'Не верный тип файла. Разрешены только jpg, jpeg, png'));
                return;
            }


            //check directory
            if (!file_exists($this->path))
                Tools::create_directory($this->path, 0777);


            if ($uploaded_file && is_uploaded_file($uploaded_file)) {
                $img = Image::CreateFromFile($uploaded_file);
                $img->resize($img_w);

                //check image height
                if ($img->height < $img_h) {
                    echo json_encode(array('success' => false, 'error' => 'Соотношение сторон изображения должно быть не менее, чем 1:4. Минимальный размер 935x240.'));
                    return;
                }

                $img->crop(0,0,$img_w,$img_h);
                $img->save($file, 'jpeg');
            } else {
                echo json_encode(array('success' => false, 'error' => 'Ошибка загрузки файла. Попробуйте еще раз.'));
                return;
            }

            echo json_encode(array('success' => true, 'link' => $file, 'img' => $file_name));
        }
    }

    private function top_img_delete() {

        $id = $_POST['id'];

        if (empty($id)) return false;
        $img = Utils::findImage($id, ROOT.$this->folder);
        if ($img) unlink($img);
        exit('1');
    }

	private function deleteUser() {
		if ($this->user['status'] != 'admin') {
			$this->body = 'У вас нет прав доступа к этому разделу';
			$this->displayBO();
			return;
		}

		if (!isset($_GET['uid'])) exit;
		$uid = intval($_GET['uid']);
		if (!$uid) exit;
		$this->db->delete('users', "uid = $uid");
//		$this->db->delete($this->usnc, "user_id = $uid");
		header("Location: ?action=boAccess");
		return;
	}


	function saveUserDivs() {
		if ($this->user['status'] != 'admin') {
			$this->body = 'У вас нет прав доступа к этому разделу';
			$this->displayBO();
			return;
		}

		$this->unpackAccessUpdate();
		header("Location: ?action=boAccess&login=$login");
		return;
	}


	function get_tree($parent = 0) {
        $params = array();
        $cond = "";
        if ($this->user['bo'] && $parent == 0) {
            $cond = " AND (LOWER(fname) = ? OR  LOWER(module) = ?) ";
            $params[] = strtolower($this->user['bo']);
            $params[] = strtolower($this->user['bo']);
        }
		$sql = "SELECT id, parent, head, fname, editable, menushow, fullpath, module FROM tree WHERE parent = $parent $cond ORDER BY priority";
		$tree = $this->db->getAll($sql, $params);

		$arr = false;
		if (is_array($tree) and count($tree)) {
			$arr = array();
			foreach ($tree as $item) {
				$id = $item['id'];
				$module = $item['module'];
				$arr[$id] = array();
				$arr[$id]['id'] = $id;
				$arr[$id]['name'] = $item['head'];
				$arr[$id]['path'] = $item['fullpath'];
				$arr[$id]['module'] = $module;
				$arr[$id]['acp_module'] = array_key_exists($module, $this->acp_modules);
				$arr[$id]['ext_module'] = array_key_exists($module, $this->ext_modules);
				$arr[$id]['menushow'] = $item['menushow'];

				$childs = $this->get_tree($id);
				if ($childs) {
					$arr[$id]['childs'] = $childs;
				}
			}
		}

		return $arr;
	}


	function save_tree_sort() {
		if ($this->user['status'] != 'admin') exit;
		if (!isset($_POST['item_id']) or !isset($_POST['parent_id'])) exit;
		$id = intval(str_replace('tree_item_', '', $_POST['item_id']));
		$parent_id = intval(str_replace('tree_parent_', '', $_POST['parent_id']));
		if (!$id) exit;
		$current_parent_id = $this->db->getOne("SELECT parent FROM tree WHERE id = {$id}");
		if (PEAR::isError($current_parent_id)) exit;
		$current_parent_id = intval($current_parent_id);
		if ($current_parent_id != $parent_id) {
			$this->db->update('tree', array('parent'=>$parent_id), "id = {$id}");
			$this->refreshUrl($parent_id);
			$link = $this->db->getOne("SELECT fullpath FROM tree WHERE id = {$id}");
			echo $link;
		}

		if (!isset($_POST['sort'])) exit;
		$sort = explode(';', $_POST['sort']);
		$sort = Tools::clear_array($sort);
		if (count($sort) < 2) exit;
		$priority = 0;
		foreach ($sort as $str) {
			$id = intval(str_replace('tree_item_', '', $str));
			if (!$id) continue;
			$this->db->update('tree', compact('priority'), "id = {$id}");
			$priority++;
		}
	}


	public function update_load_modules() {
		$Load_Modules = Load_Modules::GetInstance();
		$Load_Modules->research();
		$arr = $Load_Modules->get_ext_array();
//		echo '<pre>';var_dump($arr);echo '</pre>';exit;
		$selected = (isset($_POST['selected'])) ? $_POST['selected'] : '';
		echo '<option value="">Стандартный обработчик</option>';
		foreach ($arr as $key=>$row) {
			echo '<option '.(($selected == $key) ? 'selected="selected"' : '').' value="'.$key.'">'.$row['name'].'</option>';
		}
		exit;
	}


	function refreshUrl($parent = 0, $parent_fullpath = '/') {
		if ($parent > 0) {
			$parent_fullpath = $this->db->getOne("SELECT fullpath FROM tree WHERE id = $parent");
		}
		$list = $this->db->getAll("SELECT id, fname, parent FROM tree WHERE parent = $parent");
		foreach ($list as $doc) {
			extract($doc);
			$fullpath = $parent_fullpath.$fname.'/';
			$this->db->update('tree', compact('fullpath'), "id = $id");
			$this->refreshUrl($id, $fullpath);
		}
	}


	function getCurrentLevel($id = "") {
		if (empty($id)) $id = $this->id;
		$r = $this->getAcentors($id, false);
		if (empty($r)) return 0;
		$r = $this->utils->array_clean(explode("-", $r));
		return count($r);
	}


	function select_uri() {
		$this->tpl->assign('tree', $this->printTree(0, 0));
		$this->tpl->display('select_uri.tpl');
		exit;
	}


	function logout() {
		$this->auth->logout();
		header('location: ?action=list');
		exit;
	}


	private function img_preview() {
		header("Content-type: image/jpeg;");
		$content = file_get_contents(USER_FILES_DIRECTORY.'cube.jpg');
		echo $content;
		exit;

		if (!isset($_GET['file']) or !$_GET['file']) exit;
		$file = str_replace(array('..', '//', '\\'), '', trim($_GET['file']));
		$path = USER_FILES_DIRECTORY;
		$file = $path.$file;
		if (file_exists($file)) {

		}
		exit;
	}

	function printMapExt($id, $lv) {
		$nextlv = $lv+1;
		$self = $this->getDoc($id, 0);
		$buf = "";
		//              if ($self['fname'] == 'error') return;
		//              if ($self['fname'] == 'sitemap') return;
		$ch = $this->getChilds($id, 0);
		if ($id == 0) {
			$buf .= "<a href=\"/\">Главная</a><br>\n";
		} else if ($self['fname'] != '') {
			$buf .= "<a class=\"black\" href=\"".($self['editable'] == 'y'? "?id=".$self['did']."" : "#" )."\">".str_repeat("&nbsp;", $lv*4).$self['head']."</a>";
			if (!empty($self['redirect'])) $buf .= " <span style='color:green' title='переадресация'> ></span> ";
			if ($self['menushow'] == 'n') $buf .= " <span style='color:blue' title='не показывать в меню'>x</span> ";
			if (!$self["publish_time"]) $buf .= " <span style='color:red' title='не опубликовано'>!</span> ";
			$buf .= "<br>\n";
		}
		if (!empty($ch)) {
			foreach ($ch as $child) {
				$buf .= $this->printMapExt($child['did'], $nextlv);
			}
		}
		return $buf;
	}

	// File Library ------------------------------------------------------------
	function filelibrary() {
		$filelibrary = new FileLibrary();
		if ($filelibrary->rewrite($this)) {
			$this->displayBO();
		}
	}

    // Galleries ------------------------------------------------------------
    function galleries() {
        $galleries = new Galleries();
        if ($galleries->rewrite($this)) {
            $this->displayBO();
        }
    }


    // Settings ------------------------------------------------------------
	function settings() {
		$settings = new Settings();
		if ($settings->rewrite($this)) {
			$this->displayBO();
		}
	}
	// -------------------------------------------------------------------------

    // Mainpage ------------------------------------------------------------
    function mainpage() {
        $mainpage = new Mainpage();
        if ($mainpage->rewrite($this)) {
            $this->displayBO();
        }
    }
    // -------------------------------------------------------------------------
}
?>