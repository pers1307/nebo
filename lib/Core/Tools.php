<?
class Tools {

	/**
	 * Создаёт директории
	 * @param string $path
	 * @param hex $chmod
	 */
	static function create_directory($path, $chmod = false) {
		$s = '/';
		$path = str_replace('\\', $s, $path);
		$arr = explode($s, $path);
		$path2 = $arr[0];
		foreach ($arr as $k=>$folder) {
			$folder = trim($folder);
			if (($k == 0) or (!strlen($folder))) continue;
			$path2 .= $s.$folder;
			if (!file_exists($path2)) {
				mkdir($path2);
			}
		}
		if ($chmod) {
			chmod($path, $chmod);
		}
	}


	/**
	 * Возвращает из адреса массив строк
	 * @return array
	 */
	static function getUrl() {
		$uri = $_SERVER['REQUEST_URI'];
		$pos = strpos($uri, '?');
		if ($pos) {
			$uri = substr($uri, 0, $pos);
		}
		$uri = @explode("/", $uri);
		$uri2 = array();
		foreach ($uri as $p) {
			$p = trim($p);
			if ($p) {
				$uri2[] = $p;
			}
		}
		return array_values($uri2);
	}


	/**
	 * Преобразует число типа 10000 в 10 000
	 *
	 * @param int $str
	 * @return string
	 */
	function get_price($str) {
		$int = (string)intval($str);
		$l = strlen($int);
		$arr = array();
		for ($i=0; $i<$l; $i++) {
			$arr[] = $int{$i};
		}
		krsort($arr);
		$c = 0;
		$res = '';
		foreach ($arr as $dig) {
			$c++;
			$res = $dig.$res;
			if ($c == 3) {
				$res = " $res";
				$c = 0;
			}
		}
		return $res;
	}


	/**
	 * Преобразует дату в timestamp (Unix-метку времени)
	 *
	 * @param string $str
	 * @param string $sep
	 * @return int
	 */
	static function date_to_timestamp($str, $sep = '.') {
		$arr = explode($sep, $str);
		if (!is_array($arr) or (count($arr) != 3)) return;
		$d = $arr[0];
		$m = $arr[1];
		$y = $arr[2];
		return mktime(0, 0, 0, $m, $d, $y);
	}


	/**
	 * Подставляет окончания в зависимости от числительного
	 *
	 * @param int $d
	 * @param string $str
	 * @param string $e1
	 * @param string $e2
	 * @param string $e3
	 * @return string
	 */
	static function get_ending_numerals($d, $str, $e1, $e2, $e3) {
		if (($d >= 5 ) and ($d <= 20)) {
			$end = $e3;
		} else {
			$e = substr($d, -1);
			$end = ($e == 1) ? $e1 : ((($e > 1) and ($e < 5)) ? $e2 : $e3);
		}
		return $str.$end;
	}


	static function dump($var, $exit = false) {
		echo '<pre>';
		var_dump($var);
		echo '</pre>';
		if ($exit) exit;
	}


	static function clear_array($arr) {
		$out = array();
		foreach ($arr as $v) {
			if (($v) and (trim(strlen($v)) > 0)) {
				$out[] = $v;
			}
		}
		return $out;
	}


	static function convert_file_size($_byte) {
		$byte = intval($_byte);
		if (!$byte) {
			return 0;
		}
		if ($byte < 0) {
			$byte = intval(substr($_byte, 0, -3));
			return round(($byte / 1048576), 1).' Gb';
		}
		if ($byte < 1024) {
			return round($byte).' b';
		}
		if ($byte < 1048576) {
			return round($byte / 1024).' Kb';
		}
		if ($byte < 1073741824) {
			return round(($byte / 1048576), 1).' Mb';
		}
		
		return $res;

	}

}