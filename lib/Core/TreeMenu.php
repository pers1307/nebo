<?
class TreeMenu {
	var $db;
	var $page;
	var $smarty;

	private static $instance = null;
	var $uri;
	var $count = 0;
	var $uri_arr = array();
	private $debug_mode = false;

	function __construct() {
		$this->db = MyDB2::GetInstance();
		$this->smarty = SmartyHolder::GetInstance();
		$this->page = Page::GetInstance();
		$Location = Location::getInstance();
		$this->uri_arr = $Location->get_array();
		$this->uri = $Location->get_string();
		$this->count = count($this->uri_arr);
		$this->main();
	}

	/**
	 *
	 * @return TreeMenu
	 */
	public static function GetInstance() {
		if (is_null(self::$instance)) {
			self::$instance = new TreeMenu();
		}
		return self::$instance;
	}

	function main() {
		if (! $this->count) return;
		$selected = array();
		$parent_id = 0;
		foreach ( $this->uri_arr as $i => $str ) {
			$str = htmlspecialchars($str);
            // virtual handler workaround
            if ($str == '-virtual-handler') {
                $this->path_link = array();
                break;
            }
            // end
			$row = $this->db->getRow("SELECT id, head, fullpath FROM tree WHERE `parent` = $parent_id AND `fname` = '$str'");
			if (is_array($row) and count($row)) {
				$selected[$i] = $row['id'];
				$parent_id = $row['id'];
				$this->path_link[] = array($row['head'],$row['fullpath']);
			}
		}

        // var_dump($this->path_link);
        $this->page->doc['tree_selected'] = $selected;
		$this->smarty->assign('_path_link', $this->path_link);
	}

	/**
	 *
	 * @param int $level
	 * @return array
	 */
	function get_menu($level) {
		$level = $level - 1;
		if ($this->count < $level) return array();

		$fname = isset($this->uri_arr[$level]) ? $this->uri_arr[$level] : null;
		if ($this->debug_mode) echo $fname . '<br>'; // DEBUG
		$fullpath = $this->get_fullpath($fname, $level);
		if ($this->debug_mode) echo $fullpath . '<br>'; // DEBUG
		if ($level > 0) {
			$parent = $this->db->getOne("SELECT id FROM tree WHERE fullpath = '$fullpath'");
			$parent = intval($parent);
			if ($this->debug_mode) echo $parent . '<br>'; // DEBUG
		} else {
			$parent = $level;
		}

		$list = $this->getChilds($parent, true, true);
		return $list;
	}

	function getChilds($id, $filter = true, $menushow = false, $test = false) {
		$where = ($filter) ? " AND publish_time < NOW()" : '';
		$where .= ($menushow) ? " AND menushow = 1" : "";
		if (is_array($id)) {
			$ids = implode(",", $id);
			$sql = "SELECT id, parent, head, fname, editable, menushow, priority, fullpath, module FROM tree WHERE parent IN ($ids) $where ORDER BY priority";
		} else {
			$id = intval($id);
			$sql = "SELECT id, parent, head, fname, editable, menushow, priority, fullpath, module FROM tree WHERE parent = $id $where ORDER BY priority";
		}
		$res = $this->db->getAll($sql);
		// echo $sql.'<br>';
		if (PEAR::isError($res)) die($res->getMessage());
		return $res;
	}

	/**
	 *
	 * @return array
	 */
	function get_selected() {
		if (! $this->count) return array();
		$output = array();
		$parent = 0;
		foreach ( $this->uri_arr as $i => $str ) {
			$str = htmlspecialchars($str);
			$row = $this->db->getRow("SELECT id, parent FROM tree WHERE `parent` = $parent AND `fname` = '$str'");
			if (is_array($row) and count($row)) {
				$output[$i] = $row['id'];
				$parent = $row['parent'];
			}
		}

		return $output;
	}

	function get_fullpath($fname, $level) {
		$fullpath = '/';
		if (count($this->uri_arr)) {
			foreach ( $this->uri_arr as $k => $url ) {
				if ($k <= $level) {
					$fullpath .= "$url/";
				}
			}
		}
		return $fullpath;
	}

	function debug() {
		$this->debug_mode = true;
		echo "uri = $this->uri<br />";
		echo "count = $this->count<br />";
		echo '<pre>';
		var_dump($this->uri_arr);
		echo '</pre>';
		echo '<hr />';
	}

	function get_childs($parent) {
		$sql = "SELECT id, parent, head, fname, editable, menushow, priority, fullpath, module FROM tree WHERE parent = $parent AND publish_time < NOW() AND menushow = 1 ORDER BY priority";
		$res = $this->db->getAll($sql);
		return (is_array($res) and count($res)) ? $res : array();
	}

	function get_brothers($parent, $hide_id = false) {
		$where = ($hide_id) ? "AND id != $hide_id" : '';
		$sql = "SELECT id, parent, head, fname, editable, menushow, priority, fullpath, module FROM tree WHERE parent = $parent $where AND publish_time < NOW() AND menushow = 1 ORDER BY priority";
		$res = $this->db->getAll($sql);
		return (is_array($res) and count($res)) ? $res : array();
	}

	function get_root($id) {
		$parent_id = $this->db->getOne("SELECT parent FROM tree WHERE id = $id");
		if ($parent_id) {
			return $this->get_root($parent_id);
		} else {
			return $id;
		}
	}

	function get_tree($parent = 0) {
		$tree = $this->get_childs($parent);
		$arr = false;
		if (is_array($tree) and count($tree)) {
			$arr = array();
			foreach ( $tree as $item ) {
				$id = $item['id'];
				$arr[$id] = array();
				$arr[$id]['name'] = $item['head'];
				$arr[$id]['path'] = $item['fullpath'];

				$childs = $this->get_tree($id);
				if ($childs) {
					$arr[$id]['childs'] = $childs;
				}
			}
		}
		return $arr;
	}

}