<?
abstract class ACP_Module {

	protected $tpl_dir;
	public $smarty;
	public $db;
	public $controller;
	public $action;
	public $doc_id;
    protected $name;

	function __construct() {
		$this->db = MyDB2::GetInstance();
		$this->smarty = SmartyHolder::GetInstance();
		$folder = str_replace('ACP_', '', $this->name);
		$this->tpl_dir = R_Modules.$folder.'/acp_tpl/';
		$this->smarty->assign('tpl_dir', $this->tpl_dir);
	}


	function getContent(&$controller) {
		$this->controller = &$controller;
		$this->doc_id = $doc_id = $controller->doc_id;
		$this->action = $action = $controller->action;

		$this->smarty->assign('doc_id', $doc_id);
		$this->smarty->assign('action', $action);

		return $this->rewrite();
	}


	function rewrite() {
		// вот именно тут в наследуемых классах разруливается весь функционал на другие функции
		// пример подключения шаблона:
		// $this->controller->body = $this->smarty->fetch_smart('acp_ИмяКласса_list.tpl');
	}

}