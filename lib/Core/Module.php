<?
class Module {

	/**
	 * @var SmartyHolder
	 */
	var $smarty;

	/**
	 * @var MyDB2
	 */
	var $db;

	/**
	 * @var Page
	 */
	var $page;

	protected $tpl_dir;

	protected $_link;

	function __construct() {
		$this->db = MyDB2::GetInstance();
		$this->page = Page::GetInstance();
		$this->smarty = SmartyHolder::GetInstance();
		$this->tpl_dir = R_Modules.$this->name.'/tpl/';

        if ($this->smarty) {
            $this->_link = isset($this->page->doc['fullpath']) ? $this->page->doc['fullpath'] : null;
            $this->smarty->assign('self_tpl_dir', $this->tpl_dir);
            $this->smarty->assign('_link', $this->_link);
        }
	}


	function error($class, $function, $line, $message) {
		$create_date = time();
		$this->db->insert('log', compact('class', 'function', 'line', 'message', 'create_date'));
	}

}