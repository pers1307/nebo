<?
require_once SMARTY_DIR.'Smarty.class.php';

class SmartyHolder extends Smarty {

	private static $instance = null;

	/**
	 * объект Смарти должен быть один
	 * @return SmartyHolder
	 */
	public static function GetInstance($tpl_root = null) {
		if (is_null(self::$instance) and $tpl_root) {
			self::$instance = new SmartyHolder($tpl_root);
		}
		return self::$instance;
	}

	public function __construct($tpl_dir) {
		parent::__construct();
		$this->template_dir = $tpl_dir;
		$this->compile_dir = R_tpl_c;
		$this->caching = false;
		$this->error_reporting = E_ALL & ~E_NOTICE;
		$this->force_compile = 1;
	}

	public function fetch_smart($template, $cache_id = null, $compile_id = null, $parent = null, $display = false) {
		$template = $this->get_resource($template);
		return $this->fetch($template, $cache_id, $compile_id, $parent, $display);
	}

	public function display_smart($template, $cache_id = null, $compile_id = null, $parent = null) {
		$template = $this->get_resource($template);
		return $this->display($template, $cache_id, $compile_id, $parent);
	}

	private function get_resource($resource_name) {
		$arr = explode('_', $resource_name);
		if (count($arr) > 1) {
			if ($arr[0] == 'acp') {
				$resource_name = R_Modules.ucfirst($arr[1]).'/acp_tpl/'.$resource_name;
			} else {
				$resource_name = R_Modules.ucfirst($arr[0]).'/tpl/'.$resource_name;
			}
		}
		return $resource_name;
	}
}