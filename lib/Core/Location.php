<?
class Location {

	private static $instance = null;
	private $url = array();
	private $count = 0;
	private $i = 0;
	private $string = '/';


	function __construct() {
		if (!isset($_SERVER['REQUEST_URI'])) return; // если срабатывает по крону
		
		$uri_string = urldecode($_SERVER['REQUEST_URI']);
		$get_string = '';
		$pos = mb_strpos($uri_string, '?');
		if ($pos) {
			$get_string = mb_substr($uri_string, $pos);
			$uri_string = mb_substr($uri_string, 0, $pos);
		}
		if ($uri_string != '/') {
			if (mb_substr($uri_string, -1) != '/') {
				header("Location: {$uri_string}/{$get_string}");
			}
		}
		$uri_tmp = @explode("/", $uri_string);
		foreach ($uri_tmp as $str) {
			$str = trim(str_replace(array("'", '"', '%'), '', $str));
			if ($str) {
				$this->url[] = $str;
				$this->string .= $str.'/';
			}
		}
		$this->count = count($this->url);
	}


	/**
	 * @return Location
	 */
	public static function getInstance() {
		if (is_null(self::$instance)) {
			self::$instance = new Location();
		}
		return self::$instance;
	}


	public function current() {
		return $this->url[$this->i];
	}


	public function next() {
		$i = $this->i + 1;
		if (isset($this->url[$i])) {
			$this->i = $i;
			return $this->url[$i];
		} else {
			return false;
		}
	}


	public function get($i) {
		return (isset($this->url[$i])) ? $this->url[$i] : false;
	}


	public function reset() {
		$this->i = 0;
		return ($this->count) ? reset($this->url) : false;
	}


	public function first() {
		return ($this->count) ? reset($this->url) : false;
	}


	public function last() {
		return ($this->count) ? end($this->url) : false;
	}


	function get_array() {
		return $this->url;
	}


	function get_string() {
		return $this->string;
	}

	
}