<?
spl_autoload_register ('autoload');
function autoload ($className) {
	$fileName = R_Modules.$className.'/'.$className.'.php';
	if (file_exists($fileName)) require_once($fileName);
	else {
		$fileName = R_core.$className.'.php';
		if (file_exists($fileName)) require_once($fileName);
	}
}