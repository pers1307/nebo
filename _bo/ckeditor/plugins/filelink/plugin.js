CKEDITOR.plugins.add('filelink', {
	init : function(editor) {
		var command = editor.addCommand('filelink', new CKEDITOR.dialogCommand('filelink'));
		command.modes = {wysiwyg:1, source:1};
		command.canUndo = true;

		editor.ui.addButton('filelink', {
			label : 'Вставить ссылку на файл',
			command : 'filelink',
			icon: this.path + 'images/filelink_icon.png'
		});

		CKEDITOR.dialog.add('filelink', this.path + 'dialogs/filelink.js');
	}
});