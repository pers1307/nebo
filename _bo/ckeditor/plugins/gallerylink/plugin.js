CKEDITOR.plugins.add('gallerylink', {
	init : function(editor) {

        var gallerylink = editor.addCommand('gallerylink', new CKEDITOR.dialogCommand('gallerylink'));
        gallerylink.modes = {wysiwyg:1, source:1};
        gallerylink.canUndo = true;

        editor.ui.addButton('gallerylink', {
            label : 'Вставить галерею',
            command : 'gallerylink',
            icon: this.path + 'images/gallerylink_icon.png'
        });

        CKEDITOR.dialog.add('gallerylink', this.path + 'dialogs/gallerylink.js');
	}
});