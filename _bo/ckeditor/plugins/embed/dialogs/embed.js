CKEDITOR.dialog.add('embed', function(editor) {
	return {
		title : 'Вставить HTML',
		minWidth : 400,
		minHeight : 200,
		onOk: function() {
			var code_text = this.getContentElement('code_text', 'embed').getInputElement().getValue();
			this._.editor.insertHtml(code_text);
		},
		contents : [{
			id : 'code_text',
			label : 'First Tab',
			title : 'First Tab',
			elements : [{
				id : 'embed',
				type : 'textarea',
				label : 'HTML-код',
			}]
		}]
	};
});