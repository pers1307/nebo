CKEDITOR.plugins.add('embed', {
	init : function(editor) {
		var command = editor.addCommand('embed', new CKEDITOR.dialogCommand('embed'));
		command.modes = {wysiwyg:1, source:1};
		command.canUndo = true;

		editor.ui.addButton('embed', {
			label : 'Вставить HTML',
			command : 'embed',
			icon: this.path + 'images/embed.gif'
		});

		CKEDITOR.dialog.add('embed', this.path + 'dialogs/embed.js');
	}
});