/*
Copyright (c) 2003-2010, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config )
{
	// Define changes to default configuration here. For example:
	config.language = 'ru';
	config.uiColor = '#f8f8f8';
	
	config.toolbar_None = [];
	config.stylesCombo_stylesSet = 'my_styles';
	config.contentsCss = ['/s/editor.css'];
	config.templates_files = [ '/_bo/templates.js' ];
	config.templates_replaceContent = false;
	config.resize_enabled = false;


	config.extraPlugins = 'filelink,gallerylink';

    config.toolbar_Info =
    [
        ['Undo','Redo','-','Cut','Copy','Paste','PasteText','PasteFromWord'],
        ['Find','-','SelectAll','RemoveFormat'],
        ['Link','Unlink'],
        ['Source'],
        '/',
        ['Bold','-','Superscript'],
        ['NumberedList','BulletedList']
    ];

    config.toolbar_Info_advanced =
    [
        ['Source'],
        ['Templates'],
        ['Cut','Copy','Paste','PasteText','PasteFromWord'],
        ['Undo','Redo','-','SelectAll','RemoveFormat'],
        '/',
        ['Bold','Superscript','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
        ['Link','Unlink'],
        '/',
        ['Styles']
    ];

    config.toolbar_Top_Text =
    [
        ['Source'],
        ['Cut','Copy','Paste','PasteText','PasteFromWord'],
        ['Undo','Redo','-','RemoveFormat'],
        ['Bold','Superscript','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
        ['Link','Unlink'],
        ['Image'],
        ['Styles']
    ];

    config.toolbar_Text =
    [
        ['Source'],
        ['Templates'],
        ['Cut','Copy','Paste','PasteText','PasteFromWord'],
        ['Undo','Redo','-','SelectAll','RemoveFormat'],
        '/',
        ['Bold','Superscript','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
        ['NumberedList','BulletedList'],
        ['HorizontalRule'],
        ['Link','Unlink','-','embed','sployler'],
        ['filelink','gallerylink','Image','SpecialChar','Table','Flash'],
        ['Styles']
    ];

    config.extraAllowedContent = 'div(file,cover,hover); div{ * }; a(*); a[*]; img[*]; table';


};

CKEDITOR.addStylesSet('my_styles',
[
    { name : 'Заголовок 1', element : 'h1' },
    { name : 'Заголовок 2', element : 'h2' },
    { name : 'Заголовок 3', element : 'h3' }
    //{ name : 'Линия', element : 'p', attributes: {'class':'hr-line'} }
    //{ name : 'Линия', element : 'div', styles: {'border-top':'1px solid #0353A5', 'margin-bottom':'10px', 'height':'1px'} }
]);
