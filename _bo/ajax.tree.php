<?
require_once("../lib/Core/Vars.php");
require_once("../lib/Core/MyDB2.php");

class Ajax_Tree {

	private $db;

	/**
	 * @return Ajax_Tree
	 */
	function Factory() {
		return new Ajax_Tree();
	}
	

	function __construct() {
		$this->db = MyDB2::GetInstance();
	}


	function get_tree($parent = 0, $space = '') {
		$sql = "SELECT id, fullpath, title FROM tree WHERE parent = $parent";
		$list = $this->db->getAll($sql);
		if (is_array($list) and count($list)) {
			$space = '&nbsp;&nbsp;'.$space;
			foreach ($list as $row) {
				extract($row);
				?><option value="<?=$fullpath?>"><?=$space.$title?></option><?
				$this->get_tree($id, $space);
			}
		}
	}

}

header("Content-type: text/plain; Charset=utf-8");
Ajax_Tree::Factory()->get_tree();