if(options.onclick){
    $toastElement.click(function(){
        options.onclick();
        hideToast();
    });
}

publish(response);
if(options.debug&&console){
    console.log(response);
}
return $toastElement;

function hideToast(override){
    if($(':focus',$toastElement).length&&!override){
        return;
    }
    return $toastElement[options.hideMethod]({
        duration:options.hideDuration,
        easing:options.hideEasing,
        complete:function(){
            removeToast($toastElement);
            if(options.onHidden){
                options.onHidden();
            }
        response.state='hidden';
            response.endTime=new Date(),
                publish(response);
        }
    });
}

function delayedhideToast(){
    if(options.timeOut>0||options.extendedTimeOut>0){
        intervalId=setTimeout(hideToast,options.extendedTimeOut);
    }
}

function stickAround(){
    clearTimeout(intervalId);
    $toastElement.stop(true,true)[options.showMethod]({
        duration:options.showDuration,
        easing:options.showEasing
    });
}/*}*/

function getContainer(options){
    if(!options){
        options=getOptions();
    }
    $container=$('#'+options.containerId);
    if($container.length){
        return $container;
    }
    $container=$('<div/>').attr('id',options.containerId).addClass(options.positionClass);
    $container.appendTo($(options.target));
    return $container;
}

function getOptions(){
    return $.extend({},getDefaults(),toastr.options);
}

function removeToast($toastElement){
    if(!$container){
        $container=getContainer();
    }
    if($toastElement.is(':visible')){
        return;
    }
    $toastElement.remove();
    $toastElement=null;
    if($container.children().length===0){
        $container.remove();
    }
}
}
)
();
});
}(typeof define==='function'&&define.amd?define:function(deps,factory){if(typeof module!=='undefined'&&module.exports){module.exports=factory(require('jquery'));}else{window['toastr']=factory(window['jQuery']);}}));